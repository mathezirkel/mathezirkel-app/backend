# Mathezirkel App Backend

[![pipeline status](https://gitlab.com/mathezirkel/mathezirkel-app/backend/badges/main/pipeline.svg)](https://gitlab.com/mathezirkel/mathezirkel-app/backend/-/commits/main) [![coverage report](https://gitlab.com/mathezirkel/mathezirkel-app/backend/badges/main/coverage.svg)](https://gitlab.com/mathezirkel/mathezirkel-app/backend/badges/-/commits/main)

This library is the primary client for accessing the database of and organizing the `Mathezirkel Augsburg` and the `Mathecamp`.

## Usage

### Quirks

When deleting a `ParticipantExtension` from the database, the sex-age-event-statistic does not get updated, until a further update.
When updating the `sex` in the base-data of a `Participant`, the sex-age-event-statistic does not get updated, until a further update.

For both of these, got to any `ParticipantExtension` that is also in the event that should be updated and change the `class_year` once, wait and change it back. If the event `end_date` is in the future, the statistic should now be recomputed.

### Configuration

See [The Oauth Guide](./docs/OAUTH2.md) how to configure auth (in our case specialized on keycloak).

See [The Keycloak API Guide](./docs/KEYCLOAK_API.md) how to configure Keycloak-Interaction to sync user data (Keycloak Users <-> App Instructor Accounts).

See [The Script Server Guide](./docs/SCRIPT_SERVER.md) how to use and present scripts to be run on the scriptserver infrastructure.

## Development

### Requirements

-   [Docker Compose](https://docs.docker.com/compose/install/)

```shell
$ cp .env.example .env
$ docker compose up
```

Connect to the dev-container to get the rust-analyzer to work properly.
(If you just open the project the host-machine's Rust installation will provide language support, while the project will be built inside the container. This is relatively instable and inefficient.)

What is better:

-   `docker compose up` from the project folder in any terminal.
-   Use the `Remote explorer` extension to join the `backend-webserver-1` as a dev-container.
-   Optional: Open the terminal of your host from inside the vscode-container-window
    -   Command: `Terminal: create new Integrated Terminal (local)`
    -   `docker compose up` (attaches to the running docker session)
    -   close the original terminal
-   Install the extensions that are recommended
-   Now the diesel commands work out of the box locally (otherwise this is pretty ugly also)

Use the [diesel cli](https://diesel.rs/guides/getting-started.html#installing-diesel-cli):

```shell
# From the host machine:
$ docker compose run --rm webserver diesel print-schema  # visual
# ...

# Inside the dev-container:
$ diesel print-schema  # visual
$ diesel migration run  # redoes src/database/migrations/schema.rs
$ diesel migration redo  # redoes src/database/migrations/schema.rs
$ diesel migration generate XXXX  # after this edit the sql files and later run "migration run" to integrate
$ diesel database reset  # only for locally resetting the database!!
```

The generated schema might need patching. This can be done using the file `src/database/schema.patch`. The configuration is documented [Here under the key `patch_file`](https://diesel.rs/guides/configuring-diesel-cli.html). [This Github issue](https://github.com/diesel-rs/diesel/discussions/3336#discussioncomment-3718627) explains the reasoning why this is needed in the [migration to diesel2](https://diesel.rs/guides/migration_guide.html#2-0-0-nullability-of-array-elements).

### Load/Write Backups from/to Development database

Place Backup in `/backup_io/backup.dump`.

```shell
$ docker compose run --rm database pg_restore --no-owner --clean -d databasename /backup_io/backup.dump
```

Writes Backup to `/backup_io/backup.dump`.

```shell
$ docker compose run --rm database pg_dump -Fc -d databasename -f /backup_io/backup.dump
```

### Schema

If making code changes to the database/GraphQL-API, make sure to keep the [Schema-File](./docs/SCHEMA.md) in sync with the codebase.

### Testing Graphql Endpoint

To use the interactive Graphiql-Tool, visit http://localhost:8080/graphiql.
On the bottom of the page, there is a section `HTTP HEADERS`.
Paste the content:

```
{"Authorization": "Bearer development_token"}
```

Also make sure, to set in the `.env` file:

```
DISABLE_OAUTH_FOR_DEVELOPMENT=true
```

## Docker

### Building Server-Images Locally

Run from main repository location

```shell
docker build -f docker/webserver/Dockerfile .
docker build -f docker/scriptserver/Dockerfile .
```

## Release

A new version can be released by pushing a commit to the main branch on GitLab.

## License

The backend is published under GPL 3, see [LICENSE](LICENSE).

<!-- RE-release comment 1 -->
