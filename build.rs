use static_files::resource::generate_resources_mapping;
use std::{env, path::Path};

/// Generates the files in `/static_generated` from the ones in `/static_files`.
/// This is used to bundle non-text files (e.g. images) into the server executable.
fn main() -> std::io::Result<()> {
    // store the compile time commit sha from the gitlab-pipeline-variable
    let compile_time_ci_commit_sha = env::var("CI_COMMIT_SHA")
        .ok()
        .unwrap_or(String::from("CI_COMMIT_SHA not set on compile"));
    println!(
        "cargo:rustc-env={}={}",
        "CI_COMMIT_SHA", compile_time_ci_commit_sha
    );

    let out_dir = "static_generated";
    let generated_filename = Path::new(&out_dir).join("generated.rs");
    generate_resources_mapping("./static_files", None, generated_filename)
}
