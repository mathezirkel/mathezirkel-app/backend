# JQ

Test the jq endpoint:

```sh
# JQ Script (successful)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"json": "{}", "filter": "."}' \
    http://localhost:8082/jq

# JQ Script (fails because false token)
curl -X POST \
    -H "Authorization: Bearer falsetoken" \
    -H "Content-Type: application/json" \
    -d '{"json": "{}", "filter": "."}' \
    http://localhost:8082/jq

# JQ Script (fails because improper json-input)
curl -v -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"json": "{", "filter": "."}' \
    http://localhost:8082/jq

# Get version and status of scriptserver
curl -v -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{}' \
    http://localhost:8082/version

# https://www.base64encode.org/ -> Activate "perform URL save encoding"
# Python script execution (successful, basic script with stdout)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "entrypoint_file": {"name": "hi.py", "content_base_64": "cHJpbnQoIkhlbGxvIFdvcmxkIik="}, "arguments": [], "returns_output_file_names": [], "entrypoint_file_signature": "1911dfc5f19b4182dde2978b76f8a746001163e6c13930442f926682a46d1ca2"}' \
    http://localhost:8082/python

# Python script execution (fails, script signature verification)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "entrypoint_file": {"name": "hi.py", "content_base_64": "cHJpbnQoIkhlbGxvIFdvcmxkIik="}, "arguments": [], "returns_output_file_names": [], "entrypoint_file_signature": "1911dfc5f19b4182dde2978b76f8a746001163e6c13930442f926682a46d1ca3"}' \
    http://localhost:8082/python

# Python script execution (successful, uses command line arguments supplied to the script)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "entrypoint_file": {"name": "arg.py", "content_base_64": "aW1wb3J0IHN5cwpwcmludCAoJ2FyZ3VtZW50IGxpc3QnLCBzeXMuYXJndik="}, "arguments": ["nice"], "returns_output_file_names": [], "entrypoint_file_signature": "2f3bce02645651bfc731d48de1ac150d4d8c98bd8ac7090823e7edc48b116f98"}' \
    http://localhost:8082/python

# Python script execution (successful, returns a result file)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "entrypoint_file": {"name": "file.py", "content_base_64": "ZiA9IG9wZW4oImRlbW9maWxlLnR4dCIsICJhIikKZi53cml0ZSgiTm93IHRoZSBmaWxlIGhhcyBtb3JlIGNvbnRlbnQhIikKZi5jbG9zZSgp"}, "arguments": [], "returns_output_file_names": ["demofile.txt"], "entrypoint_file_signature": "14a381d0dfd1c05d572fd58cd7050557b4589d5400753e49900d3d9b3e4dd2c9"}' \
    http://localhost:8082/python

# Latex compilation (fails because non-local main script)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "main_doc_file": {"name": "./asd.tex", "content_base_64": "XGRvY3VtZW50Y2xhc3N7YXJ0aWNsZX0KXGJlZ2lue2RvY3VtZW50fQpGaXJzdCBkb2N1bWVudC4gVGhpcyBpcyBhIHNpbXBsZSBleGFtcGxlLCB3aXRoIG5vIApleHRyYSBwYXJhbWV0ZXJzIG9yIHBhY2thZ2VzIGluY2x1ZGVkLgpcZW5ke2RvY3VtZW50fQ"}, "main_doc_file_signature": "asd"}' \
    http://localhost:8082/latex

# Latex compilation (fails because file extension)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "main_doc_file": {"name": "asd.txt", "content_base_64": "XGRvY3VtZW50Y2xhc3N7YXJ0aWNsZX0KXGJlZ2lue2RvY3VtZW50fQpGaXJzdCBkb2N1bWVudC4gVGhpcyBpcyBhIHNpbXBsZSBleGFtcGxlLCB3aXRoIG5vIApleHRyYSBwYXJhbWV0ZXJzIG9yIHBhY2thZ2VzIGluY2x1ZGVkLgpcZW5ke2RvY3VtZW50fQ"}, "main_doc_file_signature": "asd"}' \
    http://localhost:8082/latex

# Latex compilation (fails because signature verification)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "main_doc_file": {"name": "asd.tex", "content_base_64": "XGRvY3VtZW50Y2xhc3N7YXJ0aWNsZX0KXGJlZ2lue2RvY3VtZW50fQpGaXJzdCBkb2N1bWVudC4gVGhpcyBpcyBhIHNpbXBsZSBleGFtcGxlLCB3aXRoIG5vIApleHRyYSBwYXJhbWV0ZXJzIG9yIHBhY2thZ2VzIGluY2x1ZGVkLgpcZW5ke2RvY3VtZW50fQ=="}, "main_doc_file_signature": "asdasd"}' \
    http://localhost:8082/latex

# Latex compilation (successful)
curl -X POST \
    -H "Authorization: Bearer testtoken" \
    -H "Content-Type: application/json" \
    -d '{"supplementary_files": [], "main_doc_file": {"name": "asd.tex", "content_base_64": "XGRvY3VtZW50Y2xhc3N7YXJ0aWNsZX0KXGJlZ2lue2RvY3VtZW50fQpGaXJzdCBkb2N1bWVudC4gVGhpcyBpcyBhIHNpbXBsZSBleGFtcGxlLCB3aXRoIG5vIApleHRyYSBwYXJhbWV0ZXJzIG9yIHBhY2thZ2VzIGluY2x1ZGVkLgpcZW5ke2RvY3VtZW50fQ=="}, "main_doc_file_signature": "29bc00410f2bfa39bfdb95e27b17c7a74c99b1b9919ac3f1b12ac23795ecd6c1"}' \
    http://localhost:8082/latex
```

# DECODE BASE 64 to file

```python3
import base64

def decode_base64_url_encoded_data(base64_url_encoded_data, output_filename):
    # Decode base64 URL-encoded data
    decoded_data = base64.urlsafe_b64decode(base64_url_encoded_data)

    # Write the decoded data to a PDF file
    with open(output_filename, 'wb') as f:
        f.write(decoded_data)

if __name__ == "__main__":
    # Example base64 URL-encoded data (replace this with your actual data)
    base64_url_encoded_data = "..."

    # Output filename for the PDF file
    output_filename = "output.pdf"

    # Decode and save the data to a PDF file
    decode_base64_url_encoded_data(base64_url_encoded_data, output_filename)
```

# WEBDAV

Tested the webdav endpoint (No longer directly accessible):

```sh
curl -X POST \
    -H "Authorization: Bearer development_token" \
    -H "Content-Type: application/json" \
    -d '{"path": "/"}' \
    http://localhost:8080/webdav_ls


curl -X POST \
    -H "Authorization: Bearer development_token" \
    -H "Content-Type: application/json" \
    -d '{"path": "/test"}' \
    http://localhost:8080/webdav_mkdir


curl -X POST \
    -H "Authorization: Bearer development_token" \
    -H "Content-Type: application/json" \
    -d '{"path": "/asd.asd", "content": "HELLOASD"}' \
    http://localhost:8080/webdav_put


curl -X POST \
    -H "Authorization: Bearer development_token" \
    -H "Content-Type: application/json" \
    -d '{"path": "/asd.asd"}' \
    http://localhost:8080/webdav_delete
```
