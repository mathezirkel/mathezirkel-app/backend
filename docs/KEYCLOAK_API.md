# Keycloak Admin REST API & Instructor Management

## Configuration (Initially)

### Admin CLI

Allow Admin API access on the proper realm

-   Clients
    -   admin-cli
        -   Settings
            -   Capability Config
                -   Set `Client authentication` to `On`
                -   Save
        -   Credentials
            -   Copy the `Client secret` and provide in `KEYCLOAK_ADMIN_REST_API_CLIENT_SECRET` env variable
            -   `KEYCLOAK_ADMIN_REST_API_CLIENT_ID` env variable should be `admin-cli`
-   `KEYCLOAK_ADMIN_REST_API_URL` should be along the line of `https://auth.mathezirkel-augsburg.de/admin/realms/dev`
-   `KEYCLOAK_ADMIN_REST_API_USERNAME` should be the username of a admin of the proper realm and proper access
-   `KEYCLOAK_ADMIN_REST_API_PASSWORD` should be the password of a admin of the proper realm and proper access

### Configure user fields

-   Realm Settings

    -   General
        -   Set `User Profile Enabled` to `On`
        -   Hit Save
    - Themes
        - Set `Account theme` to `keycloak.v3`
        -   Hit Save
    -   User Profile -> JSON editor
        -   Copy the [config](./keycloak-user-profile.json) into the editor
        -   Hit Save
