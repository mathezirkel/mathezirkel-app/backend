# Scriptserver

## Generate Signing keys

To assure the safety of the mathezirkel-cluster, only signed scripts can be run on the scriptserver cluster.

To generate the signing keys, you require the signing key that is deployed to the clusters script-server.

In the dev-environment this key is `test-signing-key-python` (or `test-signing-key-latex` respectively).
In prod, this is the value set in the secret env variable `SCRIPTSERVER_SECRET_PYTHON_SIGNING_KEY` / `SCRIPTSERVER_SECRET_LATEX_SIGNING_KEY`.

The signing tool can be used just like any application to generate the signing keys and base-64 encoded strings to run scripts. Make sure the `<<FILENAME-WITH-PATH>>` is found relatively from the CURRENT execution folder (that folder you run this inside will be mapped into the container temporarily).

```shell
docker run -v "$(pwd)":/tmp registry.gitlab.com/mathezirkel/mathezirkel-app/backend/signing-tool:main <<FILENAME-WITH-PATH>> <<SIGNINGKEY-SECRET>>
# Example:
docker run -v "$(pwd)":/tmp registry.gitlab.com/mathezirkel/mathezirkel-app/backend/signing-tool:main test.py test-signing-key-python
```

### Extra feature: Programmatic signing-key generation

If the feature `ENABLE_PYTHON_SCRIPTS_CAN_SIGN_LATEX_SCRIPTS` is set to `true`, the if and only if a signed python script generates a file named `doc.tex` the server will sign this new file.
The signature will replace all content in the `std_out` field of the result. This can the be used in further export stages to generate the pdf in a latex stage.

## CURL test the server

See [HERE](./CURL_SCRIPTS.md) some curl expressions that are used to test the scriptserver.
