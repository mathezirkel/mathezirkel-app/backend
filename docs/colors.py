import matplotlib.pyplot as plt
import numpy as np
import colorsys

# Sample RGB colors
rgb_colors = [
    (135, 206, 250),
    (176, 226, 255),
    (255, 193, 193),
    (255, 165, 79),
    (255, 187, 255),
    (224, 102, 255),
    (238, 210, 255),
    (255, 106, 106),
    (214, 96, 96),
    (175, 217, 166),
    (159, 173, 211),
    (202, 232, 249),
    (190, 166, 209),
    (230, 129, 120),
    (252, 156, 115),
    (196, 184, 255),
    (234, 120, 140),
    (253, 192, 28),
    (233, 206, 113),
    (219, 168, 46),
    (167, 211, 72),
    (191, 235, 46),
    (255, 220, 60),
    (62, 149, 155),
    (216, 213, 236),
    (238, 173, 118),
    (255, 127, 80),
    (144, 238, 144),
    (166, 230, 166),
    (151, 213, 238),
    (222, 184, 135),
    (197, 250, 197),
    (255, 102, 153),
    (130, 189, 248),
    (204, 255, 153),
    (179, 255, 102),
    (247, 83, 193),
    (238, 149, 210),
    (229, 255, 173),
    (240, 144, 121),
    (230, 184, 175),
    (147, 196, 125),
    (245, 134, 3),
    (248, 248, 37),
    (91, 236, 236),
    (247, 27, 244),
    (94, 104, 247),
    (0, 247, 120),
    (106, 247, 125),
    (250, 85, 233),
    (227, 214, 93),
    (227, 120, 213),
    (129, 227, 198),
    (143, 157, 227),
    (191, 122, 225),
    (232, 107, 169),
    (140, 212, 182),
    (227, 127, 174),
    (127, 84, 255),
]


# Convert RGB to HLS
hls_colors = [
    colorsys.rgb_to_hls(r / 255.0, g / 255.0, b / 255.0) for r, g, b in rgb_colors
]

# Create a figure and a polar subplot
fig = plt.figure()
ax = fig.add_subplot(111, polar=True)

# Plot each color on the color wheel
for index, ((r, g, b), (h, l, s)) in enumerate(zip(rgb_colors, hls_colors)):
    angle = h * 2 * np.pi  # Convert hue to radians
    radius = s  # Saturation as radius
    color = (r / 255.0, g / 255.0, b / 255.0)
    ax.plot(angle, radius, "o", color=color, markersize=18)
    ax.annotate(
        str(index),
        (angle, radius),
        textcoords="offset points",
        xytext=(0, 0),
        ha="center",
    )


# Set the limits and labels
ax.set_ylim(0, 1)
ax.set_yticks([])  # Hide the radial ticks
ax.set_xticks(np.linspace(0, 2 * np.pi, 12, endpoint=False))
ax.set_xticklabels(
    [
        "0°",
        "30°",
        "60°",
        "90°",
        "120°",
        "150°",
        "180°",
        "210°",
        "240°",
        "270°",
        "300°",
        "330°",
    ]
)

plt.title("HLS Color Wheel")
plt.show()
