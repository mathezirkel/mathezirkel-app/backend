use sha2::{Digest, Sha256};

/// Hashes a String with the sha 256 Algorithm and outputs the result as a 64-char long printable ASCII subset string
/// Outputs pure Hexadecimal encoding, so must not be feared to be used in urls and so on
///
/// # Caution
///
/// make sure, this function always has the same implementation as https://gitlab.com/mathezirkel/mathezirkel-app/client/-/blob/main/src-tauri/src/hash.rs
///
/// # Arguments
///
/// * `input` - The String to input into the hasher, will be converted into bytes, so must not even be printable
///
/// # Examples
///
/// ```
/// let token_string = String::from("somestring");
/// let hashed_token = hash_sha256(&token_string);
/// ```
pub fn hash_sha256(input: &String) -> String {
    let mut hasher = Sha256::new();

    hasher.update(input.clone().as_bytes());

    let hashed_token = hasher
        .finalize()
        .iter()
        .map(|byte| format!("{:02x}", byte))
        .collect::<String>();

    return hashed_token;
}
