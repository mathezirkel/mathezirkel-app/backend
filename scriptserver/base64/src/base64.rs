use base64::{
    alphabet,
    engine::{self, general_purpose, DecodePaddingMode},
    Engine as _,
};

const BASE_64_ENGINE: engine::GeneralPurpose = engine::GeneralPurpose::new(
    &alphabet::URL_SAFE,
    general_purpose::PAD
        .with_encode_padding(true)
        .with_decode_padding_mode(DecodePaddingMode::Indifferent),
);

pub fn base_64_encode_string_to_string(input: &str) -> String {
    let mut string_buf = String::new();
    BASE_64_ENGINE.encode_string(input, &mut string_buf);
    string_buf
}

pub fn base_64_encode_bytes_to_string(input: &Vec<u8>) -> String {
    let mut string_buf = String::new();
    BASE_64_ENGINE.encode_string(input, &mut string_buf);
    string_buf
}

pub fn base_64_decode_string_to_bytes(input: &str) -> Vec<u8> {
    let bytes_buf = BASE_64_ENGINE.decode(input).unwrap();
    bytes_buf
}

pub fn base_64_decode_string_to_string(input: &str) -> String {
    let bytes_buf = BASE_64_ENGINE.decode(input).unwrap();
    String::from_utf8_lossy(&bytes_buf).to_string()
}
