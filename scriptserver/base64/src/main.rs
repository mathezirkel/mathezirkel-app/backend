use mathezirkel_app_scriptserver_base64::{base_64_encode_bytes_to_string, generate_signature};
use std::env;
use std::fs;

pub fn main() {
    // Check if enough command line arguments are provided
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        eprintln!(
            "Usage: {} <file_path> <secret_key> [--base64-only / --signature-only]",
            args[0]
        );
        std::process::exit(1);
    }

    // Extract file path and secret key from command line arguments
    let file_path = &args[1];
    let secret_key = &args[2];
    let mut mode = &String::from("");
    if args.len() >= 4 {
        mode = &args[3];
    }

    let mut only_base_64 = false;
    let mut only_signature = false;
    if mode.trim() == "--base64-only" {
        only_base_64 = true;
    }
    if mode.trim() == "--signature-only" {
        only_signature = true;
    }

    // Read file as bytes
    let file_bytes = match fs::read(file_path) {
        Ok(bytes) => bytes,
        Err(err) => {
            eprintln!("Error reading file: {}", err);
            std::process::exit(1);
        }
    };

    // Encode file bytes to base64
    let base64_encoded = base_64_encode_bytes_to_string(&file_bytes);

    // Generate signing key using the provided secret key and base64 encoded data
    let signature = generate_signature(&base64_encoded, secret_key);

    if only_base_64 {
        println!("{}", base64_encoded);
    } else if only_signature {
        println!("{}", signature);
    } else {
        println!("Base 64 encoded script: {}", base64_encoded);
        println!("Signature of the script: {}", signature);
    }
}
