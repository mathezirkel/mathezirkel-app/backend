use mathezirkel_app_hashing::hash_sha256;

pub fn generate_signature(base_64_input: &str, secret: &str) -> String {
    return hash_sha256(&format!(
        "{}{}{}{}",
        &secret.trim(),
        &base_64_input.trim(),
        &base_64_input.trim(),
        &secret.trim()
    ));
}

pub fn verify_signature(base_64_input: &str, secret: &str, key_to_compare: &str) -> bool {
    let real = generate_signature(base_64_input, secret);

    return real == key_to_compare;
}
