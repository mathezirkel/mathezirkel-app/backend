use std::env;

fn main() -> () {
    // store the compile time commit sha from the gitlab-pipeline-variable:
    let compile_time_ci_commit_sha = env::var("CI_COMMIT_SHA")
        .ok()
        .unwrap_or(String::from("CI_COMMIT_SHA not set on compile"));
    println!(
        "cargo:rustc-env={}={}",
        "CI_COMMIT_SHA", compile_time_ci_commit_sha
    );
}
