use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ScriptFile {
    pub name: String,
    pub content_base_64: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ScriptResponse {
    pub success: bool,
    pub std_out: String,
    pub std_err: String,
    pub files: Vec<ScriptFile>,
    pub async_id: Option<Uuid>,
}

#[derive(Serialize, Deserialize)]
pub struct VersionRequestPostVariables {}

#[derive(Serialize, Deserialize)]
pub struct JQRequestPostVariables {
    pub json: String,
    pub filter: String,
}

#[derive(Serialize, Deserialize)]
pub struct ProbeRequestPostVariables {
    pub id: Uuid,
}

#[derive(Serialize, Deserialize)]
pub struct PythonRequestPostVariables {
    pub supplementary_files: Vec<ScriptFile>,
    pub entrypoint_file: ScriptFile,
    pub entrypoint_file_signature: String,
    pub arguments: Vec<String>,
    pub returns_output_file_names: Vec<String>,
}

#[derive(Serialize, Deserialize)]
pub struct LatexRequestPostVariables {
    pub supplementary_files: Vec<ScriptFile>,
    pub main_doc_file: ScriptFile,
    pub main_doc_file_signature: String,
}
