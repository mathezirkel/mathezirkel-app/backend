use crate::scripts::run_commandline_command;
use mathezirkel_app_scriptserver_base64::{
    base_64_decode_string_to_bytes, base_64_encode_bytes_to_string, base_64_encode_string_to_string,
};
use std::fs;
use std::path::{Path, PathBuf};
use uuid::Uuid;

const GLOBAL_STORAGE_PATH: &str = "/storage/"; // TODO move to .env variable

/// Helper function to construct the absolute path for the file or directory.
pub fn construct_path(folder_name: &str, name_with_rel_path: &str) -> String {
    let mut path = PathBuf::new();
    path.push(GLOBAL_STORAGE_PATH);
    path.push(folder_name);
    path.push(name_with_rel_path);
    path.to_string_lossy().to_string()
}

/// Creates the directory at the specified path if it does not exist.
/// creates the folders if required
pub fn mk_dir(path: &str) -> bool {
    let global_path = construct_path("", path);
    if let Err(err) = fs::create_dir_all(&global_path) {
        error!("Error creating directory: {} {}", err, global_path);
        return false;
    }
    true
}

/// Checks if the directory exists at the specified path.
pub fn check_dir_exists(path: &str) -> bool {
    let global_path = construct_path(path, "");
    Path::new(&global_path).exists() && Path::new(&global_path).is_dir()
}

/// creates the folders if required
/// # Argument
/// * `file_content` base 64 encoded
pub fn put_file(file_content: &str, name_with_rel_path: &str, folder_name: &str) -> bool {
    let mut file_path = construct_path(folder_name, name_with_rel_path);

    let binding = PathBuf::from(&file_path);
    let parent_dir = binding.parent().unwrap().to_string_lossy().to_string();

    let mut is_zip = false;
    if let Some(extension) = binding.extension() {
        if extension.to_string_lossy().to_string() == "zip" {
            is_zip = true;
        }
    } else {
        // has no extension, is folder.
        info!("Inserted folder on file creation. Assumed it was zipped. Store as zip and try unzipping.");

        is_zip = true;
        file_path = construct_path(folder_name, &format!("{}.zip", name_with_rel_path));
    }

    if !check_dir_exists(&parent_dir) {
        if !mk_dir(&parent_dir) {
            return false;
        }
    }

    if let Err(err) = fs::write(&file_path, base_64_decode_string_to_bytes(file_content)) {
        error!("Error writing file: {} {}", err, file_path);
        return false;
    }

    if is_zip {
        // unzip
        let file_name = PathBuf::from(&file_path)
            .file_stem()
            .unwrap()
            .to_string_lossy()
            .to_string();
        info!(
            "Unzipping {} ('{}' in '{}')",
            file_path, file_name, parent_dir
        );
        let (unzip_success, _, unzip_err) =
            run_commandline_command("unzip", &[&file_name], None, &parent_dir);

        if !unzip_success {
            error!("Error unzipping file: {}", unzip_err);
            return false;
        }
    }

    true
}

/// # Returns
/// * `file_content` base 64 encoded
pub fn read_file(name_with_rel_path: &str, folder_name: &str) -> String {
    let mut file_path = construct_path(folder_name, name_with_rel_path);

    if Path::new(&file_path).is_dir() {
        info!("File output is folder, zip first");

        // read a folder. Needs to zip it first
        let zip_uuid = Uuid::new_v4().to_string();
        let zip_full_filename = format!("{}.zip", zip_uuid);
        let (zip_success, _, zip_err) = run_commandline_command(
            "zip",
            &["-r", &zip_full_filename, &name_with_rel_path],
            None,
            &construct_path(folder_name, ""),
        );
        if !zip_success {
            error!("Zip file could not be created: {}", zip_err);
            return base_64_encode_string_to_string("");
        }

        let zip_file_full_path = construct_path(folder_name, &zip_full_filename);
        file_path = zip_file_full_path;
    }

    match fs::read(&file_path) {
        Ok(content) => base_64_encode_bytes_to_string(&content),
        Err(err) => {
            error!("Error reading file: {} {}", err, file_path);
            base_64_encode_string_to_string("")
        }
    }
}

/// Removes the file or directory at the specified path.
pub fn rm(path: &str) -> bool {
    let global_path = construct_path("", path);

    if Path::new(&global_path).is_dir() {
        if let Err(err) = fs::remove_dir_all(&global_path) {
            error!("Error removing dir: {} {}", err, global_path);
            return false;
        }
    } else {
        if let Err(err) = fs::remove_file(&global_path) {
            error!("Error removing file: {} {}", err, global_path);
            return false;
        }
    }

    true
}
