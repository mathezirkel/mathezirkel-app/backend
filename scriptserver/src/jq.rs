use crate::scripts::run_commandline_command;
use mathezirkel_app_scriptserver_interfaces::{JQRequestPostVariables, ScriptResponse};

pub fn jq(input_variables: &JQRequestPostVariables) -> ScriptResponse {
    let json = &input_variables.json;
    let filter = &input_variables.filter;

    debug!("Running JQ with filter '{}'", filter);

    let (success, stdout, stderr) = run_commandline_command("jq", &["-r", filter], Some(json), "/");

    if success {
        debug!("Successful run of jq");
        ScriptResponse {
            success: true,
            files: Vec::new(),
            std_err: String::from(""),
            std_out: stdout,
            async_id: None,
        }
    } else {
        error!("Failed to run jq {}", &stderr);
        ScriptResponse {
            success: false,
            files: Vec::new(),
            std_err: stderr,
            std_out: String::from(""),
            async_id: None,
        }
    }
}
