use crate::{response_job_cache::add_to_retrieve_cache, scripts::file_using_command_line_command};
use mathezirkel_app_scriptserver_base64::verify_signature;
use mathezirkel_app_scriptserver_interfaces::{
    LatexRequestPostVariables, ScriptFile, ScriptResponse,
};
use std::{env, path::PathBuf};
use uuid::Uuid;

pub fn latex(input_variables: &LatexRequestPostVariables) -> ScriptResponse {
    let path = PathBuf::from(&input_variables.main_doc_file.name);
    let doc_file_name = match path.file_stem() {
        Some(stem) => stem.to_string_lossy().to_string(),
        None => {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(
                    "Executing Latex script requires the main_doc_file to have a name",
                ),
                files: Vec::new(),
                async_id: None,
            }
        }
    };
    let doc_file_extension = match path.extension() {
        Some(ext) => ext.to_string_lossy().to_string(),
        None => {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(
                    "Executing Latex script requires the main_doc_file to have an extension",
                ),
                files: Vec::new(),
                async_id: None,
            }
        }
    };
    if doc_file_extension != "tex" {
        return ScriptResponse {
            success: false,
            std_out: String::from(""),
            std_err: String::from(
                "Executing Latex script requires the main_doc_file to have the extension .tex",
            ),
            files: Vec::new(),
            async_id: None,
        };
    }
    if path.parent().is_some() {
        let parent_path = path.parent().unwrap().to_string_lossy().to_string();

        if parent_path != "" {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "Executing Latex script requires the main_doc_file to NOT have a path, is {}",
                    &parent_path
                )),
                files: Vec::new(),
                async_id: None,
            };
        }
    }

    // !!! Verify the signing key of the main script
    let main_doc_file_base_64 = String::from(&input_variables.main_doc_file.content_base_64);
    let script_verification = verify_signature(
        &main_doc_file_base_64,
        &env::var("SCRIPTSERVER_SECRET_LATEX_SIGNING_KEY").expect(
            "Scriptserver signing-key ('SCRIPTSERVER_SECRET_LATEX_SIGNING_KEY') not set in .env",
        ),
        &input_variables.main_doc_file_signature,
    );
    if !script_verification {
        return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "Executing Latex script requires a correctly signed main_doc_file, but the provided signature \"{}\" did not match.",
                    &input_variables.main_doc_file_signature
                )),
                files: Vec::new(),
                async_id: None,
            };
    }

    debug!(
        "Running latex with main document file '{}.{}'",
        &doc_file_name, &doc_file_extension
    );

    let mut files: Vec<ScriptFile> = Vec::new();
    for file in &input_variables.supplementary_files {
        // try avoiding files being able to shadow the main script
        if file.name.contains(&doc_file_name) {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "No supplementary file may contain the name of the main_doc_file in their name! \"{}\" does.",
                    &file.name
                )),
                files: Vec::new(),
                async_id: None,
            };
        }
        files.push(ScriptFile {
            content_base_64: String::from(&file.content_base_64),
            name: String::from(&file.name),
        })
    }
    // Main script file ALWAYS last, to assure no supplementary file shadows the one with the verified signature
    files.push(ScriptFile {
        content_base_64: main_doc_file_base_64,
        name: String::from(format!("{}.{}", &doc_file_name, &doc_file_extension)),
    });

    let job_uuid = Uuid::new_v4();
    let job_uuid_to_return = job_uuid.clone();

    actix_rt::task::spawn_blocking(move || {
        let script_result = file_using_command_line_command(
            "/usr/bin/latexmk",
            &[
                "-pdf",
                "-g",
                "-pdflatex=/usr/bin/pdflatex -file-line-error -interaction=nonstopmode %O %S", // TODO might require --shell-escape (THIS IS NOT ALLOWED FOR SECURITY REASONS FOR NOW)
                &format!("{}.{}", &doc_file_name, &doc_file_extension),
            ],
            None,
            &files,
            &[&format!("{}.pdf", &doc_file_name)],
        );

        // need to debug locally if latex script execution fails
        debug!(
            "Latex Out: {}, Latex Err: {}",
            &script_result.std_out, &script_result.std_err
        );

        debug!("Adding job with uuid {} to retrieve cache", &job_uuid);
        add_to_retrieve_cache(job_uuid, script_result);
    });

    ScriptResponse {
        success: false,
        std_out: String::from("The job runs asynchronously, probe for it"),
        std_err: String::from("The job runs asynchronously, probe for it"),
        files: Vec::new(),
        async_id: Some(job_uuid_to_return),
    }
}
