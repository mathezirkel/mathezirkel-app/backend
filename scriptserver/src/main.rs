#[macro_use]
extern crate log;

mod filesystem;
mod jq;
mod latex;
mod python;
mod response_job_cache;
mod routes_handlers;
mod scripts;

use actix_cors::Cors;
use actix_web::{http::header, middleware, web, App, Error, HttpResponse, HttpServer};
use dotenv::dotenv;
use routes_handlers::{jq_route, latex_route, probe_cache_route, python_route, version_route};
use std::env;

pub async fn health_route() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok()
        .append_header(("Content-Type", "text/plain"))
        .body("The scriptserver is ready."))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // init logging, env variables (requires .env file next to the executable) and database
    env_logger::init();
    dotenv().ok();

    // main Actix server function
    let server = HttpServer::new(move || {
        App::new()
            .app_data(web::PayloadConfig::new(150000000)) // 150 mb payload limit
            .app_data(web::JsonConfig::default().limit(150000000)) // 150 mb json limit
            .wrap(
                Cors::default()
                    .allow_any_origin() // TODO: could be tightened but access should be cluster-only anyway so probably unnecessary
                    .allowed_methods(vec!["POST", "GET"])
                    .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                    .allowed_header(header::CONTENT_TYPE)
                    .supports_credentials()
                    .max_age(3600),
            )
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            // all web-service-endpoints are registered below here
            .service(web::resource("/jq").route(web::post().to(jq_route)))
            .service(web::resource("/python").route(web::post().to(python_route)))
            .service(web::resource("/latex").route(web::post().to(latex_route)))
            .service(web::resource("/cache").route(web::post().to(probe_cache_route)))
            .service(web::resource("/version").route(web::post().to(version_route)))
            .service(web::resource("/health").route(web::get().to(health_route)))
    });

    // Start the server on the specified port/host
    let server_host = env::var("SCRIPTSERVER_HOST")
        .expect("Scriptserver host ('SCRIPTSERVER_HOST') not set in .env");
    let server_port = env::var("SCRIPTSERVER_PORT")
        .expect("Scriptserver port ('SCRIPTSERVER_PORT') not set in .env");
    info!(
        "Scriptserver started on http://{}:{}",
        server_host, server_port
    );
    server
        .bind(format!("{}:{}", server_host, server_port))
        .unwrap()
        .run()
        .await
}
