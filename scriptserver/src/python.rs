use crate::{response_job_cache::add_to_retrieve_cache, scripts::file_using_command_line_command};
use mathezirkel_app_scriptserver_base64::{generate_signature, verify_signature};
use mathezirkel_app_scriptserver_interfaces::{
    PythonRequestPostVariables, ScriptFile, ScriptResponse,
};
use std::{env, path::PathBuf};
use uuid::Uuid;

const NEW_SIGN_FILE_NAME: &str = "doc.tex";

pub fn python(input_variables: &PythonRequestPostVariables) -> ScriptResponse {
    let path = PathBuf::from(&input_variables.entrypoint_file.name);
    let script_file_name = match path.file_stem() {
        Some(stem) => stem.to_string_lossy().to_string(),
        None => {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(
                    "Executing Python script requires the entrypoint_file to have a name",
                ),
                files: Vec::new(),
                async_id: None,
            }
        }
    };
    let script_file_extension = match path.extension() {
        Some(ext) => ext.to_string_lossy().to_string(),
        None => {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(
                    "Executing Python script requires the entrypoint_file to have an extension",
                ),
                files: Vec::new(),
                async_id: None,
            }
        }
    };
    if script_file_extension != "py" {
        return ScriptResponse {
            success: false,
            std_out: String::from(""),
            std_err: String::from(
                "Executing Python script requires the entrypoint_file to have the extension .py",
            ),
            files: Vec::new(),
            async_id: None,
        };
    }
    if path.parent().is_some() {
        let parent_path = path.parent().unwrap().to_string_lossy().to_string();

        if parent_path != "" {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "Executing Python script requires the entrypoint_file to NOT have a path, is {}",
                    &parent_path
                )),
                files: Vec::new(),
                async_id: None,
            };
        }
    }

    // !!! Verify the signing key of the main script
    let entrypoint_file_base_64 = String::from(&input_variables.entrypoint_file.content_base_64);
    let script_verification = verify_signature(
        &entrypoint_file_base_64,
        &env::var("SCRIPTSERVER_SECRET_PYTHON_SIGNING_KEY").expect(
            "Scriptserver signing-key ('SCRIPTSERVER_SECRET_PYTHON_SIGNING_KEY') not set in .env",
        ),
        &input_variables.entrypoint_file_signature,
    );
    if !script_verification {
        return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "Executing Python script requires a correctly signed entrypoint_file, but the provided signature \"{}\" did not match.",
                    &input_variables.entrypoint_file_signature
                )),
                files: Vec::new(),
                async_id: None,
            };
    }

    debug!(
        "Running Python with entry file '{}.{}'",
        &script_file_name, &script_file_extension
    );

    let mut files: Vec<ScriptFile> = Vec::new();
    for file in &input_variables.supplementary_files {
        // try avoiding files being able to shadow the main script
        if file.name.contains(&script_file_name) {
            return ScriptResponse {
                success: false,
                std_out: String::from(""),
                std_err: String::from(format!(
                    "No supplementary file may contain the name of the entrypoint_file in their name! \"{}\" does.",
                    &file.name
                )),
                files: Vec::new(),
                async_id: None,
            };
        }
        // if signing is enabled, check for signing overwrite
        if env::var("ENABLE_PYTHON_SCRIPTS_CAN_SIGN_LATEX_SCRIPTS")
            .or::<String>(Ok(String::from("false")))
            .unwrap()
            == String::from("true")
        {
            if file.name.contains(NEW_SIGN_FILE_NAME) {
                // ONLY NEW_SIGN_FILE_NAME might be signed ever. No shady business. DO not allow supplying such a file, must be generated
                info!("Python script may sign generated files named {}. But such a file was provided. Conflict and danger of injection.", file.name);

                return ScriptResponse {
                    success: false,
                    std_out: String::from(""),
                    std_err: String::from(format!(
                        "No supplementary file may contain the name of the new-signing-generation target \"{}\", but \"{}\" does.",
                        NEW_SIGN_FILE_NAME,
                        file.name
                    )),
                    files: Vec::new(),
                    async_id: None,
                };
            }
        }
        files.push(ScriptFile {
            content_base_64: String::from(&file.content_base_64),
            name: String::from(&file.name),
        })
    }
    // Main script file ALWAYS last, to assure no supplementary file shadows the one with the verified signature
    files.push(ScriptFile {
        content_base_64: entrypoint_file_base_64,
        name: String::from(format!("{}.{}", &script_file_name, &script_file_extension)),
    });

    let job_uuid = Uuid::new_v4();
    let job_uuid_to_return = job_uuid.clone();

    let movable_arguments = input_variables.arguments.clone();
    let movable_output_file_names = input_variables.returns_output_file_names.clone();

    actix_rt::task::spawn_blocking(move || {
        let mut input_slice: Vec<String> = Vec::new();
        input_slice.push(String::from(format!(
            "{}.{}",
            &script_file_name, &script_file_extension
        ))); // main script name
        for arg in movable_arguments {
            input_slice.push(String::from(arg));
        }
        let slice_of_program_args: Vec<&str> = input_slice.iter().map(|s| s.as_ref()).collect();

        let mut output_file_names_slice: Vec<String> = Vec::new();
        for fname in movable_output_file_names {
            output_file_names_slice.push(String::from(fname));
        }
        let slice_of_output_file_names: Vec<&str> =
            output_file_names_slice.iter().map(|s| s.as_ref()).collect();

        let mut script_result = file_using_command_line_command(
            "python3",
            &slice_of_program_args,
            None,
            &files,
            &slice_of_output_file_names,
        );

        // check whether a new python script must be signed
        if env::var("ENABLE_PYTHON_SCRIPTS_CAN_SIGN_LATEX_SCRIPTS")
            .or::<String>(Ok(String::from("false")))
            .unwrap()
            == String::from("true")
        {
            // could possibly be signing

            for file in &script_result.files {
                if file.name == NEW_SIGN_FILE_NAME {
                    // ONLY NEW_SIGN_FILE_NAME might be signed ever. No shady business
                    info!("Python script produced a latex script that is named {}. Signing it and placing the signature in the std_out", NEW_SIGN_FILE_NAME);

                    script_result.std_out = generate_signature(&file.content_base_64, &env::var("SCRIPTSERVER_SECRET_LATEX_SIGNING_KEY").expect(
                    "Scriptserver signing-key ('SCRIPTSERVER_SECRET_LATEX_SIGNING_KEY') not set in .env"));
                }
            }
        }

        // need to debug locally if python script execution fails
        debug!(
            "Python Out: {}, Python Err: {}",
            &script_result.std_out, &script_result.std_err
        );

        debug!("Adding job with uuid {} to retrieve cache", &job_uuid);
        add_to_retrieve_cache(job_uuid, script_result);
    });

    ScriptResponse {
        success: false,
        std_out: String::from("The job runs asynchronously, probe for it"),
        std_err: String::from("The job runs asynchronously, probe for it"),
        files: Vec::new(),
        async_id: Some(job_uuid_to_return),
    }
}
