use lazy_static::lazy_static;
use mathezirkel_app_scriptserver_interfaces::ScriptResponse;
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
    time::{Duration, SystemTime},
};
use uuid::Uuid;

lazy_static! {
    static ref TOKEN_RESPONSE_CACHE: Arc<RwLock<HashMap<Uuid, (SystemTime, ScriptResponse)>>> =
        Arc::new(RwLock::new(HashMap::new()));
}

pub fn add_to_retrieve_cache(uuid: Uuid, to_insert: ScriptResponse) {
    TOKEN_RESPONSE_CACHE
        .write()
        .unwrap()
        .insert(uuid, (SystemTime::now(), to_insert));
}

pub fn try_pop_from_retrieve_cache(uuid: Uuid) -> Option<ScriptResponse> {
    let mut cache = TOKEN_RESPONSE_CACHE.write().unwrap();

    let mut content: Option<ScriptResponse> = None;
    if cache.contains_key(&uuid) {
        let (_, gotten_content) = cache.remove(&uuid).unwrap(); // just tested that is included and we hold lock

        content = Some(gotten_content);
    }

    let keys: Vec<Uuid> = cache.keys().cloned().collect();
    for key in keys {
        let timestamp_to_potentially_clear = cache
            .get(&key)
            .unwrap() // we got keys from the collection itself, so all of them should resolve to filled
            // Only this region deletes and we hold a write lock, so this must be set
            .0
            .clone();
        if timestamp_to_potentially_clear < (SystemTime::now() - Duration::from_secs(3600)) {
            cache.remove(&key);
            debug!("Cleared out response cache values from RAM");
        }
    }

    content
}
