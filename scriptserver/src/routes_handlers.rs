use crate::{
    jq::jq, latex::latex, python::python, response_job_cache::try_pop_from_retrieve_cache,
};
use actix_web::{
    web::{self},
    Error, HttpResponse,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use last_git_commit::LastGitCommit;
use mathezirkel_app_scriptserver_interfaces::{
    JQRequestPostVariables, LatexRequestPostVariables, ProbeRequestPostVariables,
    PythonRequestPostVariables, ScriptResponse, VersionRequestPostVariables,
};
use std::env;

async fn general_route<F>(auth: BearerAuth, function: F) -> Result<HttpResponse, Error>
where
    F: Fn() -> ScriptResponse,
{
    let bearer_token = String::from(auth.token());

    let compare_token = env::var("SCRIPTSERVER_AUTH_TOKEN")
        .expect("Token ('SCRIPTSERVER_AUTH_TOKEN') not set in .env");
    let access = bearer_token.eq(&compare_token);

    if access {
        let script_response = function();

        return match script_response.success || script_response.async_id.is_some() {
            true => Ok(HttpResponse::Ok()
                .append_header(("Content-Type", "text/json"))
                .json(script_response)),
            false => Ok(HttpResponse::InternalServerError()
                .append_header(("Content-Type", "text/json"))
                .json(script_response)),
        };
    } else {
        // error, if the bearer-auth challenge was failed
        return Ok(HttpResponse::Unauthorized()
            .append_header(("Content-Type", "text/json"))
            .json(ScriptResponse {
                success: false,
                files: Vec::new(),
                std_err: String::from(
                    "Unauthorized: Bearer Token did not authenticate you to access this server",
                ),
                std_out: String::from(""),
                async_id: None,
            }));
    }
}

pub async fn jq_route(
    auth: BearerAuth,
    post_var: web::Json<JQRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    let args = &post_var.into_inner();
    general_route(auth, || jq(args)).await
}

pub async fn version_route(
    auth: BearerAuth,
    _post_var: web::Json<VersionRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    general_route(auth, || {
        let long = match LastGitCommit::new().build() {
            Ok(lgc) => lgc.id().long(), // should work when compiling locally
            Err(_) => String::from(env!("CI_COMMIT_SHA")), // fallback, assumes CI_COMMIT_SHA env variable was set on compile time instead: should be in the building-pipeline
        };

        ScriptResponse {
            success: true,
            files: Vec::new(),
            std_err: String::from(""),
            std_out: long,
            async_id: None,
        }
    })
    .await
}

pub async fn python_route(
    auth: BearerAuth,
    post_var: web::Json<PythonRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    let args = &post_var.into_inner();
    general_route(auth, || python(args)).await
}

pub async fn latex_route(
    auth: BearerAuth,
    post_var: web::Json<LatexRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    let args = &post_var.into_inner();
    general_route(auth, || latex(args)).await
}

pub async fn probe_cache_route(
    auth: BearerAuth,
    post_var: web::Json<ProbeRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    let bearer_token = String::from(auth.token());

    let compare_token = env::var("SCRIPTSERVER_AUTH_TOKEN")
        .expect("Token ('SCRIPTSERVER_AUTH_TOKEN') not set in .env");
    let access = bearer_token.eq(&compare_token);

    if access {
        let uuid = post_var.0.id;
        debug!("Received query to cache for uuid {}", &uuid);

        return match try_pop_from_retrieve_cache(uuid.clone()) {
            Some(res) => {
                debug!("Found in cache, returning");

                Ok(HttpResponse::Ok()
                    .append_header(("Content-Type", "text/json"))
                    .json(res))
            }
            None => {
                debug!("Not in cache maybe not yet finished");

                Ok(HttpResponse::Ok()
                    .append_header(("Content-Type", "text/json"))
                    .json(ScriptResponse {
                        success: false,
                        files: Vec::new(),
                        std_err: String::from(
                            "Cache does not (yet) have the response with this uuid",
                        ),
                        std_out: String::from(
                            "Cache does not (yet) have the response with this uuid",
                        ),
                        async_id: Some(uuid),
                    }))
            }
        };
    } else {
        // error, if the bearer-auth challenge was failed
        return Ok(HttpResponse::Unauthorized()
            .append_header(("Content-Type", "text/json"))
            .json(ScriptResponse {
                success: false,
                files: Vec::new(),
                std_err: String::from(
                    "Unauthorized: Bearer Token did not authenticate you to access this server",
                ),
                std_out: String::from(""),
                async_id: None,
            }));
    }
}
