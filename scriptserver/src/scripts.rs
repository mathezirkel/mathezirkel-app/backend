use crate::filesystem::{check_dir_exists, construct_path, mk_dir, put_file, read_file, rm};
use mathezirkel_app_scriptserver_interfaces::{ScriptFile, ScriptResponse};
use std::io::Write;
use std::process::{Command, Stdio};
use uuid::Uuid;

pub fn run_commandline_command(
    program: &str,
    program_args: &[&str],
    stdin: Option<&str>,
    execution_dir: &str,
) -> (bool, String, String) {
    let mut binding = Command::new(program);
    let mut cmd = binding
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .env_clear()
        .current_dir(execution_dir);

    for arg in program_args {
        cmd = cmd.arg(arg);
    }
    if stdin.is_some() {
        cmd = cmd.arg("/dev/stdin");
    }

    let mut child = cmd
        .spawn()
        .expect(&format!("Failed to start the command \"{}\"", program));

    let mut stdin_obj = child.stdin.take().expect("Failed to open stdin");
    let input = match stdin {
        Some(inp) => String::from(inp),
        None => String::from(""),
    };
    std::thread::spawn(move || {
        stdin_obj
            .write_all(input.as_bytes())
            .expect("Failed to write to stdin");
    });
    let output = child
        .wait_with_output()
        .expect("Failed to read stdout/stderr");

    (
        output.status.success(),
        String::from_utf8(output.stdout).unwrap_or(String::from("Failed to unwrap stdout")),
        String::from_utf8(output.stderr).unwrap_or(String::from("Failed to unwrap stderr")),
    )
}

pub fn file_using_command_line_command<'a>(
    program: &str,
    program_args: &'a [&'a str],
    stdin: Option<&str>,
    input_files: &Vec<ScriptFile>,
    output_files: &'a [&'a str],
) -> ScriptResponse {
    let id = Uuid::new_v4().to_string();

    debug!("Running file-using command in folder with id {}", &id);

    if !mk_dir(&id) {
        return ScriptResponse {
            success: false,
            files: Vec::new(),
            std_err: String::from("Failed crating main content folder"),
            std_out: String::from(""),
            async_id: None,
        };
    }

    for input_file in input_files {
        if !put_file(&input_file.content_base_64, &input_file.name, &id) {
            return ScriptResponse {
                success: false,
                files: Vec::new(),
                std_err: String::from(format!("Failed crating input file {}", input_file.name)),
                std_out: String::from(""),
                async_id: None,
            };
        }
    }

    let (success, std_out, std_err) =
        run_commandline_command(program, program_args, stdin, &construct_path(&id, ""));

    let mut output_files_output: Vec<ScriptFile> = Vec::new();
    for output_file in output_files {
        output_files_output.push(ScriptFile {
            name: String::from(*output_file),
            content_base_64: read_file(output_file, &id),
        })
    }

    if !rm(&id) {
        return ScriptResponse {
            success: false,
            files: Vec::new(),
            std_err: String::from("Failed deleting main content folder"),
            std_out: String::from(""),
            async_id: None,
        };
    }

    if check_dir_exists(&id) {
        return ScriptResponse {
            success: false,
            files: Vec::new(),
            std_err: String::from("Some un-cleanable files remained after running"),
            std_out: String::from(""),
            async_id: None,
        };
    }

    ScriptResponse {
        files: output_files_output,
        success,
        std_out,
        std_err,
        async_id: None,
    }
}
