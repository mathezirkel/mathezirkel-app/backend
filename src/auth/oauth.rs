use crate::database::instructors;
use crate::db;
use crate::models::Instructor;
use diesel::prelude::*;
use lazy_static::lazy_static;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge,
    PkceCodeVerifier, RedirectUrl, RefreshToken, Scope, TokenResponse, TokenUrl,
};
use reqwest::{self};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tokio::sync::RwLock as AsyncRwLock;
use url::Url;
use uuid::Uuid;

/// Generates an Oauth Client Variable setting all necessary urls from env
///
/// # Arguments
///
/// * `hash` - In this implementation a hash is accompanying every request for a new oauth-token. The user later needs to provide the clear-text that generates the hash in order to redeem himself and get the token from the server
///
/// # Returns
///
/// * A `BasicClient` that encapsulates this information
fn get_client(hash: &String) -> BasicClient {
    let mut redirect_url = Url::parse(
        &env::var("APPLICATION_BASE_URL").expect("URL ('APPLICATION_BASE_URL') not set in .env"),
    )
    .unwrap();
    redirect_url
        .path_segments_mut()
        .unwrap()
        .push("redirect_target")
        .push(hash.as_str());

    return BasicClient::new(
        ClientId::new(
            env::var("OAUTH_CLIENT_ID").expect("OAUTHVALUE ('OAUTH_CLIENT_ID') not set in .env"),
        ),
        Some(ClientSecret::new(env::var("OAUTH_CLIENT_SECRET").expect(
            "OAUTHVALUE ('OAUTH_CLIENT_SECRET') not set in .env",
        ))),
        AuthUrl::new(
            env::var("OAUTH_AUTH_URL").expect("OAUTHVALUE ('OAUTH_AUTH_URL') not set in .env"),
        )
        .unwrap(),
        Some(
            TokenUrl::new(
                env::var("OAUTH_TOKEN_URL")
                    .expect("OAUTHVALUE ('OAUTH_TOKEN_URL') not set in .env"),
            )
            .unwrap(),
        ),
    )
    .set_redirect_uri(RedirectUrl::new(redirect_url.to_string()).unwrap());
}

/// Generates the url that the user needs to be redirected to/needs to click in order to start the Oauth2 login process
///
/// # Arguments
///
/// * `hash` - In this implementation a hash is accompanying every request for a new oauth-token. The user later needs to provide the clear-text that generates the hash in order to redeem himself and get the token from the server
///
/// # Returns
///
/// * `String` - The url the user needs to be redirected to/needs to click
/// * `CsrfToken` - The Csrf-token later needs to be verified by comparing it to a return field of the Oauth2 response
/// * `PkceCodeVerifier` - The verifier to be used to get the Oauth2 token from the token endpoint. Pass it to `get_token_for_code`
pub fn generate_url(hash: &String) -> (String, CsrfToken, PkceCodeVerifier) {
    let client = get_client(hash);

    // Generate a PKCE challenge.
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    // Generate the full authorization URL.
    let (auth_url, csrf_token) = client
        .authorize_url(CsrfToken::new_random)
        .add_scope(Scope::new("openid".to_string()))
        .set_pkce_challenge(pkce_challenge)
        .url();

    // This is the URL you should redirect the user to, in order to trigger the authorization process
    debug!("Browse to: {}", auth_url);

    return (String::from(auth_url), csrf_token, pkce_verifier);
}

/// Gets the token from the token endpoint of the Oauth2 server, before it verifies the PKCE Challenge.
/// The CSRF-Challenge needs to be verified SEPARATELY, make sure to do this.
///
/// # Arguments
///
/// * `hash` - In this implementation a hash is accompanying every request for a new oauth-token. The user later needs to provide the clear-text that generates the hash in order to redeem himself and get the token from the server
/// * `code` - code return value of the Oauth2 Process, can be redeemed at its token endpoint to get a new Bearer-Token (this function)
/// * `pkce_verifier_secret` - the string obtained by calling `.secret()` on a `PkceCodeVerifier` because this value is stored more easily as `PkceCodeVerifier`s cannot be cloned
///
/// # Returns
///
/// * `bool` - `true` on Success (Sets Access-Token-Secret Output), `false` on any kind of Error (Sets Error-Message Output)
/// * `(String, String)` - A tuple of the codes: (The Access-Token-Secret, The Refresh-Token-Secret)
/// * `String` - The Error Message on Error, "" on Success
pub async fn get_token_for_code(
    hash: &String,
    code: &String,
    pkce_verifier_secret: &String,
) -> (bool, (String, String), String) {
    // Now you can trade it for an access token.
    let token_result = get_client(hash)
        .exchange_code(AuthorizationCode::new(String::from(code)))
        // Set the PKCE code verifier.
        .set_pkce_verifier(PkceCodeVerifier::new(pkce_verifier_secret.to_string()))
        .request_async(async_http_client)
        .await;

    // Unwrapping token_result will either produce a Token or a RequestTokenError.
    let content = match token_result {
        Ok(content) => (
            true,
            (
                content.access_token().secret().to_string(),
                match content.refresh_token() {
                    None => String::from(""),
                    Some(refresh_token) => refresh_token.secret().to_string(),
                },
            ),
            String::from(""),
        ),
        Err(error) => {
            error!("{:?}", error);
            (
                false,
                (String::from(""), String::from("")),
                error.to_string(),
            )
        }
    };
    return content;
}

/// Gets a new token from the token endpoint of the Oauth2 server.
/// This uses a refresh token for authentication
///
/// # Arguments
///
/// * `refresh_token` - refresh_token return value of the Oauth2 Process, can be redeemed at its token endpoint to get a new Bearer-Token (this function)
///
/// # Returns
///
/// * `bool` - `true` on Success (Sets Access-Token-Secret Output), `false` on any kind of Error (Sets Error-Message Output)
/// * `(String, String)` - A tuple of the codes: (The Access-Token-Secret, The Refresh-Token-Secret)
/// * `String` - The Error Message on Error, "" on Success
pub async fn get_token_for_refresh_token(
    refresh_token: &String,
) -> (bool, (String, String), String) {
    // Now you can trade it for an access token.
    let token_result = get_client(&String::from(""))
        .exchange_refresh_token(&RefreshToken::new(String::from(refresh_token)))
        .request_async(async_http_client)
        .await;

    // Unwrapping token_result will either produce a Token or a RequestTokenError.
    let content = match token_result {
        Ok(content) => (
            true,
            (
                content.access_token().secret().to_string(),
                match content.refresh_token() {
                    None => String::from(""),
                    Some(refresh_token) => refresh_token.secret().to_string(),
                },
            ),
            String::from(""),
        ),
        Err(error) => {
            error!("{:?}", error);
            (
                false,
                (String::from(""), String::from("")),
                error.to_string(),
            )
        }
    };
    return content;
}

pub enum Role {
    ReadParticipant,
    WriteParticipant,
    DeleteRecords,
    WriteEvents,
    Export,
    ReadInstructor,
    WriteInstructor,
    ManageSignup,
    ManageZirkelPlan,
}
impl Role {
    pub fn value(&self) -> String {
        match self {
            Role::ReadParticipant => String::from("readparticipant"),
            Role::WriteParticipant => String::from("writeparticipant"),
            Role::DeleteRecords => String::from("deleterecords"),
            Role::WriteEvents => String::from("writeevents"),
            Role::Export => String::from("export"),
            Role::ReadInstructor => String::from("readinstructor"),
            Role::WriteInstructor => String::from("writeinstructor"),
            Role::ManageSignup => String::from("managesignup"),
            Role::ManageZirkelPlan => String::from("managezirkelplan"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct ServerClaims {
    // JWT claims
    sub: String,
    name: String,
    preferred_username: String,
    given_name: String,
    family_name: String,
    email_verified: bool,
    #[serde(default)] // is not sent by server if empty array
    roles: Vec<String>,
}

// there is no struct inheritance... so keep this in sync with the one above. https://stackoverflow.com/questions/32552593/is-it-possible-for-one-struct-to-extend-an-existing-struct-keeping-all-the-fiel
#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    // JWT claims
    sub: String,
    name: String,
    preferred_username: String,
    given_name: String,
    family_name: String,
    email_verified: bool,
    #[serde(default)] // is not sent by server if empty array
    pub roles: Vec<String>,
    // additionally injected from database
    pub instructor_uuid: Option<Uuid>,
}

/// Verifies whether the token provided is considered valid by the server and passed the role check if set
///
/// # Arguments
///
/// * `token` - The Bearer-Token that should be verified
/// * `role` - If set to `None`, the check will pass as soon as the token is accepted by the server. If set to `Role` member, the Claims need to contain this role or access will not be granted.
///
/// # Returns
///
/// * `bool`: `true` if all checks pass, `false` if checks fail or role is not given
/// * `Option<Claims>`: if the access is given, this returns the Claims (given by the auth-server not the token, so update of access and revocation can be done on the fly)
pub async fn verify_bearer_token(token: String, role: Option<Role>) -> (bool, Option<Claims>) {
    if token.trim() == "EMPTYREPLACEMENTTOKEN" {
        debug!("Access denied because definite empty token placeholder");
        return (false, None);
    }

    let url = Url::parse(
        &env::var("OAUTH_USERINFO_URL").expect("OAUTHVALUE ('OAUTH_USERINFO_URL') not set in .env"),
    )
    .unwrap();

    // ! Overwrite dev mode/unit tests only
    if env::var("DISABLE_OAUTH_FOR_DEVELOPMENT")
        .or::<String>(Ok(String::from("false")))
        .unwrap()
        == String::from("true")
    {
        warn!("Authentication in Development overwrite, make sure this is what you want. NEVER use in production!");
        if token == String::from("development_token") {
            warn!("Development Access granted.");
            return (
                true,
                Some(Claims {
                    sub: String::from("asd"),
                    name: String::from("TESTUSER; DEVCONFIG"),
                    preferred_username: String::from("TESTUSER; DEVCONFIG"),
                    given_name: String::from("TESTUSER; DEVCONFIG"),
                    family_name: String::from("TESTUSER; DEVCONFIG"),
                    email_verified: true,
                    instructor_uuid: None,
                    roles: [
                        Role::ReadParticipant.value(),
                        Role::WriteParticipant.value(),
                        Role::DeleteRecords.value(),
                        Role::WriteEvents.value(),
                        Role::Export.value(),
                        Role::ReadInstructor.value(),
                        Role::WriteInstructor.value(),
                        Role::ManageSignup.value(),
                        Role::ManageZirkelPlan.value(),
                    ]
                    .to_vec(),
                }),
            );
        }
    }

    let (check_success, json_response_option, error_option) =
        check_bearer_request_cached(&url, &token).await;

    if check_success {
        let json_response = json_response_option.unwrap(); // must work, is only set on success

        if json_response.sub.is_empty() || json_response.sub.len() < 5 {
            // 5 is random assumption so that it cant be "none" or stuff like this

            // this is logged, both if the oauth server produced an error but sent an empty/incomplete message
            error!("Oauth server responded Request ok but no valid json or no full response, check if all fields are getting sent by endpoint!");
            return (false, None);
        }

        if role.is_some() {
            debug!("Checking for role access");

            if !json_response.roles.contains(&role.unwrap().value()) {
                error!("User has access, but not sufficient role");
            }
        }

        // find the corresponding instructor-uuid in the database, if possible
        let instructor_uuid = match &mut db::connection() {
            Err(_) => None,
            Ok(conn) => {
                match instructors::table
                    .filter(instructors::keycloak_username.eq(&json_response.preferred_username))
                    .first::<Instructor>(conn)
                {
                    Err(_) => None,
                    Ok(instr) => Some(instr.id),
                }
            }
        };

        // the process succeeded
        debug!("User authenticated successfully");

        // make sure to add reading roles if has writing roles (otherwise frontend breaks)
        let mut roles = json_response.roles;
        if roles.contains(&Role::WriteInstructor.value())
            && !roles.contains(&Role::ReadInstructor.value())
        {
            roles.push(Role::ReadInstructor.value());
        }
        if roles.contains(&Role::WriteParticipant.value())
            && !roles.contains(&Role::ReadParticipant.value())
        {
            roles.push(Role::ReadParticipant.value());
        }

        return (
            true,
            Some(Claims {
                email_verified: json_response.email_verified,
                family_name: json_response.family_name,
                given_name: json_response.given_name,
                name: json_response.name,
                preferred_username: json_response.preferred_username,
                roles,
                sub: json_response.sub,
                instructor_uuid: instructor_uuid,
            }),
        );
    }

    let text = error_option.unwrap(); // must work, is only set on non-success
    error!("Userinfo Request did not succeed: {}", text);
    return (false, None);
}

lazy_static! {
    static ref TOKEN_RESPONSE_CACHE: Arc<AsyncRwLock<HashMap<String, (SystemTime, Option<ServerClaims>,)>>> =
        Arc::new(AsyncRwLock::new(HashMap::new()));
}

async fn check_bearer_request_cached(
    url: &Url,
    token: &String,
) -> (bool, Option<ServerClaims>, Option<String>) {
    let mut cache = TOKEN_RESPONSE_CACHE.write().await;

    // make sure to init the cache if applicable
    if !cache.contains_key(token) {
        cache.insert(String::from(token), (UNIX_EPOCH, None));
    }

    // Get what is in cache
    let (timestamp, server_claims_option) = cache
        .get(token)
        .unwrap() // was just set, or already present, so can be safely unwrapped
        .clone();

    let time_difference_that_server_claims_may_be_cached_in_seconds: u64 = 15; // TODO extract to env parameter

    let entry_is_recent_enough = timestamp.clone()
        > (SystemTime::now()
            - Duration::from_secs(time_difference_that_server_claims_may_be_cached_in_seconds));

    let result: (bool, Option<ServerClaims>, Option<String>);
    if entry_is_recent_enough {
        // return the cached entry

        match server_claims_option {
            None => {
                result = (false, None, Some(String::from("Cache-Timestamp indicated there should be a ServerClaims cached. But none was found")));
            }
            Some(cached_server_claims) => {
                debug!("Authentication from cached userinfo");
                result = (true, Some(cached_server_claims.clone()), None);
            }
        }
    } else {
        // need to query server for data
        let (
            new_request_success,
            new_request_server_claims_option,
            new_request_error_string_option,
        ) = check_bearer_request(url, token).await;

        // add to the cache
        if new_request_success {
            let server_claims_to_cache = new_request_server_claims_option.as_ref().unwrap().clone(); // success means, this must be correctly filled and can be unwrapped definitely

            let mutable_cache_element = cache.get_mut(token).unwrap(); // This must be set, because was at the beginning and we hold a write lock
            mutable_cache_element.0 = SystemTime::now();
            mutable_cache_element.1 = Some(server_claims_to_cache);
            debug!("Updated userinfo cache");
        }

        let keys: Vec<String> = cache.keys().cloned().collect();
        for key in keys {
            let timestamp_to_potentially_clear = cache
                .get(&key)
                .unwrap() // we got keys from the collection itself, so all of them should resolve to filled
                // Only this region deletes and we hold a write lock, so this must be set
                .0
                .clone();
            if timestamp_to_potentially_clear < (SystemTime::now() - Duration::from_secs(3600)) {
                cache.remove(&key);
                debug!("Cleared out old token cache values from RAM");
            }
        }

        result = (
            new_request_success,
            new_request_server_claims_option,
            new_request_error_string_option,
        )
    }

    return result;
}

async fn check_bearer_request(
    url: &Url,
    token: &String,
) -> (bool, Option<ServerClaims>, Option<String>) {
    // Http POST request
    let client = reqwest::Client::new();
    let response = match client
        .post(url.to_string())
        .form(&[("access_token", token)])
        .send()
        .await
    {
        Ok(response) => response,
        Err(error) => {
            // If we are here, the Oauth2 server didn't respond at all, which indicates something is broken
            error!(
                "Userinfo Request did not respond, maybe check endpoint: {}",
                error
            );
            return (
                false,
                None,
                Some(String::from(format!("Reqwest Error {}", error))),
            );
        }
    };

    if response.status().is_success() {
        // request Succeeded, try parsing json
        let json_response =
            response
                .json::<ServerClaims>()
                .await
                .unwrap_or_else(|_| ServerClaims {
                    sub: String::from(""),
                    name: String::from(""),
                    preferred_username: String::from(""),
                    given_name: String::from(""),
                    family_name: String::from(""),
                    email_verified: false,
                    roles: [].to_vec(),
                });

        return (true, Some(json_response), None);
    }

    // If we fell through here, the Oauth2 server responded with an error
    // This may be because the request is 401-Unauthorized
    return (
        false,
        None,
        Some(
            response
                .text()
                .await
                .unwrap_or_else(|_| String::from("Could not get an error-response-reason")),
        ),
    );
}
