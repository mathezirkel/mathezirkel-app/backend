use crate::auth::oauth::{
    generate_url, get_token_for_code, get_token_for_refresh_token, verify_bearer_token,
};
use crate::auth::{hash_sha256, Claims};
use crate::web;
use actix_web::{http::header::ContentType, Error, HttpResponse, Responder};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use lazy_static::lazy_static;
use oauth2::{CsrfToken, PkceCodeVerifier};
use serde::Serialize;
use std::collections::HashMap;
use std::env;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};
use url::Url;
use urlencoding::encode;

#[derive(Serialize)]
struct RedeemResponse {
    success: bool,
    token: String,
    refresh_token: Option<String>,
}

// The Hashmap that serves as the temporary application memory for the auth-process.
// Because of this, the auth secrets never need to be written to the database, so there is no need to store them safely
lazy_static! {
    static ref HASH_PROCESS_VARS: Arc<Mutex<
        HashMap<
            String,
            (
                SystemTime,
                CsrfToken,
                PkceCodeVerifier,
                Option<String>, // the access token
                Option<String> // the refresh token
            ),
        >>,
    > = Arc::new(Mutex::new(HashMap::new()));
}

/// Actix answerer-callback to the `/request_token` URL (GET)
///
/// # Arguments
///
/// * `query` - Actix Query variables
pub async fn request_token(query: web::Query<HashMap<String, String>>) -> impl Responder {
    debug!("Token was requested");

    remove_old_process_values();

    // process current hash
    let hash = query.get("hash").map_or(String::from(""), String::from);

    let mut worked: bool = true;
    if hash.is_empty() || hash.len() != 64 {
        worked = false;
    }

    let (redirection_href, csrf_token, pkce_verifier) = generate_url(&hash);

    HASH_PROCESS_VARS.lock().unwrap().insert(
        hash.clone(),
        (SystemTime::now(), csrf_token, pkce_verifier, None, None),
    );

    let mut html = String::from(include_str!("./oauth_screen.html"));
    if worked {
        html = html.replace(
            "{{message1}}",
            "Request access to the database for the Mathezirkel-App-Client.",
        );
        html = html.replace("{{message2}}", "Check the hash beforehand in order to make sure to authenticate the correct application!");
        html = html.replace("{{messagebutton}}", "Request Access via OAUTH2");
        html = html.replace("{{hash}}", &hash);
        html = html.replace("{{href}}", &redirection_href);
    } else {
        html = html.replace(
            "{{message1}}",
            "Request access to the database for the Mathezirkel-App-Client.",
        );
        html = html.replace(
            "{{message2}}",
            "The hash does not have the correct format to proceed.",
        );
        html = html.replace("{{hash}}", "SOMETHING IS NOT RIGHT; RETRY");
        html = html.replace("{{buttonattr}}", "hidden");
    }

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(html)
}

/// Actix answerer-callback to the `/redirect_target/{hash}` URL (GET)
///
/// # Arguments
///
/// * `path` - Actix Path variables
/// * `query` - Actix Query variables
pub async fn redirect_target(
    path: web::Path<(String,)>,
    query: web::Query<HashMap<String, String>>,
) -> impl Responder {
    debug!("Redirection arrived after authentication");

    // Extract Values from HTML Request
    let (hash,) = path.into_inner();
    let error = query
        .get("error")
        .map_or(String::from(""), String::from)
        .clone();
    let csrf_state = query
        .get("state")
        .map_or(String::from(""), String::from)
        .clone();
    let _session_state = query // currently not used, could be used in conjunction with `offline_access` of Oauth2 to get whether there is a session present.
        .get("session_state")
        .map_or(String::from(""), String::from)
        .clone();
    let code = query
        .get("code")
        .map_or(String::from(""), String::from)
        .clone();

    let mut html = String::from(include_str!("./oauth_screen.html"));
    if hash.is_empty() || hash.len() != 64 {
        // hash mal-formatted, seems to have been tempered with
        error!("Hash mal-formatted, aborting.");

        html = html.replace(
            "{{message1}}",
            "Callback received from authentication server.",
        );
        html = html.replace(
            "{{message2}}",
            "The hash does not have the correct format to proceed.",
        );
        html = html.replace("{{hash}}", "SOMETHING IS NOT RIGHT; RETRY");
        html = html.replace("{{buttonattr}}", "hidden");
    } else {
        if !HASH_PROCESS_VARS.lock().unwrap().contains_key(&hash) {
            // hash not contained any longer in reference
            info!("Hash not present, do not start process, probably old.");

            html = html.replace(
                "{{message1}}",
                "Callback received from authentication server. The hash is not stored in the reference database.",
            );
            html = html.replace(
                "{{message2}}",
                "Either process may have finished (check client), or it got corrupted or the process took too long.",
            );
            html = html.replace("{{hash}}", "IF CLIENT DOESN'T WORK, RETRY");
            html = html.replace("{{buttonattr}}", "hidden");
        } else {
            if !error.is_empty() {
                // Authentication server returned error
                error!("Error from OAUTH2-Server, aborting");

                html = html.replace("{{message1}}", "Error received from authentication server.");
                html = html.replace(
                    "{{message2}}",
                    &format!(
                        "The proceeding seems to have produced an error. Message: {}",
                        error
                    ),
                );
                html = html.replace("{{hash}}", &hash);
                html = html.replace("{{buttonattr}}", "hidden");
            } else {
                if String::from(
                    HASH_PROCESS_VARS
                        .lock()
                        .unwrap()
                        .get(&hash)
                        .unwrap()
                        .1
                        .secret(),
                ) != String::from(csrf_state)
                {
                    // CSRF ERROR
                    error!("CSRF-Error, aborting");

                    html = html.replace("{{message1}}", "CSRF Information mismatch");
                    html = html.replace(
                        "{{message2}}",
                        "This may be a bug or indication of an attack (Cross-Site-Request-Forgery)",
                    );
                    html = html.replace("{{hash}}", &hash);
                    html = html.replace("{{buttonattr}}", "hidden");
                } else {
                    // get the token
                    let pkce_verifier_secret = String::from(
                        HASH_PROCESS_VARS
                            .lock()
                            .unwrap()
                            .get(&hash)
                            .unwrap()
                            .2
                            .secret(),
                    );

                    let mut success = true;
                    let mut token = String::from("");
                    let mut refresh_token = String::from("");
                    let mut error_message = String::from("");
                    if HASH_PROCESS_VARS
                        .lock()
                        .unwrap()
                        .get(&hash)
                        .unwrap()
                        .3
                        .is_none()
                    {
                        debug!(
                            "Bearer-Token for hash not found in storage, requesting from Server."
                        );
                        // only redeem the first time
                        (success, (token, refresh_token), error_message) =
                            get_token_for_code(&hash, &code, &pkce_verifier_secret).await;
                    }

                    if success {
                        // write the tokens for polling function to get
                        if HASH_PROCESS_VARS
                            .lock()
                            .unwrap()
                            .get(&hash)
                            .unwrap()
                            .3
                            .is_none()
                        {
                            // not delete if present

                            // write into the storage
                            let mut map_guard = HASH_PROCESS_VARS.lock().unwrap();
                            map_guard.get_mut(&hash).unwrap().3 = Some(token);
                        }
                        if refresh_token.len() != 0 {
                            // write refresh_token, if it was supported/provided by the oauth server
                            if HASH_PROCESS_VARS
                                .lock()
                                .unwrap()
                                .get(&hash)
                                .unwrap()
                                .4
                                .is_none()
                            {
                                // not delete if present

                                // write into the storage
                                let mut map_guard = HASH_PROCESS_VARS.lock().unwrap();
                                map_guard.get_mut(&hash).unwrap().4 = Some(refresh_token);
                            }
                        }

                        debug!("Successfully generated BearerToken");

                        // message
                        html = html.replace(
                            "{{message1}}",
                            "SUCCESS. Callback received from authentication server.",
                        );
                        html = html.replace("{{message2}}", "Authenticated the client with the following hash. The client should get the token on its own. This page/tab will attempt to close itself.");
                        html = html.replace("{{hash}}", &hash);
                        html = html.replace("{{buttonattr}}", "hidden");
                        html =
                            html.replace("{{script1}}", "setTimeout(() => window.close(), 700);");
                    } else {
                        // Token redeem error, report
                        error!("Token redeem error on OAUTH2 Server");

                        html = html.replace(
                                "{{message1}}",
                                "Error. Process came back positive, but code not redeemed for a token. May be OAUTH Server Error",
                            );
                        html = html.replace("{{message2}}", &format!("Error: {}", error_message));
                        html = html.replace("{{hash}}", hash.as_str());
                        html = html.replace("{{buttonattr}}", "hidden");
                    }
                }
            }
        }
    }

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(html)
}

/// Actix answerer-callback to the `/redeem_hash` URL (GET)
///
/// # Arguments
///
/// * `query` - Actix Query variables
pub async fn redeem_hash(query: web::Query<HashMap<String, String>>) -> impl Responder {
    debug!("Plain token was provided to try to redeem hash");

    let plain_temp_token = query.get("token").map_or(String::from(""), String::from);
    let hashed_from_offered = hash_sha256(&plain_temp_token);

    let mut worked: bool = false;
    if !hashed_from_offered.is_empty() && hashed_from_offered.len() == 64 {
        // check if hash is recent enough
        if timestamp_valid(&hashed_from_offered) {
            // checks succeeded
            debug!("Hash to plain token was found in storage and has valid timestamp");

            // inject TOKEN for development (so that the application works while it has no connection to the oauth server, like dev or unit tests)
            if env::var("DISABLE_OAUTH_FOR_DEVELOPMENT")
                .or::<String>(Ok(String::from("false")))
                .unwrap()
                == String::from("true")
            {
                warn!("DISABLE_OAUTH_FOR_DEVELOPMENT overwrite in .env set to true");
                HASH_PROCESS_VARS.lock().unwrap().insert(
                    hashed_from_offered.clone(),
                    (
                        SystemTime::now(),
                        CsrfToken::new(String::from("")),
                        PkceCodeVerifier::new(String::from("")),
                        Some(String::from("development_token")),
                        None,
                    ),
                );
                worked = true; // ! ONLY if specifically set to be disabled (development and Unit tests), Will give hardcoded User back
            }

            // check if the Process of getting the oauth token from the server already finished
            if HASH_PROCESS_VARS
                .lock()
                .unwrap()
                .contains_key(&hashed_from_offered)
                && HASH_PROCESS_VARS
                    .lock()
                    .unwrap()
                    .get(&hashed_from_offered)
                    .unwrap()
                    .3
                    .is_some()
            {
                debug!("Token present and auth worked, give it back");
                worked = true;
            } else {
                debug!("Check successful, but Token not present: Probably Oauth2 Process not finished yet because of User/Web delay");
            }
        } else {
            debug!("Hash older than SECONDS_TO_WAIT_FOR_TOKEN_REDEEM or missing");

            remove_old_process_values();
        }
    }

    // only return data if all checks passed
    if worked {
        let token_to_return_tuple = clear_entries(&hashed_from_offered);

        // give the info out
        let response = RedeemResponse {
            success: true,
            token: token_to_return_tuple.0,
            refresh_token: token_to_return_tuple.1,
        };
        return web::Json(response);
    } else {
        // Error
        let response = RedeemResponse {
            success: false,
            token: String::from(""),
            refresh_token: None,
        };
        return web::Json(response);
    }
}

/// Actix answerer-callback to the `/refresh_token` URL (GET)
///
/// # Arguments
///
/// * `query` - Actix Query variables
pub async fn refresh_token(query: web::Query<HashMap<String, String>>) -> impl Responder {
    debug!("Attempt to refresh the token was started");

    let plain_refresh_token = query
        .get("refresh_token")
        .map_or(String::from(""), String::from);

    let (worked, (new_token, new_refresh_token), error_string) =
        get_token_for_refresh_token(&plain_refresh_token).await;

    let refresh_token_response;
    if new_refresh_token.len() == 0 {
        refresh_token_response = None;
    } else {
        refresh_token_response = Some(new_refresh_token)
    }

    // only return data if all checks passed
    if worked {
        // give the info out
        let response = RedeemResponse {
            success: true,
            token: new_token,
            refresh_token: refresh_token_response,
        };
        return web::Json(response);
    } else {
        error!("The refresh_token process errored: {}", error_string);

        // Error
        let response = RedeemResponse {
            success: false,
            token: String::from(""),
            refresh_token: None,
        };
        return web::Json(response);
    }
}

/// Remove the RAM-stored data for the auth process
///
/// # Arguments
///
/// * `hash_key` - The key that all corresponding data should be removed to
///
/// # Returns
///
/// * `(String, Option<String>)` - The token (if any, otherwise "") that was removed from HASH_TOKENS, additionally (if present) the refresh token
fn clear_entries(hash_key: &String) -> (String, Option<String>) {
    let mut map_guard = HASH_PROCESS_VARS.lock().unwrap();

    if map_guard.contains_key(hash_key) {
        let token_to_return_tuple = map_guard.remove(hash_key).unwrap(); // remove while getting element

        return (
            token_to_return_tuple.3.unwrap_or(String::from("")),
            token_to_return_tuple.4,
        );
    }

    return (String::from(""), None);
}

/// Check if the TIMESTAMP of the key is considered "fresh-enough" to be used in processes
///
/// # Arguments
///
/// * `hash_key` - The key that should be checked
fn timestamp_valid(hash_key: &String) -> bool {
    if !HASH_PROCESS_VARS.lock().unwrap().contains_key(hash_key) {
        return false;
    }

    let stored_time = HASH_PROCESS_VARS
        .lock()
        .unwrap()
        .get(hash_key)
        .unwrap()
        .0
        .clone();

    // check if hash is recent enough
    return stored_time
        > (SystemTime::now()
            - Duration::from_secs(
                env::var("SECONDS_TO_WAIT_FOR_TOKEN_REDEEM")
                    .expect("delay ('SECONDS_TO_WAIT_FOR_TOKEN_REDEEM') not set in .env")
                    .parse::<u64>()
                    .expect("delay ('SECONDS_TO_WAIT_FOR_TOKEN_REDEEM') must be u64"),
            ));
}

/// Take care of old hashes (important, so that you can not build up memory requirements of the server by spamming the endpoint)
fn remove_old_process_values() {
    let keys: Vec<String> = HASH_PROCESS_VARS.lock().unwrap().keys().cloned().collect();
    for key in keys {
        if !timestamp_valid(&key) {
            debug!("Cleared out old process values from RAM");

            clear_entries(&key);
        }
    }
}

#[derive(Serialize)]
struct UserInfo {
    access: bool,
    user: Option<Claims>,
}

/// Actix answerer-callback to the `/redeem_hash` URL (GET)
///
/// # Arguments
///
/// * `auth` - Actix Bearer-Auth variables
pub async fn user_info(auth: BearerAuth) -> Result<HttpResponse, Error> {
    debug!("Userinfo was requested");

    let bearer_token = String::from(auth.token());

    let (access, claims) = verify_bearer_token(bearer_token, None).await;

    // only return data if all checks passed
    if access {
        // Access
        let response = UserInfo {
            access: true,
            user: Some(claims.unwrap()),
        };

        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .json(response))
    } else {
        // No Access
        let response = HttpResponse::Unauthorized()
            .append_header(("Content-Type", "text/plain"))
            .body("Unauthorized: Bearer Token did not authenticate you to access this server");

        Ok(response)
    }
}

/// Actix answerer-callback to the `/logout` URL (GET)
pub async fn logout() -> Result<HttpResponse, Error> {
    debug!("Logout was requested");

    let mut post_logout_redirect_url = Url::parse(
        &env::var("APPLICATION_BASE_URL").expect("URL ('APPLICATION_BASE_URL') not set in .env"),
    )
    .unwrap();
    post_logout_redirect_url
        .path_segments_mut()
        .unwrap()
        .push("post_logout_redirect");

    Ok(HttpResponse::TemporaryRedirect()
        .append_header((
            "Location",
            String::from(format!(
                "{}?post_logout_redirect_uri={}&client_id={}",
                &env::var("OAUTH_LOGOUT_URL")
                    .expect("OAUTHVALUE ('OAUTH_LOGOUT_URL') not set in .env"),
                &encode(&post_logout_redirect_url.to_string()).to_string(),
                &env::var("OAUTH_CLIENT_ID")
                    .expect("OAUTHVALUE ('OAUTH_CLIENT_ID') not set in .env")
            )),
        ))
        .finish())
}

/// Actix answerer-callback to the `/post_logout_redirect` URL (GET)
///
pub async fn post_logout_redirect() -> impl Responder {
    debug!("Logout has been finished successfully");

    let mut html = String::from(include_str!("./oauth_screen.html"));
    html = html.replace("{{message1}}", "Successfully logged out");
    html = html.replace(
        "{{message2}}",
        "This page/tab will attempt to close itself.",
    );
    html = html.replace("{{script1}}", "setTimeout(() => window.close(), 700);");
    html = html.replace("{{buttonattr}}", "hidden");
    html = html.replace("{{hashattr}}", "hidden");

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(html)
}

/// Actix answerer-callback to the `/` URL (GET)
pub async fn index() -> impl Responder {
    let mut html = String::from(include_str!("./oauth_screen.html"));

    html = html.replace(
        "{{message1}}",
        "On its own, the backend is not usable. You require the app-client to use this database.",
    );
    html = html.replace(
        "{{message2}}",
        "Click the button to be reditected to the hosted PWA-client for this application.",
    );
    html = html.replace("{{hashattr}}", "hidden");
    html = html.replace("{{href}}", "https://app.mathezirkel-augsburg.de");
    html = html.replace("{{buttonattr}}", "");
    html = html.replace("{{messagebutton}}", "PWA-client HERE");

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(html)
}
