use rand_seeder::rand_core::RngCore;
use rand_seeder::{Seeder, SipRng};
use std::collections::HashMap;

use uuid::Uuid;

use crate::models::Event;

pub fn generate_colors_for_zirkelplan_entries(event_id: &Uuid) -> HashMap<Uuid, String> {
    let event = match Event::find(event_id.clone()) {
        Ok(event) => event,
        Err(_) => return HashMap::new(),
    };
    let slots = match event.zirkel_plan_slots() {
        Ok(slots) => match slots {
            Some(slots) => slots,
            None => return HashMap::new(),
        },
        Err(_) => return HashMap::new(),
    };
    let entries = slots
        .into_iter()
        .flat_map(|slot| match slot.zirkel_plan_entries() {
            Ok(entries) => match entries {
                Some(entries) => entries,
                None => return Vec::new(),
            },
            Err(_) => return Vec::new(),
        })
        .collect::<Vec<_>>();

    let mut id_seed_instruction_vec: Vec<(Uuid, String)> = Vec::new();
    for entry in entries {
        let mut strings: Vec<String> = entry
            .instructor_extension_uuids
            .iter()
            .map(|uuid| uuid.to_string())
            .collect();
        strings.sort();
        let seed: String = strings.join("");

        id_seed_instruction_vec.push((entry.id.clone(), seed));
    }

    return generate_colors_for_uuid_seed_combination(id_seed_instruction_vec);
}

pub fn generate_colors_for_fractivity_distributions(event_id: &Uuid) -> HashMap<Uuid, String> {
    let event = match Event::find(event_id.clone()) {
        Ok(event) => event,
        Err(_) => return HashMap::new(),
    };
    let distributions: Vec<crate::models::FractivityDistribution> =
        match event.fractivity_distributions() {
            Ok(distributions) => match distributions {
                Some(distributions) => distributions,
                None => return HashMap::new(),
            },
            Err(_) => return HashMap::new(),
        };

    let mut id_seed_instruction_vec: Vec<(Uuid, String)> = Vec::new();
    for distribution in distributions {
        id_seed_instruction_vec.push((
            distribution.id.clone(),
            distribution.fractivity_entry().topic.clone(),
        ));
    }

    return generate_colors_for_uuid_seed_combination(id_seed_instruction_vec);
}

fn generate_colors_for_uuid_seed_combination(inputs: Vec<(Uuid, String)>) -> HashMap<Uuid, String> {
    let mut new_event_wide_cache: HashMap<Uuid, String> = HashMap::new();

    let mut event_color_list: Vec<(u32, u32, u32)> = get_new_copy_of_color_list();
    let mut seed_to_color_store: HashMap<String, (u32, u32, u32)> = HashMap::new();
    for (id, seed) in inputs {
        let mut closest_color: (u32, u32, u32);
        if seed_to_color_store.contains_key(&seed) {
            let test = seed_to_color_store.get(&seed).unwrap();
            closest_color = test.clone();
        } else {
            let (r, g, b) = generate_random_color(&seed);

            closest_color = (r, g, b);
            if !event_color_list.is_empty() {
                let mut min_distance = u32::MAX;
                let mut closest_color_index = 0;
                for (index, &(r2, g2, b2)) in event_color_list.iter().enumerate() {
                    let r_diff = r as i32 - r2 as i32;
                    let g_diff = g as i32 - g2 as i32;
                    let b_diff = b as i32 - b2 as i32;
                    let distance: u32 =
                        (r_diff * r_diff + g_diff * g_diff + b_diff * b_diff) as u32;

                    if distance < min_distance {
                        min_distance = distance;
                        closest_color_index = index;
                    }
                }

                // Remove the closest color from the event_color_list and store in case exact same color is required later
                closest_color = event_color_list.remove(closest_color_index);
                seed_to_color_store.insert(seed, closest_color.clone());
            }
        }

        new_event_wide_cache.insert(id, color_to_string(closest_color));
    }

    return new_event_wide_cache;
}

fn generate_random_color(seed: &String) -> (u32, u32, u32) {
    let mut rng: SipRng = Seeder::from(seed).make_rng();
    let lighter_offset = 40;

    let r = lighter_offset + (rng.next_u32() % (255 - lighter_offset));
    let g = lighter_offset + (rng.next_u32() % (255 - lighter_offset));
    let b = lighter_offset + (rng.next_u32() % (255 - lighter_offset));

    return (r, g, b);
}

fn get_new_copy_of_color_list() -> Vec<(u32, u32, u32)> {
    return vec![
        (135, 206, 250),
        (176, 226, 255),
        (255, 193, 193),
        (255, 165, 79),
        (255, 187, 255),
        (224, 102, 255),
        (238, 210, 255),
        (255, 106, 106),
        (214, 96, 96),
        (175, 217, 166),
        (159, 173, 211),
        (202, 232, 249),
        (190, 166, 209),
        (230, 129, 120),
        (252, 156, 115),
        (196, 184, 255),
        (234, 120, 140),
        (253, 192, 28),
        (233, 206, 113),
        (219, 168, 46),
        (167, 211, 72),
        (191, 235, 46),
        (255, 220, 60),
        (62, 149, 155),
        (216, 213, 236),
        (238, 173, 118),
        (255, 127, 80),
        (144, 238, 144),
        (166, 230, 166),
        (151, 213, 238),
        (222, 184, 135),
        (197, 250, 197),
        (255, 102, 153),
        (130, 189, 248),
        (204, 255, 153),
        (179, 255, 102),
        (247, 83, 193),
        (238, 149, 210),
        (229, 255, 173),
        (240, 144, 121),
        (230, 184, 175),
        (147, 196, 125),
        (245, 134, 3),
        (248, 248, 37),
        (91, 236, 236),
        (247, 27, 244),
        (94, 104, 247),
        (0, 247, 120),
        (106, 247, 125),
        (250, 85, 233),
        (227, 214, 93),
        (227, 120, 213),
        (129, 227, 198),
        (143, 157, 227),
        (191, 122, 225),
        (232, 107, 169),
        (140, 212, 182),
        (227, 127, 174),
        (127, 84, 255),
    ];
}

fn color_to_string(color: (u32, u32, u32)) -> String {
    let (r, g, b) = color;

    String::from(format!("#{:02x}{:02x}{:02x}", r, g, b))
}
