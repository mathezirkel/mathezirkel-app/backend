CREATE TYPE sex_enum AS ENUM ('male', 'female', 'diverse', 'redacted');
CREATE TYPE nutrition_enum AS ENUM ('omnivore', 'vegetarian', 'vegan', 'redacted');
CREATE TYPE classyear_enum AS ENUM ('class_5', 'class_6', 'class_7', 'class_8', 'class_9', 'class_10', 'class_11', 'class_12', 'class_13', 'class_info');
CREATE TYPE traveltype_enum AS ENUM ('private', 'bus');
CREATE TYPE eventtype_enum AS ENUM ('closing_event', 'zirkel', 'math_day', 'winter_camp', 'math_camp');
