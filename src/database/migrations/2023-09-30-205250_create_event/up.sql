CREATE TABLE "events"
(
    id UUID DEFAULT uuid_generate_v4 (),
    type eventtype_enum NOT NULL,
    year INT NOT NULL,
    name TEXT NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    default_fee INT NOT NULL DEFAULT 0,

    PRIMARY KEY (id)
)
