CREATE TABLE "zirkel"
(
    id UUID DEFAULT uuid_generate_v4 (),
    event_id UUID REFERENCES events(id) NOT NULL,
    name TEXT NOT NULL,
    instructors TEXT[] NOT NULL DEFAULT array[]::text[],
    topics TEXT[] NOT NULL DEFAULT array[]::text[],
    room TEXT,

    PRIMARY KEY (id)
)
