CREATE TABLE "participants"
(
    id UUID DEFAULT uuid_generate_v4 (),
    family_name TEXT NOT NULL,
    family_name_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    family_name_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    given_name TEXT NOT NULL,
    given_name_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    given_name_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    call_name TEXT,
    call_name_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    call_name_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    birth_date DATE NOT NULL,
    birth_date_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    birth_date_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    sex sex_enum NOT NULL,
    sex_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    sex_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    gender TEXT,
    gender_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    gender_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    street TEXT NOT NULL,
    street_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    street_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    street_number TEXT NOT NULL,
    street_number_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    street_number_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    postal_code TEXT NOT NULL,
    postal_code_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    postal_code_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    city TEXT NOT NULL,
    city_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    city_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    country TEXT NOT NULL,
    country_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    country_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    notes TEXT,
    notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION update_participants_timestamps_function()
RETURNS TRIGGER AS $$
BEGIN
    IF 
        (OLD.family_name = 'REDACTED' OR OLD.family_name = 'redacted' OR OLD.family_name = '' OR OLD.family_name IS NULL) AND 
        (NEW.family_name <> 'REDACTED' AND NEW.family_name <> 'redacted' AND NEW.family_name <> '' AND NEW.family_name IS NOT NULL) 
    THEN
        NEW.family_name_created_at = NOW(); 
    END IF;
    IF 
        NEW.family_name IS DISTINCT FROM OLD.family_name AND
        NEW.family_name <> 'REDACTED' AND NEW.family_name <> 'redacted' AND NEW.family_name <> '' AND NEW.family_name IS NOT NULL
    THEN
        NEW.family_name_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.given_name = 'REDACTED' OR OLD.given_name = 'redacted' OR OLD.given_name = '' OR OLD.given_name IS NULL) AND 
        (NEW.given_name <> 'REDACTED' AND NEW.given_name <> 'redacted' AND NEW.given_name <> '' AND NEW.given_name IS NOT NULL) 
    THEN
        NEW.given_name_created_at = NOW(); 
    END IF;
    IF 
        NEW.given_name IS DISTINCT FROM OLD.given_name AND
        NEW.given_name <> 'REDACTED' AND NEW.given_name <> 'redacted' AND NEW.given_name <> '' AND NEW.given_name IS NOT NULL
    THEN
        NEW.given_name_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.call_name = 'REDACTED' OR OLD.call_name = 'redacted' OR OLD.call_name = '' OR OLD.call_name IS NULL) AND 
        (NEW.call_name <> 'REDACTED' AND NEW.call_name <> 'redacted' AND NEW.call_name <> '' AND NEW.call_name IS NOT NULL) 
    THEN
        NEW.call_name_created_at = NOW(); 
    END IF;
    IF 
        NEW.call_name IS DISTINCT FROM OLD.call_name AND
        NEW.call_name <> 'REDACTED' AND NEW.call_name <> 'redacted' AND NEW.call_name <> '' AND NEW.call_name IS NOT NULL
    THEN
        NEW.call_name_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: DATE
    IF 
        (OLD.birth_date IS NULL OR OLD.birth_date = '0001-01-01') AND 
        (NEW.birth_date IS NOT NULL AND NEW.birth_date <> '0001-01-01') 
    THEN
        NEW.birth_date_created_at = NOW(); 
    END IF;
    IF 
        NEW.birth_date IS DISTINCT FROM OLD.birth_date AND
        NEW.birth_date IS NOT NULL AND NEW.birth_date <> '0001-01-01'
    THEN
        NEW.birth_date_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: ENUM
    IF 
        (OLD.sex = 'redacted' OR OLD.sex IS NULL) AND 
        (NEW.sex <> 'redacted' AND NEW.sex IS NOT NULL) 
    THEN
        NEW.sex_created_at = NOW(); 
    END IF;
    IF 
        NEW.sex IS DISTINCT FROM OLD.sex AND
        NEW.sex <> 'redacted' AND NEW.sex IS NOT NULL
    THEN
        NEW.sex_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.gender = 'REDACTED' OR OLD.gender = 'redacted' OR OLD.gender = '' OR OLD.gender IS NULL) AND 
        (NEW.gender <> 'REDACTED' AND NEW.gender <> 'redacted' AND NEW.gender <> '' AND NEW.gender IS NOT NULL) 
    THEN
        NEW.gender_created_at = NOW(); 
    END IF;
    IF 
        NEW.gender IS DISTINCT FROM OLD.gender AND
        NEW.gender <> 'REDACTED' AND NEW.gender <> 'redacted' AND NEW.gender <> '' AND NEW.gender IS NOT NULL
    THEN
        NEW.gender_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.street = 'REDACTED' OR OLD.street = 'redacted' OR OLD.street = '' OR OLD.street IS NULL) AND 
        (NEW.street <> 'REDACTED' AND NEW.street <> 'redacted' AND NEW.street <> '' AND NEW.street IS NOT NULL) 
    THEN
        NEW.street_created_at = NOW(); 
    END IF;
    IF 
        NEW.street IS DISTINCT FROM OLD.street AND
        NEW.street <> 'REDACTED' AND NEW.street <> 'redacted' AND NEW.street <> '' AND NEW.street IS NOT NULL
    THEN
        NEW.street_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.street_number = 'REDACTED' OR OLD.street_number = 'redacted' OR OLD.street_number = '' OR OLD.street_number IS NULL) AND 
        (NEW.street_number <> 'REDACTED' AND NEW.street_number <> 'redacted' AND NEW.street_number <> '' AND NEW.street_number IS NOT NULL) 
    THEN
        NEW.street_number_created_at = NOW(); 
    END IF;
    IF 
        NEW.street_number IS DISTINCT FROM OLD.street_number AND
        NEW.street_number <> 'REDACTED' AND NEW.street_number <> 'redacted' AND NEW.street_number <> '' AND NEW.street_number IS NOT NULL
    THEN
        NEW.street_number_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.postal_code = 'REDACTED' OR OLD.postal_code = 'redacted' OR OLD.postal_code = '' OR OLD.postal_code IS NULL) AND 
        (NEW.postal_code <> 'REDACTED' AND NEW.postal_code <> 'redacted' AND NEW.postal_code <> '' AND NEW.postal_code IS NOT NULL) 
    THEN
        NEW.postal_code_created_at = NOW(); 
    END IF;
    IF 
        NEW.postal_code IS DISTINCT FROM OLD.postal_code AND
        NEW.postal_code <> 'REDACTED' AND NEW.postal_code <> 'redacted' AND NEW.postal_code <> '' AND NEW.postal_code IS NOT NULL
    THEN
        NEW.postal_code_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.city = 'REDACTED' OR OLD.city = 'redacted' OR OLD.city = '' OR OLD.city IS NULL) AND 
        (NEW.city <> 'REDACTED' AND NEW.city <> 'redacted' AND NEW.city <> '' AND NEW.city IS NOT NULL) 
    THEN
        NEW.city_created_at = NOW(); 
    END IF;
    IF 
        NEW.city IS DISTINCT FROM OLD.city AND
        NEW.city <> 'REDACTED' AND NEW.city <> 'redacted' AND NEW.city <> '' AND NEW.city IS NOT NULL
    THEN
        NEW.city_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.country = 'REDACTED' OR OLD.country = 'redacted' OR OLD.country = '' OR OLD.country IS NULL) AND 
        (NEW.country <> 'REDACTED' AND NEW.country <> 'redacted' AND NEW.country <> '' AND NEW.country IS NOT NULL) 
    THEN
        NEW.country_created_at = NOW(); 
    END IF;
    IF 
        NEW.country IS DISTINCT FROM OLD.country AND
        NEW.country <> 'REDACTED' AND NEW.country <> 'redacted' AND NEW.country <> '' AND NEW.country IS NOT NULL
    THEN
        NEW.country_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.notes = 'REDACTED' OR OLD.notes = 'redacted' OR OLD.notes = '' OR OLD.notes IS NULL) AND 
        (NEW.notes <> 'REDACTED' AND NEW.notes <> 'redacted' AND NEW.notes <> '' AND NEW.notes IS NOT NULL) 
    THEN
        NEW.notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.notes IS DISTINCT FROM OLD.notes AND
        NEW.notes <> 'REDACTED' AND NEW.notes <> 'redacted' AND NEW.notes <> '' AND NEW.notes IS NOT NULL
    THEN
        NEW.notes_updated_at = NOW(); 
    END IF;
    

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_participants_timestamps
BEFORE UPDATE ON participants 
FOR EACH ROW EXECUTE PROCEDURE update_participants_timestamps_function();