CREATE TABLE "extensions"
(
    -- BASE-EXTENSION
    id UUID DEFAULT uuid_generate_v4 (),
    participant_id UUID REFERENCES participants(id) NOT NULL,
    event_id UUID REFERENCES events(id) NOT NULL,
    zirkel_id UUID REFERENCES zirkel(id),

    -- CLOSING_EVENT-EXTENSION

    certificate BOOL NOT NULL DEFAULT true,
    participates BOOL NOT NULL DEFAULT true,

    -- ZIRKEL-EXTENSION

    email_self TEXT,
    email_self_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    email_self_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    additional_emails TEXT[] NOT NULL DEFAULT array[]::text[],
    additional_emails_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    additional_emails_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    notes TEXT,
    notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    reason_of_signoff TEXT,
    time_of_signoff TIMESTAMP,
    class_year classyear_enum,

    -- MATH_DAY-EXTENSION

    nutrition nutrition_enum,
    nutrition_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    nutrition_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    food_restriction TEXT,
    food_restriction_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    food_restriction_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fee INT,
    fee_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fee_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    confirmed BOOL NOT NULL DEFAULT true,
    waiting_list_timestamp TIMESTAMP,

    -- WINTER_CAMP-EXTENSION

    arrival traveltype_enum,
    arrival_notes TEXT,
    arrival_notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    arrival_notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    departure traveltype_enum,
    departure_notes TEXT,
    departure_notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    departure_notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    room_partner_wishes TEXT[] NOT NULL DEFAULT array[]::text[],
    room_partner_wishes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    room_partner_wishes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    zirkel_partner_wishes TEXT[] NOT NULL DEFAULT array[]::text[],
    zirkel_partner_wishes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    zirkel_partner_wishes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    medical_notes TEXT[] NOT NULL DEFAULT array[]::text[],
    medical_notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    medical_notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    topic_wishes TEXT[] NOT NULL DEFAULT array[]::text[],
    fractivity_wishes TEXT[] NOT NULL DEFAULT array[]::text[],
    leaving_premise BOOL NOT NULL DEFAULT false,
    carpool_data_sharing BOOL NOT NULL DEFAULT false,
    remove_ticks BOOL NOT NULL DEFAULT false,
    room_number TEXT,

    -- MATH_CAMP-EXTENSION

    instruments TEXT[] NOT NULL DEFAULT array[]::text[],
    pool BOOL NOT NULL DEFAULT false,

    PRIMARY KEY (id),
    UNIQUE(participant_id, event_id) -- Only one extension per Participant and Event possible
);

CREATE OR REPLACE FUNCTION update_extensions_timestamps_function()
RETURNS TRIGGER AS $$
BEGIN
    IF 
        (OLD.email_self = 'REDACTED' OR OLD.email_self = 'redacted' OR OLD.email_self = '' OR OLD.email_self IS NULL) AND 
        (NEW.email_self <> 'REDACTED' AND NEW.email_self <> 'redacted' AND NEW.email_self <> '' AND NEW.email_self IS NOT NULL) 
    THEN
        NEW.email_self_created_at = NOW(); 
    END IF;
    IF 
        NEW.email_self IS DISTINCT FROM OLD.email_self AND
        NEW.email_self <> 'REDACTED' AND NEW.email_self <> 'redacted' AND NEW.email_self <> '' AND NEW.email_self IS NOT NULL
    THEN
        NEW.email_self_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: ARRAY
    IF 
        (OLD.additional_emails IS NULL OR OLD.additional_emails = array[]::text[]) AND 
        (NEW.additional_emails IS NOT NULL AND NEW.additional_emails <> array[]::text[]) 
    THEN
        NEW.additional_emails_created_at = NOW(); 
    END IF;
    IF 
        NEW.additional_emails IS DISTINCT FROM OLD.additional_emails AND
        NEW.additional_emails IS NOT NULL AND NEW.additional_emails <> array[]::text[]
    THEN
        NEW.additional_emails_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.notes = 'REDACTED' OR OLD.notes = 'redacted' OR OLD.notes = '' OR OLD.notes IS NULL) AND 
        (NEW.notes <> 'REDACTED' AND NEW.notes <> 'redacted' AND NEW.notes <> '' AND NEW.notes IS NOT NULL) 
    THEN
        NEW.notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.notes IS DISTINCT FROM OLD.notes AND
        NEW.notes <> 'REDACTED' AND NEW.notes <> 'redacted' AND NEW.notes <> '' AND NEW.notes IS NOT NULL
    THEN
        NEW.notes_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: Enum
    IF 
        (OLD.nutrition = 'redacted' OR OLD.nutrition IS NULL) AND 
        (NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL) 
    THEN
        NEW.nutrition_created_at = NOW(); 
    END IF;
    IF 
        NEW.nutrition IS DISTINCT FROM OLD.nutrition AND
        NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL
    THEN
        NEW.nutrition_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.food_restriction = 'REDACTED' OR OLD.food_restriction = 'redacted' OR OLD.food_restriction = '' OR OLD.food_restriction IS NULL) AND 
        (NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL) 
    THEN
        NEW.food_restriction_created_at = NOW(); 
    END IF;
    IF 
        NEW.food_restriction IS DISTINCT FROM OLD.food_restriction AND
        NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL
    THEN
        NEW.food_restriction_updated_at = NOW(); 
    END IF;

    -- CAUTION: Integer
    IF 
        (OLD.fee IS NULL OR OLD.fee = -1) AND 
        (NEW.fee IS NOT NULL AND NEW.fee <> -1) 
    THEN
        NEW.fee_created_at = NOW(); 
    END IF;
    IF 
        NEW.fee IS DISTINCT FROM OLD.fee AND
        NEW.fee IS NOT NULL AND NEW.fee <> -1
    THEN
        NEW.fee_updated_at = NOW(); 
    END IF;

    IF 
        (OLD.arrival_notes = 'REDACTED' OR OLD.arrival_notes = 'redacted' OR OLD.arrival_notes = '' OR OLD.arrival_notes IS NULL) AND 
        (NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL) 
    THEN
        NEW.arrival_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.arrival_notes IS DISTINCT FROM OLD.arrival_notes AND
        NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL
    THEN
        NEW.arrival_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.departure_notes = 'REDACTED' OR OLD.departure_notes = 'redacted' OR OLD.departure_notes = '' OR OLD.departure_notes IS NULL) AND 
        (NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL) 
    THEN
        NEW.departure_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.departure_notes IS DISTINCT FROM OLD.departure_notes AND
        NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL
    THEN
        NEW.departure_notes_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: ARRAY
    IF 
        (OLD.room_partner_wishes IS NULL OR OLD.room_partner_wishes = array[]::text[]) AND 
        (NEW.room_partner_wishes IS NOT NULL AND NEW.room_partner_wishes <> array[]::text[]) 
    THEN
        NEW.room_partner_wishes_created_at = NOW(); 
    END IF;
    IF 
        NEW.room_partner_wishes IS DISTINCT FROM OLD.room_partner_wishes AND
        NEW.room_partner_wishes IS NOT NULL AND NEW.room_partner_wishes <> array[]::text[]
    THEN
        NEW.room_partner_wishes_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: ARRAY
    IF 
        (OLD.zirkel_partner_wishes IS NULL OR OLD.zirkel_partner_wishes = array[]::text[]) AND 
        (NEW.zirkel_partner_wishes IS NOT NULL AND NEW.zirkel_partner_wishes <> array[]::text[]) 
    THEN
        NEW.zirkel_partner_wishes_created_at = NOW(); 
    END IF;
    IF 
        NEW.zirkel_partner_wishes IS DISTINCT FROM OLD.zirkel_partner_wishes AND
        NEW.zirkel_partner_wishes IS NOT NULL AND NEW.zirkel_partner_wishes <> array[]::text[]
    THEN
        NEW.zirkel_partner_wishes_updated_at = NOW(); 
    END IF;
    
    -- CAUTION: ARRAY
    IF 
        (OLD.medical_notes IS NULL OR OLD.medical_notes = array[]::text[]) AND 
        (NEW.medical_notes IS NOT NULL AND NEW.medical_notes <> array[]::text[]) 
    THEN
        NEW.medical_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.medical_notes IS DISTINCT FROM OLD.medical_notes AND
        NEW.medical_notes IS NOT NULL AND NEW.medical_notes <> array[]::text[]
    THEN
        NEW.medical_notes_updated_at = NOW(); 
    END IF;
    
    -- OTHER-USE TIMESTAMPS --

    IF 
        (OLD.reason_of_signoff IS NULL OR OLD.reason_of_signoff = '') AND 
        (NEW.reason_of_signoff IS NOT NULL AND NEW.reason_of_signoff <> '') 
    THEN
        NEW.time_of_signoff = NOW(); 
    END IF;
    IF 
        NEW.reason_of_signoff IS NULL OR NEW.reason_of_signoff = ''
    THEN
        NEW.time_of_signoff = NULL; 
    END IF;

    IF 
        OLD.confirmed = true AND NEW.confirmed = false
    THEN
        NEW.waiting_list_timestamp = NOW(); 
    END IF;
    IF 
        OLD.confirmed = false AND NEW.confirmed = true
    THEN        
        NEW.waiting_list_timestamp = NULL; 
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_extensions_timestamps
BEFORE UPDATE ON extensions 
FOR EACH ROW EXECUTE PROCEDURE update_extensions_timestamps_function();
