CREATE TABLE "contacts"
(
    id UUID DEFAULT uuid_generate_v4 (),
    extension_id UUID REFERENCES extensions(id) NOT NULL,
    name TEXT NOT NULL,
    name_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    telephone TEXT NOT NULL,
    telephone_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    telephone_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION update_contacts_timestamps_function()
RETURNS TRIGGER AS $$
BEGIN
    IF 
        (OLD.name = 'REDACTED' OR OLD.name = 'redacted' OR OLD.name = '' OR OLD.name IS NULL) AND 
        (NEW.name <> 'REDACTED' AND NEW.name <> 'redacted' AND NEW.name <> '' AND NEW.name IS NOT NULL) 
    THEN
        NEW.name_created_at = NOW(); 
    END IF;
    IF 
        NEW.name IS DISTINCT FROM OLD.name AND
        NEW.name <> 'REDACTED' AND NEW.name <> 'redacted' AND NEW.name <> '' AND NEW.name IS NOT NULL
    THEN
        NEW.name_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.telephone = 'REDACTED' OR OLD.telephone = 'redacted' OR OLD.telephone = '' OR OLD.telephone IS NULL) AND 
        (NEW.telephone <> 'REDACTED' AND NEW.telephone <> 'redacted' AND NEW.telephone <> '' AND NEW.telephone IS NOT NULL) 
    THEN
        NEW.telephone_created_at = NOW(); 
    END IF;
    IF 
        NEW.telephone IS DISTINCT FROM OLD.telephone AND
        NEW.telephone <> 'REDACTED' AND NEW.telephone <> 'redacted' AND NEW.telephone <> '' AND NEW.telephone IS NOT NULL
    THEN
        NEW.telephone_updated_at = NOW(); 
    END IF;
    
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_contacts_timestamps
BEFORE UPDATE ON contacts 
FOR EACH ROW EXECUTE PROCEDURE update_contacts_timestamps_function();
