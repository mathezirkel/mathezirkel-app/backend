DROP TRIGGER IF EXISTS set_default_fee ON extensions;
DROP FUNCTION IF EXISTS set_default_fee_function;
DROP FUNCTION IF EXISTS get_default_fee_from_event_function;