CREATE OR REPLACE FUNCTION get_default_fee_from_event_function(event_id UUID)
RETURNS integer AS $$
DECLARE
    default_fee_value integer;
BEGIN
    SELECT default_fee INTO default_fee_value
    FROM events
    WHERE id = event_id;
    
    RETURN default_fee_value;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION set_default_fee_function()
RETURNS TRIGGER AS $$
BEGIN
    NEW.fee := get_default_fee_from_event_function(NEW.event_id);
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER set_default_fee
BEFORE INSERT ON extensions 
FOR EACH ROW EXECUTE PROCEDURE set_default_fee_function();
