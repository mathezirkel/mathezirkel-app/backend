ALTER TABLE "events"
DROP COLUMN count_male,
DROP COLUMN count_female,
DROP COLUMN count_diverse;

DROP TRIGGER IF EXISTS update_sex_statistic ON extensions;
DROP FUNCTION IF EXISTS set_new_sex_statistic_values;
DROP FUNCTION IF EXISTS get_sex_count_from_event_function;
DROP FUNCTION IF EXISTS get_end_date_from_event;