ALTER TABLE "events"
ADD COLUMN count_male INT NOT NULL DEFAULT 0,
ADD COLUMN count_female INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse INT NOT NULL DEFAULT 0;

CREATE OR REPLACE FUNCTION get_sex_count_from_event_function(specific_event_id UUID, specific_sex sex_enum)
RETURNS integer AS $$
DECLARE
    sex_count integer;
BEGIN
    SELECT COUNT(*) INTO sex_count
    FROM extensions as e
    JOIN participants AS p ON p.id = e.participant_id
    WHERE e.event_id = specific_event_id AND p.sex = specific_sex;
    
    RETURN sex_count;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION get_end_date_from_event(specific_event_id UUID)
RETURNS date AS $$
DECLARE
    ret_date date;
BEGIN
    SELECT end_date INTO ret_date
    FROM events
    WHERE id = specific_event_id;
    
    RETURN ret_date;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION set_new_sex_statistic_values()
RETURNS TRIGGER AS $$
BEGIN
    IF get_end_date_from_event(NEW.event_id) >= CURRENT_DATE THEN
        UPDATE events
        SET 
            count_male = get_sex_count_from_event_function(NEW.event_id, 'male'),
            count_female = get_sex_count_from_event_function(NEW.event_id, 'female'),
            count_diverse = get_sex_count_from_event_function(NEW.event_id, 'diverse')
        WHERE id = NEW.event_id; 
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_sex_statistic
AFTER INSERT OR UPDATE OR DELETE ON extensions 
FOR EACH ROW EXECUTE PROCEDURE set_new_sex_statistic_values();
