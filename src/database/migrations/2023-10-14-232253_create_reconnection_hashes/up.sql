CREATE TABLE "reconnection_hashes"
(
    hash TEXT NOT NULL,
    participant_id UUID REFERENCES participants(id) ON DELETE CASCADE NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (hash, participant_id)
);

CREATE OR REPLACE FUNCTION delete_recent_reconnection_hashes()
RETURNS TRIGGER AS $$
BEGIN
    -- Delete recent changed values
    DELETE FROM reconnection_hashes
    WHERE 
        timestamp > NOW() - interval '2 hours' AND
        timestamp < NOW() - interval '200 milliseconds' AND
        participant_id = NEW.participant_id;
    -- Delete previous iterations of the same hash (refresh timestamp)
    DELETE FROM reconnection_hashes
    WHERE 
        participant_id = NEW.participant_id AND
        hash = NEW.hash;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER delete_recent_reconnection_hashes_trigger
BEFORE INSERT ON reconnection_hashes 
FOR EACH ROW EXECUTE PROCEDURE delete_recent_reconnection_hashes();