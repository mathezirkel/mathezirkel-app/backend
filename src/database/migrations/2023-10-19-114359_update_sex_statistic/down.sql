ALTER TABLE "events"
DROP COLUMN count_male_5,
DROP COLUMN count_female_5,
DROP COLUMN count_diverse_5,
DROP COLUMN count_male_6,
DROP COLUMN count_female_6,
DROP COLUMN count_diverse_6,
DROP COLUMN count_male_7,
DROP COLUMN count_female_7,
DROP COLUMN count_diverse_7,
DROP COLUMN count_male_8,
DROP COLUMN count_female_8,
DROP COLUMN count_diverse_8,
DROP COLUMN count_male_9,
DROP COLUMN count_female_9,
DROP COLUMN count_diverse_9,
DROP COLUMN count_male_10,
DROP COLUMN count_female_10,
DROP COLUMN count_diverse_10,
DROP COLUMN count_male_11,
DROP COLUMN count_female_11,
DROP COLUMN count_diverse_11,
DROP COLUMN count_male_12,
DROP COLUMN count_female_12,
DROP COLUMN count_diverse_12,
DROP COLUMN count_male_13,
DROP COLUMN count_female_13,
DROP COLUMN count_diverse_13;

DROP TRIGGER IF EXISTS update_sex_statistic ON extensions;
DROP FUNCTION IF EXISTS set_new_sex_statistic_values;
DROP FUNCTION IF EXISTS get_class_resolved_sex_count_from_event_function;

CREATE OR REPLACE FUNCTION set_new_sex_statistic_values()
RETURNS TRIGGER AS $$
BEGIN
    IF get_end_date_from_event(NEW.event_id) >= CURRENT_DATE THEN
        UPDATE events
        SET 
            count_male = get_sex_count_from_event_function(NEW.event_id, 'male'),
            count_female = get_sex_count_from_event_function(NEW.event_id, 'female'),
            count_diverse = get_sex_count_from_event_function(NEW.event_id, 'diverse')
        WHERE id = NEW.event_id; 
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_sex_statistic
AFTER INSERT OR UPDATE OR DELETE ON extensions 
FOR EACH ROW EXECUTE PROCEDURE set_new_sex_statistic_values();