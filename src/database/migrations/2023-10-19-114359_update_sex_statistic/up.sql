ALTER TABLE "events"
ADD COLUMN count_male_5 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_5 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_5 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_6 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_6 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_6 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_7 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_7 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_7 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_8 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_8 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_8 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_9 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_9 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_9 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_10 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_10 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_10 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_11 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_11 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_11 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_12 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_12 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_12 INT NOT NULL DEFAULT 0,
ADD COLUMN count_male_13 INT NOT NULL DEFAULT 0,
ADD COLUMN count_female_13 INT NOT NULL DEFAULT 0,
ADD COLUMN count_diverse_13 INT NOT NULL DEFAULT 0;

CREATE OR REPLACE FUNCTION get_class_resolved_sex_count_from_event_function(specific_event_id UUID, specific_sex sex_enum, specific_classyear classyear_enum)
RETURNS integer AS $$
DECLARE
    sex_count integer;
BEGIN
    SELECT COUNT(*) INTO sex_count
    FROM extensions as e
    JOIN participants AS p ON p.id = e.participant_id
    WHERE e.event_id = specific_event_id AND p.sex = specific_sex AND e.class_year = specific_classyear;
    
    RETURN sex_count;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION set_new_sex_statistic_values()
RETURNS TRIGGER AS $$
BEGIN
    IF get_end_date_from_event(NEW.event_id) >= CURRENT_DATE AND 
        (
            NEW.class_year <> OLD.class_year OR
            NEW.class_year IS NULL OR
            OLD.class_year IS NULL OR 
            TG_OP = 'INSERT' OR  
            TG_OP = 'DELETE'  
        )
    THEN
        UPDATE events
        SET 
            count_male = get_sex_count_from_event_function(NEW.event_id, 'male'),
            count_female = get_sex_count_from_event_function(NEW.event_id, 'female'),
            count_diverse = get_sex_count_from_event_function(NEW.event_id, 'diverse'),
            count_male_5 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_5'),
            count_female_5 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_5'),
            count_diverse_5 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_5'),
            count_male_6 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_6'),
            count_female_6 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_6'),
            count_diverse_6 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_6'),
            count_male_7 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_7'),
            count_female_7 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_7'),
            count_diverse_7 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_7'),
            count_male_8 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_8'),
            count_female_8 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_8'),
            count_diverse_8 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_8'),
            count_male_9 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_9'),
            count_female_9 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_9'),
            count_diverse_9 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_9'),
            count_male_10 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_10'),
            count_female_10 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_10'),
            count_diverse_10 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_10'),
            count_male_11 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_11'),
            count_female_11 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_11'),
            count_diverse_11 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_11'),
            count_male_12 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_12'),
            count_female_12 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_12'),
            count_diverse_12 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_12'),
            count_male_13 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'male', 'class_13'),
            count_female_13 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'female', 'class_13'),
            count_diverse_13 = get_class_resolved_sex_count_from_event_function(NEW.event_id, 'diverse', 'class_13')
        WHERE id = NEW.event_id; 
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_sex_statistic
AFTER INSERT OR UPDATE OR DELETE ON extensions 
FOR EACH ROW EXECUTE PROCEDURE set_new_sex_statistic_values();
