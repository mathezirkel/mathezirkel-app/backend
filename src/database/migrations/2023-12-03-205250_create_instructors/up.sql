CREATE TABLE "instructors"
(
    id UUID DEFAULT uuid_generate_v4 (),
    last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    keycloak_id TEXT NOT NULL,
    keycloak_username TEXT,
    given_name TEXT,
    family_name TEXT,
    call_name TEXT,
    birth_date DATE,
    sex sex_enum,
    gender TEXT,
    pronoun TEXT,
    street TEXT,
    street_number TEXT,
    postal_code TEXT,
    city TEXT,
    country TEXT,
    email TEXT,
    telephone TEXT,
    abbreviation TEXT,

    PRIMARY KEY (id),
    UNIQUE(keycloak_id) -- Only one instructor per keycloak id
)
