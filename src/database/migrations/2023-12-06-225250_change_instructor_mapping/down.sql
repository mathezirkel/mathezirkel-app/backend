ALTER TABLE "zirkel"
    ADD COLUMN instructors TEXT[] NOT NULL DEFAULT array[]::text[];

DROP TABLE instructor_extension_to_zirkel_mapping;
