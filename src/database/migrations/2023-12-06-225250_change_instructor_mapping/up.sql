ALTER TABLE "zirkel"
    DROP COLUMN instructors;

CREATE TABLE "instructor_extension_to_zirkel_mapping"
(
    id UUID DEFAULT uuid_generate_v4 (),
    instructor_extension_id UUID REFERENCES instructor_extensions(id) ON DELETE CASCADE NOT NULL,
    zirkel_id UUID REFERENCES zirkel(id) ON DELETE CASCADE NOT NULL,

    PRIMARY KEY (id),
    UNIQUE(instructor_extension_id, zirkel_id) -- Only one mapping per Instructor and Zirkel possible
);