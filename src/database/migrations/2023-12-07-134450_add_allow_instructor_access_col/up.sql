ALTER TABLE "events"
    ADD COLUMN give_instructors_full_read_access BOOL NOT NULL DEFAULT false;
