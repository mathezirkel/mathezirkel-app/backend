ALTER TABLE "events"
    ADD COLUMN statistics JSON NOT NULL DEFAULT '{"ct_male_5":0,"ct_male_6":0,"ct_male_7":0,"ct_male_8":0,"ct_male_9":0,"ct_male_10":0,"ct_male_11":0,"ct_male_12":0,"ct_male_13":0,"ct_male_all":0,"ct_female_5":0,"ct_female_6":0,"ct_female_7":0,"ct_female_8":0,"ct_female_9":0,"ct_female_10":0,"ct_female_11":0,"ct_female_12":0,"ct_female_13":0,"ct_female_all":0,"ct_diverse_5":0,"ct_diverse_6":0,"ct_diverse_7":0,"ct_diverse_8":0,"ct_diverse_9":0,"ct_diverse_10":0,"ct_diverse_11":0,"ct_diverse_12":0,"ct_diverse_13":0,"ct_diverse_all":0,"ct_all_5":0,"ct_all_6":0,"ct_all_7":0,"ct_all_8":0,"ct_all_9":0,"ct_all_10":0,"ct_all_11":0,"ct_all_12":0,"ct_all_13":0,"ct_all_all":0,"wl_male_5":0,"wl_male_6":0,"wl_male_7":0,"wl_male_8":0,"wl_male_9":0,"wl_male_10":0,"wl_male_11":0,"wl_male_12":0,"wl_male_13":0,"wl_male_all":0,"wl_female_5":0,"wl_female_6":0,"wl_female_7":0,"wl_female_8":0,"wl_female_9":0,"wl_female_10":0,"wl_female_11":0,"wl_female_12":0,"wl_female_13":0,"wl_female_all":0,"wl_diverse_5":0,"wl_diverse_6":0,"wl_diverse_7":0,"wl_diverse_8":0,"wl_diverse_9":0,"wl_diverse_10":0,"wl_diverse_11":0,"wl_diverse_12":0,"wl_diverse_13":0,"wl_diverse_all":0,"wl_all_5":0,"wl_all_6":0,"wl_all_7":0,"wl_all_8":0,"wl_all_9":0,"wl_all_10":0,"wl_all_11":0,"wl_all_12":0,"wl_all_13":0,"wl_all_all":0}';

UPDATE "events"
    SET statistics = jsonb_build_object(
        'ct_male_5', count_male_5,
        'ct_male_6', count_male_6,
        'ct_male_7', count_male_7,
        'ct_male_8', count_male_8,
        'ct_male_9', count_male_9,
        'ct_male_10', count_male_10,
        'ct_male_11', count_male_11,
        'ct_male_12', count_male_12,
        'ct_male_13', count_male_13,
        'ct_male_all', count_male_5 + count_male_6 + count_male_7 + count_male_8 + count_male_9 + count_male_10 + count_male_11 + count_male_12 + count_male_13,
        'ct_female_5', count_female_5,
        'ct_female_6', count_female_6,
        'ct_female_7', count_female_7,
        'ct_female_8', count_female_8,
        'ct_female_9', count_female_9,
        'ct_female_10', count_female_10,
        'ct_female_11', count_female_11,
        'ct_female_12', count_female_12,
        'ct_female_13', count_female_13,
        'ct_female_all', count_female_5 + count_female_6 + count_female_7 + count_female_8 + count_female_9 + count_female_10 + count_female_11 + count_female_12 + count_female_13,
        'ct_diverse_5', count_diverse_5,
        'ct_diverse_6', count_diverse_6,
        'ct_diverse_7', count_diverse_7,
        'ct_diverse_8', count_diverse_8,
        'ct_diverse_9', count_diverse_9,
        'ct_diverse_10', count_diverse_10,
        'ct_diverse_11', count_diverse_11,
        'ct_diverse_12', count_diverse_12,
        'ct_diverse_13', count_diverse_13,
        'ct_diverse_all', count_diverse_5 + count_diverse_6 + count_diverse_7 + count_diverse_8 + count_diverse_9 + count_diverse_10 + count_diverse_11 + count_diverse_12 + count_diverse_13,
        'ct_all_5', count_male_5 + count_female_5 + count_diverse_5,
        'ct_all_6', count_male_6 + count_female_6 + count_diverse_6,
        'ct_all_7', count_male_7 + count_female_7 + count_diverse_7,
        'ct_all_8', count_male_8 + count_female_8 + count_diverse_8,
        'ct_all_9', count_male_9 + count_female_9 + count_diverse_9,
        'ct_all_10', count_male_10 + count_female_10 + count_diverse_10,
        'ct_all_11', count_male_11 + count_female_11 + count_diverse_11,
        'ct_all_12', count_male_12 + count_female_12 + count_diverse_12,
        'ct_all_13', count_male_13 + count_female_13 + count_diverse_13,
        'ct_all_all', count_male_5 + count_male_6 + count_male_7 + count_male_8 + count_male_9 + count_male_10 + count_male_11 + count_male_12 + count_male_13 + count_female_5 + count_female_6 + count_female_7 + count_female_8 + count_female_9 + count_female_10 + count_female_11 + count_female_12 + count_female_13 + count_diverse_5 + count_diverse_6 + count_diverse_7 + count_diverse_8 + count_diverse_9 + count_diverse_10 + count_diverse_11 + count_diverse_12 + count_diverse_13) || jsonb_build_object(
        'wl_male_5', 0,
        'wl_male_6', 0,
        'wl_male_7', 0,
        'wl_male_8', 0,
        'wl_male_9', 0,
        'wl_male_10', 0,
        'wl_male_11', 0,
        'wl_male_12', 0,
        'wl_male_13', 0,
        'wl_male_all', 0,
        'wl_female_5', 0,
        'wl_female_6', 0,
        'wl_female_7', 0,
        'wl_female_8', 0,
        'wl_female_9', 0,
        'wl_female_10', 0,
        'wl_female_11', 0,
        'wl_female_12', 0,
        'wl_female_13', 0,
        'wl_female_all', 0,
        'wl_diverse_5', 0,
        'wl_diverse_6', 0,
        'wl_diverse_7', 0,
        'wl_diverse_8', 0,
        'wl_diverse_9', 0,
        'wl_diverse_10', 0,
        'wl_diverse_11', 0,
        'wl_diverse_12', 0,
        'wl_diverse_13', 0,
        'wl_diverse_all', 0,
        'wl_all_5', 0,
        'wl_all_6', 0,
        'wl_all_7', 0,
        'wl_all_8', 0,
        'wl_all_9', 0,
        'wl_all_10', 0,
        'wl_all_11', 0,
        'wl_all_12', 0,
        'wl_all_13', 0,
        'wl_all_all', 0
    );

ALTER TABLE "events"
    DROP COLUMN count_male,
    DROP COLUMN count_female,
    DROP COLUMN count_diverse,
    DROP COLUMN count_male_5,
    DROP COLUMN count_female_5,
    DROP COLUMN count_diverse_5,
    DROP COLUMN count_male_6,
    DROP COLUMN count_female_6,
    DROP COLUMN count_diverse_6,
    DROP COLUMN count_male_7,
    DROP COLUMN count_female_7,
    DROP COLUMN count_diverse_7,
    DROP COLUMN count_male_8,
    DROP COLUMN count_female_8,
    DROP COLUMN count_diverse_8,
    DROP COLUMN count_male_9,
    DROP COLUMN count_female_9,
    DROP COLUMN count_diverse_9,
    DROP COLUMN count_male_10,
    DROP COLUMN count_female_10,
    DROP COLUMN count_diverse_10,
    DROP COLUMN count_male_11,
    DROP COLUMN count_female_11,
    DROP COLUMN count_diverse_11,
    DROP COLUMN count_male_12,
    DROP COLUMN count_female_12,
    DROP COLUMN count_diverse_12,
    DROP COLUMN count_male_13,
    DROP COLUMN count_female_13,
    DROP COLUMN count_diverse_13;

DROP TRIGGER IF EXISTS update_sex_statistic ON extensions;
DROP FUNCTION IF EXISTS set_new_sex_statistic_values;
DROP FUNCTION IF EXISTS get_sex_count_from_event_function;
DROP FUNCTION IF EXISTS get_class_resolved_sex_count_from_event_function;

-- NEW statistics update functions

CREATE OR REPLACE FUNCTION get_start_date_from_event(specific_event_id UUID)
RETURNS DATE AS $$
DECLARE
    ret_date DATE;
BEGIN
    SELECT start_date INTO ret_date
    FROM events
    WHERE id = specific_event_id;
    
    RETURN ret_date;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION get_statistic_from_event_function(specific_event_id UUID, event_start_date DATE, specific_sex TEXT, specific_classyear TEXT, waiting_list BOOL)
RETURNS INTEGER AS $$
DECLARE
    res_count INTEGER;
BEGIN
    SELECT COUNT(*) INTO res_count
    FROM extensions as e
    JOIN participants AS p ON p.id = e.participant_id
    WHERE e.event_id = specific_event_id 
        AND (CAST(p.sex AS TEXT) = specific_sex OR specific_sex = 'all') 
        AND (CAST(e.class_year AS TEXT) = specific_classyear OR specific_classyear = 'class_all') 
        AND ((NOT waiting_list AND e.confirmed AND (e.time_of_signoff IS NULL OR e.time_of_signoff >= event_start_date)) OR (waiting_list AND NOT e.confirmed AND e.time_of_signoff IS NULL));
    
    RETURN res_count;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION set_new_event_statistics_values()
RETURNS TRIGGER AS $$
DECLARE
    event_start_date DATE;
BEGIN
    IF get_end_date_from_event(NEW.event_id) >= CURRENT_DATE AND 
        (
            NEW.class_year <> OLD.class_year OR
            (NEW.class_year IS NULL AND OLD.class_year IS NOT NULL) OR
            (OLD.class_year IS NULL AND NEW.class_year IS NOT NULL) OR 
            NEW.time_of_signoff <> OLD.time_of_signoff OR
            (NEW.time_of_signoff IS NULL AND OLD.time_of_signoff IS NOT NULL) OR
            (OLD.time_of_signoff IS NULL AND NEW.time_of_signoff IS NOT NULL) OR 
            NEW.confirmed <> OLD.confirmed OR
            (NEW.confirmed IS NULL AND OLD.confirmed IS NOT NULL) OR
            (OLD.confirmed IS NULL AND NEW.confirmed IS NOT NULL) OR 
            TG_OP = 'INSERT' OR  
            TG_OP = 'DELETE'  
        )
    THEN
        SELECT get_start_date_from_event(NEW.event_id) INTO event_start_date;

        UPDATE events
            SET statistics = 
            (
                SELECT
                    jsonb_object_agg(
                        CASE WHEN waiting_list THEN 'wl_' ELSE 'ct_' END || sex || '_' || classyear, 
                        get_statistic_from_event_function(NEW.event_id, event_start_date, sex, 'class_' || classyear, waiting_list)
                    )
                FROM 
                    (SELECT '' || generate_series(5, 13) UNION ALL SELECT 'all') AS classyears(classyear),
                    (VALUES ('male'), ('female'), ('diverse'), ('all')) AS sexes(sex),
                    (VALUES (true), (false)) AS waiting_list_options(waiting_list)
            )
        WHERE id = NEW.event_id; 
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_event_statistics
AFTER INSERT OR UPDATE OR DELETE ON extensions 
FOR EACH ROW EXECUTE PROCEDURE set_new_event_statistics_values();
