-- the enum entry doesn't need to/cannot be removed really on migrating back

ALTER TABLE "instructor_extensions"
    ADD COLUMN preparation_weekend_arrival traveltype_enum,
    ADD COLUMN preparation_weekend_arrival_notes TEXT,
    ADD COLUMN preparation_weekend_arrival_notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_arrival_notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_departure traveltype_enum,
    ADD COLUMN preparation_weekend_departure_notes TEXT,
    ADD COLUMN preparation_weekend_departure_notes_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_departure_notes_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_has_contract BOOL NOT NULL DEFAULT false,
    ADD COLUMN preparation_weekend_contract_with TEXT,
    ADD COLUMN preparation_weekend_contract_with_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_contract_with_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN preparation_weekend_room_number TEXT,
    ADD COLUMN preparation_weekend_participates BOOL NOT NULL DEFAULT true;

ALTER TABLE "instructor_extensions"
    DROP COLUMN faculty,
    DROP COLUMN chair,
    DROP COLUMN personnel_number,
    DROP COLUMN personnel_number_created_at,
    DROP COLUMN personnel_number_updated_at,
    DROP COLUMN job_title,
    DROP COLUMN email_work,
    DROP COLUMN telephone_work,
    DROP COLUMN start_of_travel,
    DROP COLUMN arrival_at_business_location,
    DROP COLUMN start_of_business_activities,
    DROP COLUMN end_of_business_activities,
    DROP COLUMN start_of_return_journey,
    DROP COLUMN end_of_travel;

CREATE OR REPLACE FUNCTION update_instructor_extensions_timestamps_function()
RETURNS TRIGGER AS $$
BEGIN
    IF 
        (OLD.nutrition = 'redacted' OR OLD.nutrition IS NULL) AND 
        (NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL) 
    THEN
        NEW.nutrition_created_at = NOW(); 
    END IF;
    IF 
        NEW.nutrition IS DISTINCT FROM OLD.nutrition AND
        NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL
    THEN
        NEW.nutrition_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.food_restriction = 'REDACTED' OR OLD.food_restriction = 'redacted' OR OLD.food_restriction = '' OR OLD.food_restriction IS NULL) AND 
        (NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL) 
    THEN
        NEW.food_restriction_created_at = NOW(); 
    END IF;
    IF 
        NEW.food_restriction IS DISTINCT FROM OLD.food_restriction AND
        NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL
    THEN
        NEW.food_restriction_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.arrival_notes = 'REDACTED' OR OLD.arrival_notes = 'redacted' OR OLD.arrival_notes = '' OR OLD.arrival_notes IS NULL) AND 
        (NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL) 
    THEN
        NEW.arrival_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.arrival_notes IS DISTINCT FROM OLD.arrival_notes AND
        NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL
    THEN
        NEW.arrival_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.departure_notes = 'REDACTED' OR OLD.departure_notes = 'redacted' OR OLD.departure_notes = '' OR OLD.departure_notes IS NULL) AND 
        (NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL) 
    THEN
        NEW.departure_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.departure_notes IS DISTINCT FROM OLD.departure_notes AND
        NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL
    THEN
        NEW.departure_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.contract_with = 'REDACTED' OR OLD.contract_with = 'redacted' OR OLD.contract_with = '' OR OLD.contract_with IS NULL) AND 
        (NEW.contract_with <> 'REDACTED' AND NEW.contract_with <> 'redacted' AND NEW.contract_with <> '' AND NEW.contract_with IS NOT NULL) 
    THEN
        NEW.contract_with_created_at = NOW(); 
    END IF;
    IF 
        NEW.contract_with IS DISTINCT FROM OLD.contract_with AND
        NEW.contract_with <> 'REDACTED' AND NEW.contract_with <> 'redacted' AND NEW.contract_with <> '' AND NEW.contract_with IS NOT NULL
    THEN
        NEW.contract_with_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.preparation_weekend_arrival_notes = 'REDACTED' OR OLD.preparation_weekend_arrival_notes = 'redacted' OR OLD.preparation_weekend_arrival_notes = '' OR OLD.preparation_weekend_arrival_notes IS NULL) AND 
        (NEW.preparation_weekend_arrival_notes <> 'REDACTED' AND NEW.preparation_weekend_arrival_notes <> 'redacted' AND NEW.preparation_weekend_arrival_notes <> '' AND NEW.preparation_weekend_arrival_notes IS NOT NULL) 
    THEN
        NEW.preparation_weekend_arrival_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.preparation_weekend_arrival_notes IS DISTINCT FROM OLD.preparation_weekend_arrival_notes AND
        NEW.preparation_weekend_arrival_notes <> 'REDACTED' AND NEW.preparation_weekend_arrival_notes <> 'redacted' AND NEW.preparation_weekend_arrival_notes <> '' AND NEW.preparation_weekend_arrival_notes IS NOT NULL
    THEN
        NEW.preparation_weekend_arrival_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.preparation_weekend_departure_notes = 'REDACTED' OR OLD.preparation_weekend_departure_notes = 'redacted' OR OLD.preparation_weekend_departure_notes = '' OR OLD.preparation_weekend_departure_notes IS NULL) AND 
        (NEW.preparation_weekend_departure_notes <> 'REDACTED' AND NEW.preparation_weekend_departure_notes <> 'redacted' AND NEW.preparation_weekend_departure_notes <> '' AND NEW.preparation_weekend_departure_notes IS NOT NULL) 
    THEN
        NEW.preparation_weekend_departure_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.preparation_weekend_departure_notes IS DISTINCT FROM OLD.preparation_weekend_departure_notes AND
        NEW.preparation_weekend_departure_notes <> 'REDACTED' AND NEW.preparation_weekend_departure_notes <> 'redacted' AND NEW.preparation_weekend_departure_notes <> '' AND NEW.preparation_weekend_departure_notes IS NOT NULL
    THEN
        NEW.preparation_weekend_departure_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.preparation_weekend_contract_with = 'REDACTED' OR OLD.preparation_weekend_contract_with = 'redacted' OR OLD.preparation_weekend_contract_with = '' OR OLD.preparation_weekend_contract_with IS NULL) AND 
        (NEW.preparation_weekend_contract_with <> 'REDACTED' AND NEW.preparation_weekend_contract_with <> 'redacted' AND NEW.preparation_weekend_contract_with <> '' AND NEW.preparation_weekend_contract_with IS NOT NULL) 
    THEN
        NEW.preparation_weekend_contract_with_created_at = NOW(); 
    END IF;
    IF 
        NEW.preparation_weekend_contract_with IS DISTINCT FROM OLD.preparation_weekend_contract_with AND
        NEW.preparation_weekend_contract_with <> 'REDACTED' AND NEW.preparation_weekend_contract_with <> 'redacted' AND NEW.preparation_weekend_contract_with <> '' AND NEW.preparation_weekend_contract_with IS NOT NULL
    THEN
        NEW.preparation_weekend_contract_with_updated_at = NOW(); 
    END IF;
    
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_instructor_extensions_timestamps
BEFORE UPDATE ON instructor_extensions 
FOR EACH ROW EXECUTE PROCEDURE update_instructor_extensions_timestamps_function();
