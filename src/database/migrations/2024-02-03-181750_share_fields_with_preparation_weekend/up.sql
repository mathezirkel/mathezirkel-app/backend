DO $$ 
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumtypid = 'eventtype_enum'::regtype AND enumlabel = 'preparation_weekend') THEN
    EXECUTE 'ALTER TYPE eventtype_enum ADD VALUE ''preparation_weekend''';
  END IF;
END $$;

ALTER TABLE "instructor_extensions"
    DROP COLUMN preparation_weekend_arrival,
    DROP COLUMN preparation_weekend_arrival_notes,
    DROP COLUMN preparation_weekend_arrival_notes_created_at,
    DROP COLUMN preparation_weekend_arrival_notes_updated_at,
    DROP COLUMN preparation_weekend_departure,
    DROP COLUMN preparation_weekend_departure_notes,
    DROP COLUMN preparation_weekend_departure_notes_created_at,
    DROP COLUMN preparation_weekend_departure_notes_updated_at,
    DROP COLUMN preparation_weekend_has_contract,
    DROP COLUMN preparation_weekend_contract_with,
    DROP COLUMN preparation_weekend_contract_with_created_at,
    DROP COLUMN preparation_weekend_contract_with_updated_at,
    DROP COLUMN preparation_weekend_room_number,
    DROP COLUMN preparation_weekend_participates;

ALTER TABLE "instructor_extensions"
    ADD COLUMN faculty TEXT,
    ADD COLUMN chair TEXT,
    ADD COLUMN personnel_number TEXT,
    ADD COLUMN personnel_number_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN personnel_number_updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN job_title TEXT,
    ADD COLUMN email_work TEXT,
    ADD COLUMN telephone_work TEXT,
    ADD COLUMN start_of_travel TIMESTAMP,
    ADD COLUMN arrival_at_business_location TIMESTAMP,
    ADD COLUMN start_of_business_activities TIMESTAMP,
    ADD COLUMN end_of_business_activities TIMESTAMP,
    ADD COLUMN start_of_return_journey TIMESTAMP,
    ADD COLUMN end_of_travel TIMESTAMP;

CREATE OR REPLACE FUNCTION update_instructor_extensions_timestamps_function()
RETURNS TRIGGER AS $$
BEGIN
    IF 
        (OLD.nutrition = 'redacted' OR OLD.nutrition IS NULL) AND 
        (NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL) 
    THEN
        NEW.nutrition_created_at = NOW(); 
    END IF;
    IF 
        NEW.nutrition IS DISTINCT FROM OLD.nutrition AND
        NEW.nutrition <> 'redacted' AND NEW.nutrition IS NOT NULL
    THEN
        NEW.nutrition_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.food_restriction = 'REDACTED' OR OLD.food_restriction = 'redacted' OR OLD.food_restriction = '' OR OLD.food_restriction IS NULL) AND 
        (NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL) 
    THEN
        NEW.food_restriction_created_at = NOW(); 
    END IF;
    IF 
        NEW.food_restriction IS DISTINCT FROM OLD.food_restriction AND
        NEW.food_restriction <> 'REDACTED' AND NEW.food_restriction <> 'redacted' AND NEW.food_restriction <> '' AND NEW.food_restriction IS NOT NULL
    THEN
        NEW.food_restriction_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.arrival_notes = 'REDACTED' OR OLD.arrival_notes = 'redacted' OR OLD.arrival_notes = '' OR OLD.arrival_notes IS NULL) AND 
        (NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL) 
    THEN
        NEW.arrival_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.arrival_notes IS DISTINCT FROM OLD.arrival_notes AND
        NEW.arrival_notes <> 'REDACTED' AND NEW.arrival_notes <> 'redacted' AND NEW.arrival_notes <> '' AND NEW.arrival_notes IS NOT NULL
    THEN
        NEW.arrival_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.departure_notes = 'REDACTED' OR OLD.departure_notes = 'redacted' OR OLD.departure_notes = '' OR OLD.departure_notes IS NULL) AND 
        (NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL) 
    THEN
        NEW.departure_notes_created_at = NOW(); 
    END IF;
    IF 
        NEW.departure_notes IS DISTINCT FROM OLD.departure_notes AND
        NEW.departure_notes <> 'REDACTED' AND NEW.departure_notes <> 'redacted' AND NEW.departure_notes <> '' AND NEW.departure_notes IS NOT NULL
    THEN
        NEW.departure_notes_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.contract_with = 'REDACTED' OR OLD.contract_with = 'redacted' OR OLD.contract_with = '' OR OLD.contract_with IS NULL) AND 
        (NEW.contract_with <> 'REDACTED' AND NEW.contract_with <> 'redacted' AND NEW.contract_with <> '' AND NEW.contract_with IS NOT NULL) 
    THEN
        NEW.contract_with_created_at = NOW(); 
    END IF;
    IF 
        NEW.contract_with IS DISTINCT FROM OLD.contract_with AND
        NEW.contract_with <> 'REDACTED' AND NEW.contract_with <> 'redacted' AND NEW.contract_with <> '' AND NEW.contract_with IS NOT NULL
    THEN
        NEW.contract_with_updated_at = NOW(); 
    END IF;
    
    IF 
        (OLD.personnel_number = 'REDACTED' OR OLD.personnel_number = 'redacted' OR OLD.personnel_number = '' OR OLD.personnel_number IS NULL) AND 
        (NEW.personnel_number <> 'REDACTED' AND NEW.personnel_number <> 'redacted' AND NEW.personnel_number <> '' AND NEW.personnel_number IS NOT NULL) 
    THEN
        NEW.personnel_number_created_at = NOW(); 
    END IF;
    IF 
        NEW.personnel_number IS DISTINCT FROM OLD.personnel_number AND
        NEW.personnel_number <> 'REDACTED' AND NEW.personnel_number <> 'redacted' AND NEW.personnel_number <> '' AND NEW.personnel_number IS NOT NULL
    THEN
        NEW.personnel_number_updated_at = NOW(); 
    END IF;
    
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_instructor_extensions_timestamps
BEFORE UPDATE ON instructor_extensions 
FOR EACH ROW EXECUTE PROCEDURE update_instructor_extensions_timestamps_function();
