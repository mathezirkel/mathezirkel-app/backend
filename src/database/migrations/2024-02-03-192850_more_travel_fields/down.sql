
ALTER TABLE "events"
    DROP COLUMN purpose_of_business_trip,
    DROP COLUMN overnight_costs,
    DROP COLUMN additional_costs,
    DROP COLUMN reason_of_additional_costs,
    DROP COLUMN budget_section,
    DROP COLUMN budget_item,
    DROP COLUMN cost_center_budget,
    DROP COLUMN cost_type,
    DROP COLUMN cost_center_cost_and_activity_accounting,
    DROP COLUMN cost_object;