
ALTER TABLE "events"
    ADD COLUMN purpose_of_business_trip TEXT,
    ADD COLUMN overnight_costs DOUBLE PRECISION,
    ADD COLUMN additional_costs DOUBLE PRECISION,
    ADD COLUMN reason_of_additional_costs TEXT,
    ADD COLUMN budget_section TEXT,
    ADD COLUMN budget_item TEXT,
    ADD COLUMN cost_center_budget TEXT,
    ADD COLUMN cost_type TEXT,
    ADD COLUMN cost_center_cost_and_activity_accounting TEXT,
    ADD COLUMN cost_object TEXT;