CREATE TABLE "export_timestamps"
(
    id UUID DEFAULT uuid_generate_v4 (),
    key TEXT NOT NULL,
    export_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    graphql_success BOOL NOT NULL DEFAULT true,
    jq_success BOOL NOT NULL DEFAULT true,
    webdav_success BOOL NOT NULL DEFAULT true,
    -- possible future feature
    latex_success BOOL NOT NULL DEFAULT true,

    PRIMARY KEY (id),
    UNIQUE(key) -- Only one set of timestamp/boolean per key
);

CREATE INDEX idx_export_timestamps_key ON export_timestamps (key);