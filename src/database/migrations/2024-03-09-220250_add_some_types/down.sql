-- the enum entry doesn't need to/cannot be removed really on migrating back

ALTER TABLE "export_timestamps"
    DROP COLUMN python_success;

CREATE OR REPLACE FUNCTION preset_confirmed_function()
RETURNS TRIGGER AS $$
BEGIN
    -- Check the type of the event linked to the extension
    IF NEW.event_id IS NOT NULL THEN
        IF (SELECT type FROM events WHERE id = NEW.event_id) = 'zirkel' THEN
            -- Set confirmed to true if the type of the event is 'zirkel'
            NEW.confirmed := true;
        END IF;
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';