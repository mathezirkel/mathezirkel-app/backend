DO $$ 
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumtypid = 'eventtype_enum'::regtype AND enumlabel = 'info') THEN
    EXECUTE 'ALTER TYPE eventtype_enum ADD VALUE ''info''';
  END IF;
END $$;

ALTER TABLE "export_timestamps"
    ADD COLUMN python_success BOOL NOT NULL DEFAULT true;

CREATE OR REPLACE FUNCTION preset_confirmed_function()
RETURNS TRIGGER AS $$
BEGIN
    -- Check the type of the event linked to the extension
    IF NEW.event_id IS NOT NULL THEN
        IF (SELECT type FROM events WHERE id = NEW.event_id) in ('zirkel', 'info') THEN
            -- Set confirmed to true if the type of the event is 'zirkel' or 'info'
            NEW.confirmed := true;
        END IF;
    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';