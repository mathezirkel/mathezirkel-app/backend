ALTER TABLE "export_timestamps"
    DROP COLUMN successful_jobs,
    DROP COLUMN total_pending_jobs,
    DROP COLUMN failure,
    ADD COLUMN graphql_success BOOL NOT NULL DEFAULT true,
    ADD COLUMN jq_success BOOL NOT NULL DEFAULT true,
    ADD COLUMN webdav_success BOOL NOT NULL DEFAULT true,
    ADD COLUMN python_success BOOL NOT NULL DEFAULT true,
    ADD COLUMN latex_success BOOL NOT NULL DEFAULT true;