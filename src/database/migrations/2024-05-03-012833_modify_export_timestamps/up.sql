ALTER TABLE "export_timestamps"
    ADD COLUMN successful_jobs INT NOT NULL DEFAULT 0,
    ADD COLUMN total_pending_jobs INT NOT NULL DEFAULT 0,
    ADD COLUMN failure BOOL NOT NULL DEFAULT false,
    DROP COLUMN graphql_success,
    DROP COLUMN webdav_success,
    DROP COLUMN latex_success,
    DROP COLUMN python_success,
    DROP COLUMN jq_success;