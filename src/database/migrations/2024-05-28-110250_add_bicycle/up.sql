DO $$ 
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_enum WHERE enumtypid = 'traveltype_enum'::regtype AND enumlabel = 'bicycle') THEN
    EXECUTE 'ALTER TYPE traveltype_enum ADD VALUE ''bicycle''';
  END IF;
END $$;