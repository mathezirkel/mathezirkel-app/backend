CREATE TABLE "zirkel_plan_slots"
(
    id UUID DEFAULT uuid_generate_v4 (),
    event_id UUID REFERENCES events(id) ON DELETE CASCADE NOT NULL,

    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    name TEXT NOT NULL,
    certificate BOOL NOT NULL DEFAULT true,
    beamer BOOL NOT NULL DEFAULT true,

    PRIMARY KEY (id)
);

CREATE TABLE "zirkel_plan_entries"
(
    id UUID DEFAULT uuid_generate_v4 (),
    zirkel_plan_slot_id UUID REFERENCES zirkel_plan_slots(id) ON DELETE CASCADE NOT NULL,
    zirkel_id UUID REFERENCES zirkel(id) ON DELETE CASCADE NOT NULL,
    instructor_extension_uuids UUID[] NOT NULL DEFAULT array[]::uuid[],
    
    topic TEXT DEFAULT NULL,
    beamer BOOL NOT NULL DEFAULT false,

    PRIMARY KEY (id),
    UNIQUE(zirkel_plan_slot_id, zirkel_id) -- Only one entry per zirkel and slot
);
