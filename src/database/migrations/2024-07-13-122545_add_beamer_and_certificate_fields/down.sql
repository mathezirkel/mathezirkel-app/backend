ALTER TABLE "events"
    DROP COLUMN certificate_signature_date,
    DROP COLUMN certificate_default_signature,
    DROP COLUMN number_of_available_beamers;
