ALTER TABLE "events"
    ADD COLUMN certificate_signature_date DATE,
    ADD COLUMN certificate_default_signature TEXT,
    ADD COLUMN number_of_available_beamers INT;
