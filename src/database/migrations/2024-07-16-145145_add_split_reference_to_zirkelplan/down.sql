ALTER TABLE "zirkel_plan_entries"
    DROP COLUMN reference_entry_id;

DROP TRIGGER IF EXISTS cascade_zirkel_plan_entry_updates_trigger ON zirkel_plan_entries;
DROP FUNCTION IF EXISTS cascade_zirkel_plan_entry_updates;