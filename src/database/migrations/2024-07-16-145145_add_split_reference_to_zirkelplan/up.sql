ALTER TABLE "zirkel_plan_entries"
    ADD COLUMN reference_entry_id UUID REFERENCES zirkel_plan_entries(id) ON DELETE SET NULL;

CREATE OR REPLACE FUNCTION cascade_zirkel_plan_entry_updates()
RETURNS TRIGGER AS $$
BEGIN
    -- Check if reference_entry_id is not null
    IF NEW.reference_entry_id IS NOT NULL THEN

        -- Disable triggers to prevent recursion
        PERFORM set_config('session_replication_role', 'replica', true);

        -- Update all related rows with the same reference_entry_id
        UPDATE zirkel_plan_entries
        SET
            instructor_extension_uuids = NEW.instructor_extension_uuids,
            topic = NEW.topic,
            beamer = NEW.beamer,
            notes = NEW.notes
        WHERE reference_entry_id = NEW.reference_entry_id;

        -- Re-enable triggers
        PERFORM set_config('session_replication_role', 'origin', true);

    END IF;

    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER cascade_zirkel_plan_entry_updates_trigger
AFTER INSERT OR UPDATE ON zirkel_plan_entries 
FOR EACH ROW EXECUTE PROCEDURE cascade_zirkel_plan_entry_updates();