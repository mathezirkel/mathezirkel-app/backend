ALTER TABLE "zirkel_plan_entries"
    ADD COLUMN auto_update BOOLEAN NOT NULL DEFAULT true;

CREATE OR REPLACE FUNCTION cascade_zirkel_plan_entry_updates()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.reference_entry_id IS NULL) OR (NOT NEW.auto_update) OR (OLD.auto_update <> NEW.auto_update) THEN
        RETURN NEW;
    END IF;
    
    UPDATE zirkel_plan_entries
        SET
            auto_update = false
        WHERE reference_entry_id = NEW.reference_entry_id;
            
    -- Check if reference_entry_id is not null
    IF NEW.auto_update THEN

        -- Update all related rows with the same reference_entry_id
        UPDATE zirkel_plan_entries
        SET
            instructor_extension_uuids = NEW.instructor_extension_uuids,
            topic = NEW.topic,
            beamer = NEW.beamer,
            notes = NEW.notes
        WHERE reference_entry_id = NEW.reference_entry_id;

    END IF;

    UPDATE zirkel_plan_entries
        SET
            auto_update = true
        WHERE reference_entry_id = NEW.reference_entry_id;

    RETURN NEW;
END;
$$ language 'plpgsql';