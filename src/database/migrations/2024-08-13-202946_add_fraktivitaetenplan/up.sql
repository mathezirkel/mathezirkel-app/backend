CREATE TYPE room_enum AS ENUM ('talk', 'sport', 'crafts', 'programming', 'outside', 'meetingpoint', 'inside', 'music');

CREATE TABLE "fractivity_rooms" 
(
    id UUID DEFAULT uuid_generate_v4 (),
    event_id UUID REFERENCES events(id) ON DELETE CASCADE NOT NULL,
    name TEXT NOT NULL,
    room_types room_enum[] NOT NULL,
    PRIMARY KEY (id)
);



CREATE TABLE "fractivity_entries"
(
    id UUID DEFAULT uuid_generate_v4 (),
    event_id UUID REFERENCES events(id) ON DELETE CASCADE NOT NULL,
    instructor_extension_uuids UUID[] NOT NULL DEFAULT array[]::uuid[],

    topic TEXT NOT NULL,
    duration INT NOT NULL,
    allowed_rooms UUID[] NOT NULL,
    allowed_starts INT[] NOT NULL,
    preparation_time INT DEFAULT 15 NOT NULL,
    follow_up_time INT DEFAULT 0 NOT NULL,
    start_day DATE NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE "fractivity_distribution"
(
    id UUID DEFAULT uuid_generate_v4 (),
    fractivity_entry_id UUID REFERENCES fractivity_entries(id) ON DELETE CASCADE NOT NULL,
    start_time INT NOT NULL,
    room_id UUID REFERENCES fractivity_rooms(id) ON DELETE CASCADE  NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (fractivity_entry_id)
);
