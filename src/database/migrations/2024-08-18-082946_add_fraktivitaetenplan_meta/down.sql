ALTER TABLE "events"
    DROP COLUMN first_fractivity_time,
    DROP COLUMN last_fractivity_time,
    DROP COLUMN min_intervals_fractivity,
    DROP COLUMN blocked_fractivity_slots;

DROP TYPE blocked_slot;
