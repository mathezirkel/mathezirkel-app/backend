
CREATE TYPE blocked_slot AS (
    start_block INT,
    end_block INT,
    name_block TEXT
);

ALTER TABLE "events"
    ADD COLUMN first_fractivity_time INT NOT NULL DEFAULT 810, -- 13.5 * 60 -> =13:30
    ADD COLUMN last_fractivity_time INT NOT NULL DEFAULT 1530, -- 25.5 * 60 -> =01:30
    ADD COLUMN min_intervals_fractivity INT NOT NULL DEFAULT 15, -- 15 = 15 min
    ADD COLUMN blocked_fractivity_slots blocked_slot[] NOT NULL DEFAULT array[]::blocked_slot[];