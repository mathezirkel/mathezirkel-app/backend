-- !! CAUTION this changes the position of the zirkel_id col, therefor not really straight forward to migrate back (not intended to be done anyway, as this loses data)

ALTER TABLE "extensions"
    ADD COLUMN zirkel_id UUID;
 
-- Populate zirkel_id with the first element of zirkel_ids, or set it to NULL if zirkel_ids is NULL or empty
UPDATE "extensions"
SET zirkel_id = CASE 
                  WHEN zirkel_ids IS NULL OR array_length(zirkel_ids, 1) = 0 THEN NULL
                  ELSE zirkel_ids[1]
                END;

ALTER TABLE "extensions" 
    DROP COLUMN zirkel_ids;