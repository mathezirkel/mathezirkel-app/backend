ALTER TABLE "extensions"
    ADD COLUMN zirkel_ids UUID[] NOT NULL DEFAULT array[]::uuid[];

UPDATE "extensions"
SET zirkel_ids = array[zirkel_id]::uuid[]
WHERE zirkel_id IS NOT NULL;

    
ALTER TABLE "extensions"
    DROP COLUMN zirkel_id;