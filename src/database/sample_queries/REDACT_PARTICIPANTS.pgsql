UPDATE extensions
    SET
        email_self = 'REDACTED',
        additional_emails = array[]::text[],
        notes = 'REDACTED',
        nutrition = 'redacted',
        food_restriction = 'REDACTED',
        fee = -1,
        arrival_notes = 'REDACTED',
        departure_notes = 'REDACTED',
        room_partner_wishes = array[]::text[],
        zirkel_partner_wishes = array[]::text[],
        medical_notes = array[]::text[];

UPDATE participants
    SET
        gender = '',
        street = 'REDACTED',
        street_number = 'REDACTED',
        postal_code = 'REDACTED',
        city = 'REDACTED',
        country = 'REDACTED',
        notes = '';

UPDATE participants
    SET
        family_name = 'REDACTED',
        given_name = 'REDACTED',
        call_name = '',
        birth_date = '0001-01-01',
        sex = 'redacted'
    WHERE NOT EXISTS (
        SELECT 1
        FROM extensions AS e
        JOIN events AS ev ON ev.id = e.event_id
        WHERE e.participant_id = participants.id
        AND ev.start_date >= '2022-09-30'
        ORDER BY ev.start_date DESC
        LIMIT 1
    );