DELETE FROM participants
    WHERE NOT EXISTS (
        SELECT 1
        FROM extensions AS e
        WHERE e.participant_id = participants.id
        LIMIT 1
    );