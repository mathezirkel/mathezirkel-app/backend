// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "blocked_slot"))]
    pub struct BlockedSlot;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "classyear_enum"))]
    pub struct ClassyearEnum;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "eventtype_enum"))]
    pub struct EventtypeEnum;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "nutrition_enum"))]
    pub struct NutritionEnum;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "room_enum"))]
    pub struct RoomEnum;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "sex_enum"))]
    pub struct SexEnum;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "traveltype_enum"))]
    pub struct TraveltypeEnum;
}

diesel::table! {
    contacts (id) {
        id -> Uuid,
        extension_id -> Uuid,
        name -> Text,
        name_created_at -> Timestamp,
        name_updated_at -> Timestamp,
        telephone -> Text,
        telephone_created_at -> Timestamp,
        telephone_updated_at -> Timestamp,
        created_at -> Timestamp,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::EventtypeEnum;
    use super::sql_types::BlockedSlot;

    events (id) {
        id -> Uuid,
        #[sql_name = "type"]
        type_ -> EventtypeEnum,
        year -> Int4,
        name -> Text,
        start_date -> Date,
        end_date -> Date,
        default_fee -> Int4,
        give_instructors_full_read_access -> Bool,
        statistics -> Json,
        purpose_of_business_trip -> Nullable<Text>,
        overnight_costs -> Nullable<Float8>,
        additional_costs -> Nullable<Float8>,
        reason_of_additional_costs -> Nullable<Text>,
        budget_section -> Nullable<Text>,
        budget_item -> Nullable<Text>,
        cost_center_budget -> Nullable<Text>,
        cost_type -> Nullable<Text>,
        cost_center_cost_and_activity_accounting -> Nullable<Text>,
        cost_object -> Nullable<Text>,
        certificate_signature_date -> Nullable<Date>,
        certificate_default_signature -> Nullable<Text>,
        number_of_available_beamers -> Nullable<Int4>,
        first_fractivity_time -> Int4,
        last_fractivity_time -> Int4,
        min_intervals_fractivity -> Int4,
        blocked_fractivity_slots -> Array<BlockedSlot>,
    }
}

diesel::table! {
    export_timestamps (id) {
        id -> Uuid,
        key -> Text,
        export_time -> Timestamp,
        successful_jobs -> Int4,
        total_pending_jobs -> Int4,
        failure -> Bool,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::ClassyearEnum;
    use super::sql_types::NutritionEnum;
    use super::sql_types::TraveltypeEnum;

    extensions (id) {
        id -> Uuid,
        participant_id -> Uuid,
        event_id -> Uuid,
        certificate -> Bool,
        participates -> Bool,
        email_self -> Nullable<Text>,
        email_self_created_at -> Timestamp,
        email_self_updated_at -> Timestamp,
        additional_emails -> Array<Text>,
        additional_emails_created_at -> Timestamp,
        additional_emails_updated_at -> Timestamp,
        notes -> Nullable<Text>,
        notes_created_at -> Timestamp,
        notes_updated_at -> Timestamp,
        reason_of_signoff -> Nullable<Text>,
        time_of_signoff -> Nullable<Timestamp>,
        class_year -> Nullable<ClassyearEnum>,
        nutrition -> Nullable<NutritionEnum>,
        nutrition_created_at -> Timestamp,
        nutrition_updated_at -> Timestamp,
        food_restriction -> Nullable<Text>,
        food_restriction_created_at -> Timestamp,
        food_restriction_updated_at -> Timestamp,
        fee -> Nullable<Int4>,
        fee_created_at -> Timestamp,
        fee_updated_at -> Timestamp,
        confirmed -> Bool,
        registration_timestamp -> Nullable<Timestamp>,
        arrival -> Nullable<TraveltypeEnum>,
        arrival_notes -> Nullable<Text>,
        arrival_notes_created_at -> Timestamp,
        arrival_notes_updated_at -> Timestamp,
        departure -> Nullable<TraveltypeEnum>,
        departure_notes -> Nullable<Text>,
        departure_notes_created_at -> Timestamp,
        departure_notes_updated_at -> Timestamp,
        room_partner_wishes -> Array<Text>,
        room_partner_wishes_created_at -> Timestamp,
        room_partner_wishes_updated_at -> Timestamp,
        zirkel_partner_wishes -> Array<Text>,
        zirkel_partner_wishes_created_at -> Timestamp,
        zirkel_partner_wishes_updated_at -> Timestamp,
        medical_notes -> Array<Text>,
        medical_notes_created_at -> Timestamp,
        medical_notes_updated_at -> Timestamp,
        topic_wishes -> Array<Text>,
        fractivity_wishes -> Array<Text>,
        leaving_premise -> Bool,
        carpool_data_sharing -> Bool,
        remove_ticks -> Bool,
        room_number -> Nullable<Text>,
        instruments -> Array<Text>,
        pool -> Bool,
        telephone -> Nullable<Text>,
        telephone_created_at -> Timestamp,
        telephone_updated_at -> Timestamp,
        has_signed_up -> Bool,
        signup_photo -> Nullable<Text>,
        signup_photo_created_at -> Timestamp,
        signup_photo_updated_at -> Timestamp,
        zirkel_ids -> Array<Uuid>,
    }
}

diesel::table! {
    fractivity_distribution (id) {
        id -> Uuid,
        fractivity_entry_id -> Uuid,
        start_time -> Int4,
        room_id -> Uuid,
    }
}

diesel::table! {
    fractivity_entries (id) {
        id -> Uuid,
        event_id -> Uuid,
        instructor_extension_uuids -> Array<Uuid>,
        topic -> Text,
        duration -> Int4,
        allowed_rooms -> Array<Uuid>,
        allowed_starts -> Array<Int4>,
        preparation_time -> Int4,
        follow_up_time -> Int4,
        start_day -> Date,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::RoomEnum;

    fractivity_rooms (id) {
        id -> Uuid,
        event_id -> Uuid,
        name -> Text,
        room_types -> Array<RoomEnum>,
        priority -> Int4,
    }
}

diesel::table! {
    instructor_extension_to_zirkel_mapping (id) {
        id -> Uuid,
        instructor_extension_id -> Uuid,
        zirkel_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::NutritionEnum;
    use super::sql_types::TraveltypeEnum;

    instructor_extensions (id) {
        id -> Uuid,
        instructor_id -> Uuid,
        event_id -> Uuid,
        nutrition -> Nullable<NutritionEnum>,
        nutrition_created_at -> Timestamp,
        nutrition_updated_at -> Timestamp,
        food_restriction -> Nullable<Text>,
        food_restriction_created_at -> Timestamp,
        food_restriction_updated_at -> Timestamp,
        arrival -> Nullable<TraveltypeEnum>,
        arrival_notes -> Nullable<Text>,
        arrival_notes_created_at -> Timestamp,
        arrival_notes_updated_at -> Timestamp,
        departure -> Nullable<TraveltypeEnum>,
        departure_notes -> Nullable<Text>,
        departure_notes_created_at -> Timestamp,
        departure_notes_updated_at -> Timestamp,
        has_contract -> Bool,
        contract_with -> Nullable<Text>,
        contract_with_created_at -> Timestamp,
        contract_with_updated_at -> Timestamp,
        room_number -> Nullable<Text>,
        faculty -> Nullable<Text>,
        chair -> Nullable<Text>,
        personnel_number -> Nullable<Text>,
        personnel_number_created_at -> Timestamp,
        personnel_number_updated_at -> Timestamp,
        job_title -> Nullable<Text>,
        email_work -> Nullable<Text>,
        telephone_work -> Nullable<Text>,
        start_of_travel -> Nullable<Timestamp>,
        arrival_at_business_location -> Nullable<Timestamp>,
        start_of_business_activities -> Nullable<Timestamp>,
        end_of_business_activities -> Nullable<Timestamp>,
        start_of_return_journey -> Nullable<Timestamp>,
        end_of_travel -> Nullable<Timestamp>,
        accommodation_costs -> Nullable<Float8>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::SexEnum;

    instructors (id) {
        id -> Uuid,
        last_update -> Timestamp,
        keycloak_id -> Text,
        keycloak_username -> Nullable<Text>,
        given_name -> Nullable<Text>,
        family_name -> Nullable<Text>,
        call_name -> Nullable<Text>,
        birth_date -> Nullable<Date>,
        sex -> Nullable<SexEnum>,
        gender -> Nullable<Text>,
        pronoun -> Nullable<Text>,
        street -> Nullable<Text>,
        street_number -> Nullable<Text>,
        postal_code -> Nullable<Text>,
        city -> Nullable<Text>,
        country -> Nullable<Text>,
        email -> Nullable<Text>,
        telephone -> Nullable<Text>,
        abbreviation -> Nullable<Text>,
        iban -> Nullable<Text>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::SexEnum;

    participants (id) {
        id -> Uuid,
        family_name -> Text,
        family_name_created_at -> Timestamp,
        family_name_updated_at -> Timestamp,
        given_name -> Text,
        given_name_created_at -> Timestamp,
        given_name_updated_at -> Timestamp,
        call_name -> Nullable<Text>,
        call_name_created_at -> Timestamp,
        call_name_updated_at -> Timestamp,
        birth_date -> Date,
        birth_date_created_at -> Timestamp,
        birth_date_updated_at -> Timestamp,
        sex -> SexEnum,
        sex_created_at -> Timestamp,
        sex_updated_at -> Timestamp,
        gender -> Nullable<Text>,
        gender_created_at -> Timestamp,
        gender_updated_at -> Timestamp,
        street -> Text,
        street_created_at -> Timestamp,
        street_updated_at -> Timestamp,
        street_number -> Text,
        street_number_created_at -> Timestamp,
        street_number_updated_at -> Timestamp,
        postal_code -> Text,
        postal_code_created_at -> Timestamp,
        postal_code_updated_at -> Timestamp,
        city -> Text,
        city_created_at -> Timestamp,
        city_updated_at -> Timestamp,
        country -> Text,
        country_created_at -> Timestamp,
        country_updated_at -> Timestamp,
        notes -> Nullable<Text>,
        notes_created_at -> Timestamp,
        notes_updated_at -> Timestamp,
    }
}

diesel::table! {
    reconnection_hashes (hash, participant_id) {
        hash -> Text,
        participant_id -> Uuid,
        timestamp -> Timestamp,
    }
}

diesel::table! {
    zirkel (id) {
        id -> Uuid,
        event_id -> Uuid,
        name -> Text,
        topics -> Array<Text>,
        room -> Nullable<Text>,
    }
}

diesel::table! {
    zirkel_plan_entries (id) {
        id -> Uuid,
        zirkel_plan_slot_id -> Uuid,
        zirkel_id -> Uuid,
        instructor_extension_uuids -> Array<Uuid>,
        topic -> Nullable<Text>,
        beamer -> Bool,
        notes -> Nullable<Text>,
        reference_entry_id -> Nullable<Uuid>,
        auto_update -> Bool,
    }
}

diesel::table! {
    zirkel_plan_slots (id) {
        id -> Uuid,
        event_id -> Uuid,
        start_time -> Timestamp,
        end_time -> Timestamp,
        name -> Text,
        certificate -> Bool,
        beamer -> Bool,
    }
}

diesel::joinable!(contacts -> extensions (extension_id));
diesel::joinable!(extensions -> events (event_id));
diesel::joinable!(extensions -> participants (participant_id));
diesel::joinable!(fractivity_distribution -> fractivity_entries (fractivity_entry_id));
diesel::joinable!(fractivity_distribution -> fractivity_rooms (room_id));
diesel::joinable!(fractivity_entries -> events (event_id));
diesel::joinable!(fractivity_rooms -> events (event_id));
diesel::joinable!(instructor_extension_to_zirkel_mapping -> instructor_extensions (instructor_extension_id));
diesel::joinable!(instructor_extension_to_zirkel_mapping -> zirkel (zirkel_id));
diesel::joinable!(instructor_extensions -> events (event_id));
diesel::joinable!(instructor_extensions -> instructors (instructor_id));
diesel::joinable!(reconnection_hashes -> participants (participant_id));
diesel::joinable!(zirkel -> events (event_id));
diesel::joinable!(zirkel_plan_entries -> zirkel (zirkel_id));
diesel::joinable!(zirkel_plan_entries -> zirkel_plan_slots (zirkel_plan_slot_id));
diesel::joinable!(zirkel_plan_slots -> events (event_id));

diesel::allow_tables_to_appear_in_same_query!(
    contacts,
    events,
    export_timestamps,
    extensions,
    fractivity_distribution,
    fractivity_entries,
    fractivity_rooms,
    instructor_extension_to_zirkel_mapping,
    instructor_extensions,
    instructors,
    participants,
    reconnection_hashes,
    zirkel,
    zirkel_plan_entries,
    zirkel_plan_slots,
);
