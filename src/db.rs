use crate::error_handler::CustomError;
use chrono::NaiveDateTime;
use diesel::dsl::sql_query;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use diesel::RunQueryDsl;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use lazy_static::lazy_static;
use r2d2;
use serde::{Deserialize, Serialize};
use std::env;
use std::time::Duration;

// Diesel and r2d2 Boilerplate
type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;
pub type DbConnection = r2d2::PooledConnection<ConnectionManager<PgConnection>>;
pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./src/database/migrations");

// psql "postgresql://$DATABASE_USER:$DATABASE_PASSWORD@$DATABASE_HOST:$DATABASE_PORT/$DATABASE_NAME"
// generate the static r2d2 Connection pool
lazy_static! {
    static ref POOL: Pool = {
        let db_host =
            env::var("DATABASE_HOST").expect("Database host ('DATABASE_HOST') not set in .env");
        let db_port =
            env::var("DATABASE_PORT").expect("Database port ('DATABASE_PORT') not set in .env");
        let db_user =
            env::var("DATABASE_USER").expect("Database user ('DATABASE_USER') not set in .env");
        let db_password = env::var("DATABASE_PASSWORD")
            .expect("Database password ('DATABASE_PASSWORD') not set in .env");
        let db_name =
            env::var("DATABASE_NAME").expect("Database name ('DATABASE_NAME') not set in .env");

        let manager = ConnectionManager::<PgConnection>::new(format!(
            "postgres://{}:{}@{}:{}/{}",
            db_user, db_password, db_host, db_port, db_name
        ));
        Pool::builder()
            .connection_timeout(Duration::from_secs(3))
            .max_size(8)
            .build(manager)
            .expect("Failed to create db pool")
    };
}

/// Initialize the static r2d2 Pool, check connectivity
///
/// Caution: calling this will apply pending migrations.
/// Be aware of this on production Servers
pub fn init() {
    lazy_static::initialize(&POOL);
    let mut conn = connection().expect("Failed to get db connection");
    conn.run_pending_migrations(MIGRATIONS).unwrap();
}

/// Get a new connection to the database from the pre-established connection Pool
///
/// Make sure, `init` was run before calling this
pub fn connection() -> Result<DbConnection, CustomError> {
    POOL.get()
        .map_err(|e| CustomError::new(500, format!("Failed getting db connection: {}", e)))
}

table! {
    time_container (id) {
        id-> Int4,
        current_time -> Timestamp,
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, QueryableByName)]
#[diesel(table_name = time_container)]
struct TimeContainer {
    pub id: i32,
    pub current_time: NaiveDateTime,
}

///
/// To avoid server-time and database time being out of sync, database is the ssot for timestamps
///
pub fn get_database_current_time() -> Result<NaiveDateTime, CustomError> {
    Ok(
        sql_query("SELECT 1 as id, CURRENT_TIMESTAMP as current_time")
            .get_result::<TimeContainer>(&mut connection()?)?
            .current_time,
    )
}
