use crate::auth::{verify_bearer_token, Role};
use crate::models::{ExportTimestampStatus, UpsertExportTimestampStatus};
use crate::scripts::{gql_loopback, jq, latex, python};
use crate::webdav::{delete, mkdir, put};
use actix_web::{
    web::{self},
    Error, HttpResponse,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use mathezirkel_app_scriptserver_base64::{
    base_64_decode_string_to_bytes, base_64_decode_string_to_string,
    base_64_encode_string_to_string,
};
use mathezirkel_app_scriptserver_interfaces::{ScriptFile, ScriptResponse};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct ExportRequestPostVariables {
    asynchronous: bool, // on false, wait for the job to finish AND return the intermediate results
    jobs: Vec<ExportJobDescription>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)] // This is a crazy powerful thing https://serde.rs/enum-representations.html#untagged
enum ExportJobType {
    ExportJobGraphql {
        gql_query: String,
    },
    ExportJobJq {
        json_input: String,
        jq_filter: String,
    },
    ExportJobLatex {
        main_doc_file_name: String,
        main_doc_file_signature: String,
    },
    ExportJobPython {
        entrypoint_file_name: String,
        entrypoint_file_signature: String,
        returns_output_file_names: Vec<String>,
    },
    ExportBase64Encode {
        input_plain: String,
    },
    ExportBase64Decode {
        input_base_64_encoded: String,
    },
    ExportJobWebdav {
        file_content_base_64_encoded: String,
        file_name_with_path: String,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct ExportJobFileMapping {
    file_name: String,
    source: MappingSource,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct ExportJobArgumentsMapping {
    argument_name: String,
    source: MappingSource,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct MappingSource {
    std_out: bool,
    file_name: Option<String>,
    result_index: i32,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct ExportJobInputMapping {
    arguments: Vec<ExportJobArgumentsMapping>,
    files: Vec<ExportJobFileMapping>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct ExportJobStage {
    input_mapping: ExportJobInputMapping,
    job_type: ExportJobType,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct WebdavFolderRequirement {
    path: String,
    clear: bool,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ExportJobDescription {
    key: String,
    input: ScriptResponse,
    required_webdav_folders: Vec<WebdavFolderRequirement>,
    stages: Vec<ExportJobStage>,
}

#[derive(Serialize, Deserialize)]
pub struct ExportRequestAnswer {
    asynchronous: bool,
    answers: Vec<ExportJobStatus>,
}

#[derive(Serialize, Deserialize)]
pub struct ExportJobStatus {
    key: String,
    finished: bool,
    response: Option<ScriptResponse>,
}

pub async fn export_route(
    auth: BearerAuth,
    post_var: web::Json<ExportRequestPostVariables>,
) -> Result<HttpResponse, Error> {
    let bearer_token = String::from(auth.token());

    let (access, _claims) = verify_bearer_token(bearer_token.clone(), Some(Role::Export)).await;

    if access {
        let status = export_controller(&bearer_token, &post_var.jobs, post_var.asynchronous).await;

        Ok(HttpResponse::Ok()
            .append_header(("Content-Type", "text/json"))
            .json(status))
    } else {
        // error, if the bearer-auth challenge was failed
        return Ok(HttpResponse::Unauthorized()
            .append_header(("Content-Type", "text/plain"))
            .body("Unauthorized: Bearer Token did not authenticate you to access this server"));
    }
}

async fn export_controller(
    bearer_token: &String,
    manifest: &Vec<ExportJobDescription>,
    asynchronous: bool,
) -> ExportRequestAnswer {
    let mut answers: Vec<ExportJobStatus> = Vec::new();

    let mut folder_name_counts = HashMap::new();
    for job_desc in manifest.iter() {
        for folder_req in &job_desc.required_webdav_folders {
            // Update count for the folder_name_with_path
            *folder_name_counts
                .entry(folder_req.path.clone())
                .or_insert(0) += if folder_req.clear { 2 } else { 1 };
        }
    }
    for folder_name in folder_name_counts.keys() {
        let num_files_from_folder = folder_name_counts[folder_name];
        if num_files_from_folder > 1 {
            delete(folder_name).await;
        }

        // we can call mkdir(folder_name).await; to create a folder.
        // Though we need to call this multiple times for the folder_name "foo/bar/baz" we need to call on "foo", then on "foo/bar" and last on "foo/bar/baz".
        let mut components = folder_name.split('/').filter(|&c| !c.is_empty());
        let mut current_path = String::from("/");
        while let Some(component) = components.next() {
            current_path.push_str(component);

            debug!("Try create path {}", current_path);
            mkdir(&current_path).await;

            current_path.push('/');
        }
    }

    for job_desc in manifest.iter() {
        answers
            .push(delegate_export(String::from(bearer_token), job_desc.clone(), asynchronous).await)
    }

    ExportRequestAnswer {
        asynchronous,
        answers,
    }
}

async fn delegate_export(
    bearer_token: String,
    job_description: ExportJobDescription,
    asynchronous: bool,
) -> ExportJobStatus {
    let key = String::from(&job_description.key);

    // start the work
    let worker_status = actix_web::rt::spawn(async move {
        perform_export(&bearer_token, &job_description, !asynchronous).await
    });

    if asynchronous {
        ExportJobStatus {
            key,
            finished: false,
            response: None,
        }
    } else {
        let worker_result = worker_status.await.unwrap();
        worker_result
    }
}

async fn perform_export(
    bearer_token: &String,
    job_description: &ExportJobDescription,
    return_result: bool,
) -> ExportJobStatus {
    let key = String::from(&job_description.key);
    let initial_input = job_description.input.clone();
    let mut work_data: Vec<ScriptResponse> = Vec::new();
    work_data.push(initial_input);

    let mut job_upsert_object = UpsertExportTimestampStatus {
        key: String::from(&key),
        failure: false,
        successful_jobs: 0,
        total_pending_jobs: (job_description.stages.len() as i32),
    };
    let _ = ExportTimestampStatus::upsert(&job_upsert_object);

    for current_stage in &job_description.stages {
        // / generate the input for the next stage
        // script_input_variables
        let get_value_function = |request_supplied: &str, values_argument_name: &str| -> String {
            let mut res_str = String::from(request_supplied);

            let input_mapping = current_stage.input_mapping.clone();

            // iterate over all argument mappings if they conform to the currently viewed
            for argument_mapping in input_mapping.arguments {
                if argument_mapping.argument_name == values_argument_name {
                    // maps onto current argument
                    if let Some(source_response) =
                        work_data.get(argument_mapping.source.result_index as usize)
                    {
                        if argument_mapping.source.std_out {
                            // Info comes from std_out
                            res_str = String::from(&source_response.std_out);
                        } else {
                            if let Some(source_file_name) = argument_mapping.source.file_name {
                                // Info comes from a file
                                for real_file in &source_response.files {
                                    if source_file_name == real_file.name {
                                        // real file exists in the result before the mapping instruction, that has the name of the instruction
                                        res_str = String::from(&real_file.content_base_64);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return res_str;
        };
        let populated_job = match &current_stage.job_type {
            ExportJobType::ExportBase64Decode {
                input_base_64_encoded,
            } => ExportJobType::ExportBase64Decode {
                input_base_64_encoded: get_value_function(
                    input_base_64_encoded,
                    "input_base_64_encoded",
                ),
            },
            ExportJobType::ExportBase64Encode { input_plain } => {
                ExportJobType::ExportBase64Encode {
                    input_plain: get_value_function(input_plain, "input_plain"),
                }
            }
            ExportJobType::ExportJobGraphql { gql_query } => ExportJobType::ExportJobGraphql {
                gql_query: get_value_function(gql_query, "gql_query"),
            },
            ExportJobType::ExportJobJq {
                json_input,
                jq_filter,
            } => ExportJobType::ExportJobJq {
                json_input: get_value_function(json_input, "json_input"),
                jq_filter: get_value_function(jq_filter, "jq_filter"),
            },
            ExportJobType::ExportJobWebdav {
                file_content_base_64_encoded,
                file_name_with_path,
            } => ExportJobType::ExportJobWebdav {
                file_content_base_64_encoded: get_value_function(
                    file_content_base_64_encoded,
                    "file_content_base_64_encoded",
                ),
                file_name_with_path: get_value_function(file_name_with_path, "file_name_with_path"),
            },
            ExportJobType::ExportJobLatex {
                main_doc_file_name,
                main_doc_file_signature,
            } => ExportJobType::ExportJobLatex {
                main_doc_file_name: get_value_function(main_doc_file_name, "main_doc_file_name"),
                main_doc_file_signature: get_value_function(
                    main_doc_file_signature,
                    "main_doc_file_signature",
                ),
            },
            ExportJobType::ExportJobPython {
                entrypoint_file_name,
                entrypoint_file_signature,
                returns_output_file_names,
            } => ExportJobType::ExportJobPython {
                entrypoint_file_name: get_value_function(
                    entrypoint_file_name,
                    "entrypoint_file_name",
                ),
                entrypoint_file_signature: get_value_function(
                    entrypoint_file_signature,
                    "entrypoint_file_signature",
                ),
                returns_output_file_names: returns_output_file_names.clone(),
            },
        };
        // files
        let mut populated_files: Vec<ScriptFile> = Vec::new();

        let input_mapping = current_stage.input_mapping.clone();

        // iterate over all file mappings to fulfill all of them into the input
        for file_mapping in input_mapping.files {
            if let Some(source_response) = work_data.get(file_mapping.source.result_index as usize)
            {
                if file_mapping.source.std_out {
                    // Info comes from std_out
                    populated_files.push(ScriptFile {
                        name: String::from(&file_mapping.file_name),
                        content_base_64: String::from(&source_response.std_out),
                    });
                } else {
                    if let Some(source_file_name) = file_mapping.source.file_name {
                        // Info comes from a file
                        for real_file in &source_response.files {
                            if source_file_name == real_file.name {
                                // real file exists in the result before the mapping instruction, that has the name of the instruction
                                populated_files.push(ScriptFile {
                                    name: String::from(&file_mapping.file_name),
                                    content_base_64: String::from(&real_file.content_base_64),
                                });
                            }
                        }
                    }
                }
            }
        }

        // debug!("{:?}", populated_job); // enable if jobs crap out unexpectedly

        // / do the real work as input to the next stage
        match populated_job {
            ExportJobType::ExportBase64Decode {
                input_base_64_encoded,
            } => {
                let decoded_data = base_64_decode_string_to_string(&input_base_64_encoded);
                work_data.push(ScriptResponse {
                    files: Vec::new(),
                    std_err: String::from(""),
                    std_out: decoded_data,
                    success: true,
                    async_id: None,
                });
            }
            ExportJobType::ExportBase64Encode { input_plain } => {
                let encoded_data = base_64_encode_string_to_string(&input_plain);
                work_data.push(ScriptResponse {
                    files: Vec::new(),
                    std_err: String::from(""),
                    std_out: encoded_data,
                    success: true,
                    async_id: None,
                });
            }
            ExportJobType::ExportJobGraphql { gql_query } => {
                work_data.push(gql_loopback(bearer_token, &gql_query).await);
            }
            ExportJobType::ExportJobJq {
                json_input,
                jq_filter,
            } => {
                work_data.push(jq(&json_input, &jq_filter).await);
            }
            ExportJobType::ExportJobWebdav {
                file_content_base_64_encoded,
                file_name_with_path,
            } => {
                let content = base_64_decode_string_to_bytes(&file_content_base_64_encoded);
                let webdav_success = put(&file_name_with_path, &content).await;
                work_data.push(ScriptResponse {
                    files: Vec::new(),
                    std_err: if webdav_success {
                        String::from("")
                    } else {
                        String::from(format!(
                            "Error storing the file '{}' on webdav",
                            &file_name_with_path
                        ))
                    },
                    std_out: String::from(""),
                    success: webdav_success,
                    async_id: None,
                });
            }
            ExportJobType::ExportJobLatex {
                main_doc_file_name,
                main_doc_file_signature,
            } => {
                if let Some(index) = populated_files
                    .iter()
                    .position(|script_file| script_file.name == main_doc_file_name)
                {
                    let main_doc_file = populated_files.remove(index);

                    work_data.push(
                        latex(&main_doc_file, &main_doc_file_signature, &populated_files).await,
                    );
                } else {
                    work_data.push(ScriptResponse {
                        files: Vec::new(),
                        std_err: String::from(format!(
                            "No file found with main_doc_file_name '{}'",
                            &main_doc_file_name
                        )),
                        std_out: String::from(""),
                        success: false,
                        async_id: None,
                    });
                }
            }
            ExportJobType::ExportJobPython {
                entrypoint_file_name,
                entrypoint_file_signature,
                returns_output_file_names,
            } => {
                if let Some(index) = populated_files
                    .iter()
                    .position(|script_file| script_file.name == entrypoint_file_name)
                {
                    let entrypoint_file = populated_files.remove(index);

                    work_data.push(
                        python(
                            &entrypoint_file,
                            &entrypoint_file_signature,
                            &populated_files,
                            &returns_output_file_names,
                        )
                        .await,
                    );
                } else {
                    work_data.push(ScriptResponse {
                        files: Vec::new(),
                        std_err: String::from(format!(
                            "No file found with entrypoint_file_name '{}'",
                            &entrypoint_file_name
                        )),
                        std_out: String::from(""),
                        success: false,
                        async_id: None,
                    });
                }
            }
        }

        // / advance the stage
        if let Some(response) = work_data.last() {
            // abort if necessary
            if !response.success {
                // report failure
                job_upsert_object.failure = true;
                let _ = ExportTimestampStatus::upsert(&job_upsert_object);

                return ExportJobStatus {
                    key,
                    finished: true,
                    response: Some(response.clone()),
                };
            } else {
                // report stage advance
                job_upsert_object.successful_jobs = job_upsert_object.successful_jobs + 1;
                let _ = ExportTimestampStatus::upsert(&job_upsert_object);
            }
        }
    }

    if return_result {
        if let Some(response) = work_data.last() {
            return ExportJobStatus {
                key,
                finished: true,
                response: Some(response.clone()),
            };
        }
    }
    ExportJobStatus {
        key,
        finished: true,
        response: None,
    }
}
