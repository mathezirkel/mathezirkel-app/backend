use crate::color::{
    generate_colors_for_fractivity_distributions, generate_colors_for_zirkelplan_entries,
};
use crate::database::{
    events, extensions, instructor_extension_to_zirkel_mapping, instructor_extensions,
    participants, zirkel,
};
use crate::db::{self, get_database_current_time};
use crate::models::{FractivityDistribution, ZirkelPlanEntry, ZirkelPlanSlot};
use crate::{auth::Role, models::Instructor};
use diesel::dsl::sql;
use diesel::prelude::*;
use diesel::sql_types::Bool;
use diesel::QueryDsl;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};
use tokio::sync::RwLock as AsyncRwLock;
use uuid::Uuid;

/// Database wrapper to make is usable for Juniper
#[derive(Clone, Default)]
pub struct Context {
    roles: Vec<String>,
    instructor_uuid: Option<Uuid>,
    has_access_to_instructor_uuids_cache: Arc<AsyncRwLock<Option<Vec<Uuid>>>>,
    has_access_to_instructor_extension_uuids_cache: Arc<AsyncRwLock<Option<Vec<Uuid>>>>,
    has_access_to_participant_uuids_cache: Arc<RwLock<Option<Vec<Uuid>>>>,
    has_access_to_participant_extension_uuids_cache: Arc<RwLock<Option<Vec<Uuid>>>>,
    zirkelplan_colors_cache: Arc<RwLock<HashMap<Uuid, HashMap<Uuid, String>>>>,
    fractivity_distribution_colors_cache: Arc<RwLock<HashMap<Uuid, HashMap<Uuid, String>>>>,
}

impl Context {
    pub fn new(roles: Vec<String>, instructor_uuid: Option<Uuid>) -> Context {
        Context {
            roles,
            instructor_uuid,
            has_access_to_instructor_uuids_cache: Arc::new(AsyncRwLock::new(None)),
            has_access_to_instructor_extension_uuids_cache: Arc::new(AsyncRwLock::new(None)),
            has_access_to_participant_uuids_cache: Arc::new(RwLock::new(None)),
            has_access_to_participant_extension_uuids_cache: Arc::new(RwLock::new(None)),
            zirkelplan_colors_cache: Arc::new(RwLock::new(HashMap::new())),
            fractivity_distribution_colors_cache: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub fn check_role(&self, role: Role) -> bool {
        self.roles.contains(&role.value())
    }

    pub async fn has_access_to_instructor_uuid(&self, instructor_uuid: &Uuid) -> bool {
        let access_uuids = self.has_access_to_instructor_uuids().await;

        return access_uuids.contains(&instructor_uuid);
    }

    pub async fn has_at_least_one_of_these_instructor_extension_uuids(
        &self,
        instructor_extension_uuids: &Vec<Uuid>,
    ) -> bool {
        let access_uuids = self.has_access_to_instructor_extension_uuids().await;

        return access_uuids
            .iter()
            .any(|ext_uuid| instructor_extension_uuids.contains(&ext_uuid));
    }

    pub async fn has_access_to_instructor_uuids(&self) -> Vec<Uuid> {
        {
            let mut cache = self.has_access_to_instructor_uuids_cache.write().await;
            if cache.is_none() {
                let uncached = self.has_access_to_instructor_uuids_uncached().await;
                *cache = Some(uncached);
            }
        }

        let cache = self.has_access_to_instructor_uuids_cache.read().await;

        cache.as_ref().unwrap().clone()
    }

    pub async fn has_access_to_instructor_extension_uuids(&self) -> Vec<Uuid> {
        {
            let mut cache = self
                .has_access_to_instructor_extension_uuids_cache
                .write()
                .await;
            if cache.is_none() {
                let uncached = self
                    .has_access_to_instructor_extension_uuids_uncached()
                    .await;
                *cache = Some(uncached);
            }
        }

        let cache = self
            .has_access_to_instructor_extension_uuids_cache
            .read()
            .await;

        cache.as_ref().unwrap().clone()
    }

    pub fn has_access_to_participant_uuids(&self) -> Vec<Uuid> {
        {
            let mut cache = self.has_access_to_participant_uuids_cache.write().unwrap();
            if cache.is_none() {
                let uncached = self.has_access_to_participant_uuids_uncached();
                *cache = Some(uncached);
            }
        }

        let cache = self.has_access_to_participant_uuids_cache.read().unwrap();

        cache.as_ref().unwrap().clone()
    }

    pub fn has_access_to_participant_extension_uuids(&self) -> Vec<Uuid> {
        {
            let mut cache = self
                .has_access_to_participant_extension_uuids_cache
                .write()
                .unwrap();
            if cache.is_none() {
                let uncached = self.has_access_to_participant_extension_uuids_uncached();
                *cache = Some(uncached);
            }
        }

        let cache = self
            .has_access_to_participant_extension_uuids_cache
            .read()
            .unwrap();

        cache.as_ref().unwrap().clone()
    }

    async fn has_access_to_instructor_uuids_uncached(&self) -> Vec<Uuid> {
        let own_instructor_id = match self.instructor_uuid {
            Some(id) => id,
            None => {
                return Vec::new();
            }
        };

        let instructor = match Instructor::find(own_instructor_id).await {
            Err(_) => {
                return Vec::new();
            }
            Ok(inst) => inst,
        };

        return vec![instructor.id];
    }

    async fn has_access_to_instructor_extension_uuids_uncached(&self) -> Vec<Uuid> {
        let own_instructor_id = match self.instructor_uuid {
            Some(id) => id,
            None => {
                return Vec::new();
            }
        };

        let instructor = match Instructor::find(own_instructor_id).await {
            Err(_) => {
                return Vec::new();
            }
            Ok(inst) => inst,
        };

        return instructor
            .relation_extensions()
            .unwrap_or_default()
            .iter()
            .map(|ext| ext.id)
            .collect();
    }

    fn has_access_to_participant_uuids_uncached(&self) -> Vec<Uuid> {
        let own_instructor_id = match self.instructor_uuid {
            Some(id) => id,
            None => {
                return Vec::new();
            }
        };

        let db_time = match get_database_current_time() {
            Ok(time) => time.date(),
            Err(_) => return Vec::new(),
        };

        let mut connection = match db::connection() {
            Ok(conn) => conn,
            Err(_) => {
                return Vec::new();
            }
        };

        let access_from_event_uuids = match participants::table
            .inner_join(extensions::table.on(extensions::participant_id.eq(participants::id)))
            .inner_join(events::table.on(events::id.eq(extensions::event_id)))
            .inner_join(
                instructor_extensions::table.on(events::id.eq(instructor_extensions::event_id)),
            )
            .select(participants::id)
            .filter(events::give_instructors_full_read_access.eq(true))
            .filter(events::start_date.le(db_time))
            .filter(events::end_date.ge(db_time))
            .filter(instructor_extensions::instructor_id.eq(own_instructor_id))
            .filter(extensions::confirmed.eq(true))
            .filter(extensions::time_of_signoff.is_null())
            .load::<Uuid>(&mut connection)
        {
            Ok(res) => res,
            Err(_) => {
                return Vec::new();
            }
        };

        let access_from_zirkel_uuids = match participants::table
            .inner_join(extensions::table.on(extensions::participant_id.eq(participants::id)))
            .inner_join(zirkel::table.on(sql::<Bool>("zirkel.id = ANY(extensions.zirkel_ids)")))
            .inner_join(
                instructor_extension_to_zirkel_mapping::table
                    .on(zirkel::id.eq(instructor_extension_to_zirkel_mapping::zirkel_id)),
            )
            .inner_join(
                instructor_extensions::table.on(
                    instructor_extension_to_zirkel_mapping::instructor_extension_id
                        .eq(instructor_extensions::id),
                ),
            )
            .select(participants::id)
            .filter(instructor_extensions::instructor_id.eq(own_instructor_id))
            .load::<Uuid>(&mut connection)
        {
            Ok(res) => res,
            Err(_) => {
                return Vec::new();
            }
        };

        let all_access_uuids = union_of_vecs(access_from_event_uuids, access_from_zirkel_uuids);

        return all_access_uuids;
    }

    fn has_access_to_participant_extension_uuids_uncached(&self) -> Vec<Uuid> {
        let own_instructor_id = match self.instructor_uuid {
            Some(id) => id,
            None => {
                return Vec::new();
            }
        };

        let db_time = match get_database_current_time() {
            Ok(time) => time.date(),
            Err(_) => return Vec::new(),
        };

        let mut connection = match db::connection() {
            Ok(conn) => conn,
            Err(_) => {
                return Vec::new();
            }
        };

        let access_from_event_uuids = match extensions::table
            .inner_join(events::table.on(events::id.eq(extensions::event_id)))
            .inner_join(
                instructor_extensions::table.on(events::id.eq(instructor_extensions::event_id)),
            )
            .select(extensions::id)
            .filter(events::give_instructors_full_read_access.eq(true))
            .filter(events::start_date.le(db_time))
            .filter(events::end_date.ge(db_time))
            .filter(instructor_extensions::instructor_id.eq(own_instructor_id))
            .filter(extensions::confirmed.eq(true))
            .filter(extensions::time_of_signoff.is_null())
            .load::<Uuid>(&mut connection)
        {
            Ok(res) => res,
            Err(_) => {
                return Vec::new();
            }
        };

        let access_from_zirkel_uuids = match extensions::table
            .inner_join(zirkel::table.on(sql::<Bool>("zirkel.id = ANY(extensions.zirkel_ids)")))
            .inner_join(events::table.on(events::id.eq(zirkel::event_id)))
            .inner_join(
                instructor_extension_to_zirkel_mapping::table
                    .on(zirkel::id.eq(instructor_extension_to_zirkel_mapping::zirkel_id)),
            )
            .inner_join(
                instructor_extensions::table.on(
                    instructor_extension_to_zirkel_mapping::instructor_extension_id
                        .eq(instructor_extensions::id),
                ),
            )
            .select(extensions::id)
            .filter(instructor_extensions::instructor_id.eq(own_instructor_id))
            .filter(events::start_date.le(db_time))
            .filter(events::end_date.ge(db_time))
            .filter(extensions::confirmed.eq(true))
            .filter(extensions::time_of_signoff.is_null())
            .load::<Uuid>(&mut connection)
        {
            Ok(res) => res,
            Err(_) => {
                return Vec::new();
            }
        };

        let all_access_uuids = union_of_vecs(access_from_event_uuids, access_from_zirkel_uuids);

        return all_access_uuids;
    }

    pub fn get_color_for_zirkel_plan_entry(&self, entry: &ZirkelPlanEntry) -> String {
        let cache_event_key = match ZirkelPlanSlot::find(entry.zirkel_plan_slot_id) {
            Ok(slot) => slot.event_id,
            Err(_) => return String::from("#000000"),
        };

        let mut cache = self.zirkelplan_colors_cache.write().unwrap();

        if !cache.contains_key(&cache_event_key) {
            // fill into the cache
            let new_cache_entry = generate_colors_for_zirkelplan_entries(&cache_event_key);
            cache.insert(cache_event_key.clone(), new_cache_entry);
        }

        // now definitely cached:
        let event_cache = cache.get(&cache_event_key).unwrap();
        let color_result = String::from(
            event_cache
                .get(&entry.id)
                .unwrap_or(&String::from("#000000")),
        );

        return color_result;
    }

    pub fn get_color_for_fractivity_distribution(
        &self,
        distribution: &FractivityDistribution,
    ) -> String {
        let cache_event_key = distribution.fractivity_entry().event_id;

        let mut cache = self.fractivity_distribution_colors_cache.write().unwrap();

        if !cache.contains_key(&cache_event_key) {
            // fill into the cache
            let new_cache_entry = generate_colors_for_fractivity_distributions(&cache_event_key);
            cache.insert(cache_event_key.clone(), new_cache_entry);
        }

        // now definitely cached:
        let event_cache = cache.get(&cache_event_key).unwrap();
        let color_result = String::from(
            event_cache
                .get(&distribution.id)
                .unwrap_or(&String::from("#000000")),
        );

        return color_result;
    }
}

/// To make our Database usable by Juniper, we have to implement a marker trait.
impl juniper::Context for Context {}

fn union_of_vecs<T: Eq + std::hash::Hash + Clone>(vec1: Vec<T>, vec2: Vec<T>) -> Vec<T> {
    let mut set: HashSet<T> = vec1.into_iter().collect();
    set.extend(vec2.into_iter());
    set.into_iter().collect()
}
