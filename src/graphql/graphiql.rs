use actix_web::{http::header::ContentType, HttpResponse, Responder};
use std::env;
use url::Url;

/// Actix answerer-callback to the `/graphiql` URL (GET)
pub async fn graphiql() -> impl Responder {
    let mut url = Url::parse(
        &env::var("APPLICATION_BASE_URL").expect("URL ('APPLICATION_BASE_URL') not set in .env"),
    )
    .unwrap();
    url.path_segments_mut().unwrap().push("graphql");

    let mut html = String::from(include_str!("./graphiql.html"));
    html = html.replace("{{url}}", &url.to_string());

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(html)
}
