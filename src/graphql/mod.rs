mod context;
mod graphiql;
mod mutation;
mod query;
mod schema;

pub use context::Context;
pub use graphiql::graphiql;
pub use mutation::Mutation;
pub use query::Query;
pub use schema::schema;
pub use schema::Schema;

#[macro_export]
macro_rules! graphql_access {
    ($context:expr, $role:expr, $request:expr) => {
        match $context.check_role($role) {
            true => Ok($request),
            false => Err(format!("Doesn't have required role {}", $role.value()))?,
        }
    };
}

#[macro_export]
macro_rules! graphql_access_advanced {
    ($context:expr, $role:expr, $extra:expr, $request:expr) => {{
        let mut acc = $context.check_role($role);

        if !acc {
            let more_access: bool = $extra;

            if more_access {
                acc = true;
            }
        }

        if acc {
            Ok($request)
        } else {
            Err(format!(
                "No access through role {} or instructor_uuid based overwrite expression",
                $role.value()
            ))?
        }
    }};
}

pub use graphql_access;
pub use graphql_access_advanced;
