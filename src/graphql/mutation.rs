use crate::auth::Role;
use crate::fractivity::distribute;
use crate::graphql::{graphql_access, graphql_access_advanced};
use crate::models::{
    add_instructor_extension_to_zirkel_plan_entry_thread_save,
    remove_instructor_extension_from_zirkel_plan_entry_thread_save, Contact, CreateContact,
    CreateEvent, CreateExtension, CreateFractivityDistribution, CreateFractivityEntry,
    CreateFractivityRoom, CreateInstructorExtension, CreateParticipant, CreateZirkel,
    CreateZirkelPlanEntry, CreateZirkelPlanSlot, Event, Extension, ExtensionSignup,
    FractivityDistribution, FractivityEntry, FractivityRoom, InstructorExtension,
    InstructorExtensionToZirkelMapping, Participant, UpdateContact, UpdateEvent, UpdateExtension,
    UpdateFractivityDistribution, UpdateFractivityEntry, UpdateFractivityRoom,
    UpdateInstructorExtension, UpdateParticipant, UpdateSignupExtension, UpdateZirkel,
    UpdateZirkelPlanEntry, UpdateZirkelPlanSlot, Zirkel, ZirkelPlanEntry, ZirkelPlanSlot,
};
use crate::Context;
use juniper::{graphql_object, FieldResult};
use uuid::Uuid;

/// Juniper Mutation Structure, methods documented with macros
pub struct Mutation;
#[graphql_object(context = Context)]
impl Mutation {
    #[graphql(description = "Create a new Participant")]
    fn create_participant(
        context: &Context,
        #[graphql(description = "New Participant")] values: CreateParticipant,
    ) -> FieldResult<Option<Participant>> {
        graphql_access!(context, Role::WriteParticipant, {
            let part = Participant::create(values);
            match &part {
                Ok(participant) => (participant).create_new_set_of_hashes(),
                Err(_) => (),
            }
            part.ok()
        })
    }

    #[graphql(description = "Update a Participant")]
    fn update_participant(
        context: &Context,
        #[graphql(description = "Id of the Participant to update")] id: Uuid,
        #[graphql(description = "New values of the Participant")] values: UpdateParticipant,
    ) -> FieldResult<Option<Participant>> {
        graphql_access!(context, Role::WriteParticipant, {
            let part = Participant::update(id, values);
            match &part {
                Ok(participant) => (participant).create_new_set_of_hashes(),
                Err(_) => (),
            }
            part.ok()
        })
    }

    #[graphql(description = "Delete a Participant")]
    fn delete_participant(
        context: &Context,
        #[graphql(description = "Id of the Participant to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::DeleteRecords, Participant::delete(id).ok())
    }

    #[graphql(description = "Create a new Participant-Extension")]
    fn create_participant_extension(
        context: &Context,
        #[graphql(description = "New Participant-Extension")] values: CreateExtension,
    ) -> FieldResult<Option<Extension>> {
        graphql_access!(
            context,
            Role::WriteParticipant,
            Extension::create(values).ok()
        )
    }

    #[graphql(description = "Update a Participant-Extension")]
    fn update_participant_extension(
        context: &Context,
        #[graphql(description = "Id of the Participant-Extension to update")] id: Uuid,
        #[graphql(description = "New values of the Participant-Extension")] values: UpdateExtension,
    ) -> FieldResult<Option<Extension>> {
        graphql_access!(
            context,
            Role::WriteParticipant,
            Extension::update(id, values).ok()
        )
    }

    #[graphql(description = "Update a Participant-Signup-Extension")]
    fn update_participant_signup_extension(
        context: &Context,
        #[graphql(description = "Id of the Participant-Signup-Extension to update")] id: Uuid,
        #[graphql(description = "New values of the Participant-Signup-Extension")]
        values: UpdateSignupExtension,
    ) -> FieldResult<Option<ExtensionSignup>> {
        graphql_access!(
            context,
            Role::ManageSignup,
            Extension::update_signup(id, values).ok()
        )
    }

    #[graphql(description = "Delete a Participant-Extension")]
    fn delete_participant_extension(
        context: &Context,
        #[graphql(description = "Id of the Participant-Extension to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::DeleteRecords, {
            // delete contacts beforehand, so no error when deleting (would have been nicer to have this cascading on db level, but meh)
            let extension = match Extension::find(id) {
                Err(_) => return Ok(Some(0)),
                Ok(ext) => ext,
            };
            let contacts = match extension.relation_contacts() {
                Err(_) => return Ok(Some(0)),
                Ok(cont) => cont,
            };
            for contact in contacts {
                Contact::delete(contact.id)?;
            }

            Extension::delete(id).ok()
        })
    }

    #[graphql(description = "Create a new Contact")]
    fn create_contact(
        context: &Context,
        #[graphql(description = "New Contact")] values: CreateContact,
    ) -> FieldResult<Option<Contact>> {
        graphql_access!(
            context,
            Role::WriteParticipant,
            Contact::create(values).ok()
        )
    }

    #[graphql(description = "Update a Contact")]
    fn update_contact(
        context: &Context,
        #[graphql(description = "Id of the Contact to update")] id: Uuid,
        #[graphql(description = "New values of the Contact")] values: UpdateContact,
    ) -> FieldResult<Option<Contact>> {
        graphql_access!(
            context,
            Role::WriteParticipant,
            Contact::update(id, values).ok()
        )
    }

    #[graphql(description = "Delete a Contact")]
    fn delete_contact(
        context: &Context,
        #[graphql(description = "Id of the Contact to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        // this object is not attached to anything important, so deleting it is not worse than writing
        // therefore only `WriteParticipant` role needed
        graphql_access!(context, Role::WriteParticipant, Contact::delete(id).ok())
    }

    #[graphql(description = "Create a new Event")]
    fn create_event(
        context: &Context,
        #[graphql(description = "New Event")] values: CreateEvent,
    ) -> FieldResult<Option<Event>> {
        graphql_access!(context, Role::WriteEvents, Event::create(values).ok())
    }

    #[graphql(description = "Update an Event")]
    fn update_event(
        context: &Context,
        #[graphql(description = "Id of the Event to update")] id: Uuid,
        #[graphql(description = "New values of the Event")] values: UpdateEvent,
    ) -> FieldResult<Option<Event>> {
        graphql_access!(context, Role::WriteEvents, Event::update(id, values).ok())
    }

    #[graphql(description = "Delete an Event")]
    fn delete_event(
        context: &Context,
        #[graphql(description = "Id of the Event to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::DeleteRecords, Event::delete(id).ok())
    }

    #[graphql(description = "Create a new Zirkel")]
    fn create_zirkel(
        context: &Context,
        #[graphql(description = "New Zirkel")] values: CreateZirkel,
    ) -> FieldResult<Option<Zirkel>> {
        graphql_access!(context, Role::WriteEvents, Zirkel::create(values).ok())
    }

    #[graphql(description = "Update a Zirkel")]
    fn update_zirkel(
        context: &Context,
        #[graphql(description = "Id of the Zirkel to update")] id: Uuid,
        #[graphql(description = "New values of the Zirkel")] values: UpdateZirkel,
    ) -> FieldResult<Option<Zirkel>> {
        graphql_access!(context, Role::WriteEvents, Zirkel::update(id, values).ok())
    }

    #[graphql(description = "Delete a Zirkel")]
    fn delete_zirkel(
        context: &Context,
        #[graphql(description = "Id of the Zirkel to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::DeleteRecords, Zirkel::delete(id).ok())
    }

    #[graphql(description = "Create a new Instructor-Extension")]
    fn create_instructor_extension(
        context: &Context,
        #[graphql(description = "New Instructor-Extension")] values: CreateInstructorExtension,
    ) -> FieldResult<Option<InstructorExtension>> {
        graphql_access!(
            context,
            Role::WriteInstructor,
            InstructorExtension::create(values).ok()
        )
    }

    #[graphql(description = "Update a Instructor-Extension")]
    fn update_instructor_extension(
        context: &Context,
        #[graphql(description = "Id of the Instructor-Extension to update")] id: Uuid,
        #[graphql(description = "New values of the Instructor-Extension")]
        values: UpdateInstructorExtension,
    ) -> FieldResult<Option<InstructorExtension>> {
        graphql_access!(
            context,
            Role::WriteInstructor,
            InstructorExtension::update(id, values).ok()
        )
    }

    #[graphql(description = "Delete a Instructor-Extension")]
    fn delete_instructor_extension(
        context: &Context,
        #[graphql(description = "Id of the Instructor-Extension to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::DeleteRecords,
            InstructorExtension::delete(id).ok()
        )
    }

    #[graphql(description = "Associates the Instructor-Extension and the Zirkel")]
    fn set_instructor_extension_to_zirkel_relation(
        context: &Context,
        #[graphql(description = "Id of the Instructor-Extension to link")] instructor_extension_id: Uuid,
        #[graphql(description = "Id of the Zirkel to link")] zirkel_id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::WriteInstructor, {
            let extension = match InstructorExtension::find(instructor_extension_id) {
                Err(_) => return Ok(Some(0)),
                Ok(ext) => ext,
            };
            let zirkel = match Zirkel::find(zirkel_id) {
                Err(_) => return Ok(Some(0)),
                Ok(zir) => zir,
            };
            if extension.event_id != zirkel.event_id {
                return Ok(Some(0)); // only allow linking zirkel and instructor_extension if they both have the same event id
            }

            InstructorExtensionToZirkelMapping::create(instructor_extension_id, zirkel_id).ok()
        })
    }

    #[graphql(description = "Brakes the association between Instructor-Extension and the Zirkel")]
    fn unset_instructor_extension_to_zirkel_relation(
        context: &Context,
        #[graphql(description = "Id of the Instructor-Extension to unlink")]
        instructor_extension_id: Uuid,
        #[graphql(description = "Id of the Zirkel to unlink")] zirkel_id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::WriteInstructor,
            InstructorExtensionToZirkelMapping::delete(instructor_extension_id, zirkel_id).ok()
        )
    }

    #[graphql(description = "Create a new ZirkelPlanSlot")]
    fn create_zirkel_plan_slot(
        context: &Context,
        #[graphql(description = "New ZirkelPlanSlot")] values: CreateZirkelPlanSlot,
    ) -> FieldResult<Option<ZirkelPlanSlot>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanSlot::create(values).ok()
        )
    }

    #[graphql(description = "Update a ZirkelPlanSlot")]
    fn update_zirkel_plan_slot(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanSlot to update")] id: Uuid,
        #[graphql(description = "New values of the ZirkelPlanSlot")] values: UpdateZirkelPlanSlot,
    ) -> FieldResult<Option<ZirkelPlanSlot>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanSlot::update(id, values).ok()
        )
    }

    #[graphql(description = "Delete a ZirkelPlanSlot")]
    fn delete_zirkel_plan_slot(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanSlot to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanSlot::delete(id).ok()
        )
    }

    #[graphql(description = "Create a new ZirkelPlanEntry")]
    fn create_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "New ZirkelPlanEntry")] values: CreateZirkelPlanEntry,
    ) -> FieldResult<Option<ZirkelPlanEntry>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanEntry::create(values).ok()
        )
    }

    #[graphql(description = "Update a ZirkelPlanEntry")]
    async fn update_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry to update")] id: Uuid,
        #[graphql(description = "New values of the ZirkelPlanEntry")] values: UpdateZirkelPlanEntry,
    ) -> FieldResult<Option<ZirkelPlanEntry>> {
        graphql_access_advanced!(
            context,
            Role::ManageZirkelPlan,
            match ZirkelPlanEntry::find(id) {
                Ok(entry) => {
                    let uuids = entry.instructor_extension_uuids;
                    context
                        .has_at_least_one_of_these_instructor_extension_uuids(&uuids)
                        .await
                }
                Err(_) => {
                    false
                }
            },
            ZirkelPlanEntry::update(id, values).ok()
        )
    }

    #[graphql(description = "Link a ZirkelPlanEntry to another")]
    async fn link_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry that is linked to another")]
        source_id: Uuid,
        #[graphql(description = "Id of the ZirkelPlanEntry that the first one is linked to")]
        target_id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(context, Role::ManageZirkelPlan, {
            let target = ZirkelPlanEntry::find(target_id);

            let res = ZirkelPlanEntry::link_entry_to_entry(source_id, target_id).ok();

            match target {
                Ok(entry) => {
                    // write the values of target into the overwritten fields to ensure reliable behavior
                    entry.force_instructor_extension_uuid_array(
                        entry.instructor_extension_uuids.clone(),
                    )?;
                    ZirkelPlanEntry::update(
                        entry.id,
                        UpdateZirkelPlanEntry {
                            beamer: Some(entry.beamer),
                            notes: entry.notes,
                            topic: entry.topic,
                            zirkel_id: None,
                            zirkel_plan_slot_id: None,
                        },
                    )?;
                    // the linked field will be taken care of by the database triggers (that might have destroyed the dataset in the first place)
                }
                Err(_) => {}
            };

            res
        })
    }

    #[graphql(description = "Trigger Un-Link on ZirkelPlanEntry relations")]
    async fn unlink_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry on that un-link is triggered")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanEntry::find(id)?.unset_link_relations().ok()
        )
    }

    #[graphql(description = "Delete a ZirkelPlanEntry")]
    fn delete_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            ZirkelPlanEntry::delete(id).ok()
        )
    }

    #[graphql(description = "Add Instructor to ZirkelPlanEntry")]
    async fn add_instructor_extension_to_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry to add to")] zirkel_plan_entry_id: Uuid,
        #[graphql(description = "Id of the InstructorExtension to add")]
        instructor_extension_id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            add_instructor_extension_to_zirkel_plan_entry_thread_save(
                instructor_extension_id,
                zirkel_plan_entry_id
            )
            .await
            .ok()
        )
    }

    #[graphql(description = "Remove Instructor from ZirkelPlanEntry")]
    async fn remove_instructor_extension_from_zirkel_plan_entry(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanEntry to remove from")]
        zirkel_plan_entry_id: Uuid,
        #[graphql(description = "Id of the InstructorExtension to remove")] instructor_extension_id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            remove_instructor_extension_from_zirkel_plan_entry_thread_save(
                instructor_extension_id,
                zirkel_plan_entry_id
            )
            .await
            .ok()
        )
    }

    #[graphql(description = "Create a new FractivityRoom")]
    fn create_fractivity_room(
        context: &Context,
        #[graphql(description = "New FractivityRoom")] values: CreateFractivityRoom,
    ) -> FieldResult<Option<FractivityRoom>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            FractivityRoom::create(values).ok()
        )
    }

    #[graphql(description = "Update a FractivityRoom")]
    fn update_fractivity_room(
        context: &Context,
        #[graphql(description = "Id of the FractivityRoom to update")] id: Uuid,
        #[graphql(description = "New values of the FractivityRoom")] values: UpdateFractivityRoom,
    ) -> FieldResult<Option<FractivityRoom>> {
        graphql_access!(
            context,
            Role::ManageZirkelPlan,
            FractivityRoom::update(id, values).ok()
        )
    }

    #[graphql(description = "Delete a FractivityRoom")]
    fn delete_fractivity_room(
        context: &Context,
        #[graphql(description = "Id of the ZirkelPlanSlot to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        graphql_access!(
            context,
            Role::DeleteRecords,
            FractivityRoom::delete(id).ok()
        )
    }

    #[graphql(description = "Create a new FractivityEntry")]
    fn create_fractivity_entry(
        #[graphql(description = "New FractivityEntry")] values: CreateFractivityEntry,
    ) -> FieldResult<Option<FractivityEntry>> {
        Ok(FractivityEntry::create(values).ok())
    }

    #[graphql(description = "Update a FractivityEntry")]
    fn update_fractivity_entry(
        #[graphql(description = "Id of the FractivityEntry to update")] id: Uuid,
        #[graphql(description = "New values of the FractivityEntry")] values: UpdateFractivityEntry,
    ) -> FieldResult<Option<FractivityEntry>> {
        Ok(FractivityEntry::update(id, values).ok())
    }

    #[graphql(description = "Delete a FractivityEntry")]
    fn delete_fractivity_entry(
        #[graphql(description = "Id of the FractivityEntry to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        Ok(FractivityEntry::delete(id).ok())
    }

    #[graphql(description = "Create a new FractivityDistribution")]
    fn create_fractivity_distribution(
        #[graphql(description = "New FractivityDistribution")] values: CreateFractivityDistribution,
    ) -> FieldResult<Option<FractivityDistribution>> {
        Ok(FractivityDistribution::create(values).ok())
    }

    #[graphql(description = "Update a FractivityDistribution")]
    fn update_fractivity_distribution(
        #[graphql(description = "Id of the FractivityDistribution to update")] id: Uuid,
        #[graphql(description = "New values of the FractivityDistribution")]
        values: UpdateFractivityDistribution,
    ) -> FieldResult<Option<FractivityDistribution>> {
        Ok(FractivityDistribution::update(id, values).ok())
    }

    #[graphql(description = "Delete a FractivityDistribution")]
    fn delete_fractivity_distribution(
        #[graphql(description = "Id of the FractivityDistribution to delete")] id: Uuid,
    ) -> FieldResult<Option<i32>> {
        Ok(FractivityDistribution::delete(id).ok())
    }

    #[graphql(
        description = "Re-distribute all Fractivities for a legal distribution, deletes existing distributions for the given fractivities"
    )]
    fn distribute_fractivities(
        #[graphql(description = "Ids of the Fractivities to distribute")] fractivity_ids: Vec<Uuid>,
    ) -> FieldResult<Option<Vec<FractivityDistribution>>> {
        let fractivities = match FractivityEntry::find_all() {
            Ok(frac) => frac.into_iter().filter(|f| fractivity_ids.contains(&f.id)),
            Err(e) => return Err(e.into()),
        };
        let input = fractivities.map(|f| (f, None)).collect();
        match distribute(input) {
            Some(v) => {
                match FractivityDistribution::find_all() {
                    Ok(dist) => {
                        for d in dist {
                            if fractivity_ids.contains(&d.fractivity_entry_id) {
                                match FractivityDistribution::delete(d.id) {
                                    Ok(_) => (),
                                    Err(_) => (),
                                };
                            }
                        }
                    }
                    Err(_) => (),
                }
                Ok(Some(
                    v.into_iter()
                        .flat_map(|(f, (room_id, start_time))| {
                            FractivityDistribution::create(CreateFractivityDistribution {
                                fractivity_entry_id: f.id,
                                start_time,
                                room_id,
                            })
                            .into_iter()
                        })
                        .collect(),
                ))
            }
            None => Ok(None),
        }
    }

    #[graphql(
        description = "Distribute all Fractivities for a legal distribution that don't already have a distribution"
    )]
    fn distribute_nondistributed_fractivities(
        #[graphql(description = "Ids of the Fractivities to take into account for distribute")]
        fractivity_ids: Vec<Uuid>,
    ) -> FieldResult<Option<Vec<FractivityDistribution>>> {
        let fractivities = match FractivityEntry::find_all() {
            Ok(frac) => frac.into_iter().filter(|f| fractivity_ids.contains(&f.id)),
            Err(e) => return Err(e.into()),
        };
        let distributions = match FractivityDistribution::find_all() {
            Ok(dist) => dist
                .into_iter()
                .filter(|d| fractivity_ids.contains(&d.fractivity_entry_id))
                .collect(),
            Err(_) => Vec::new(),
        };
        let input = fractivities
            .map(|f| {
                (
                    f.clone(),
                    match distributions.iter().find(|d| d.fractivity_entry_id == f.id) {
                        Some(d) => Some((d.room_id, d.start_time)),
                        None => None,
                    },
                )
            })
            .collect();
        match distribute(input) {
            Some(v) => Ok(Some(
                v.into_iter()
                    .filter(|(f, _)| distributions.iter().all(|d| d.fractivity_entry_id != f.id))
                    .flat_map(|(f, (room_id, start_time))| {
                        FractivityDistribution::create(CreateFractivityDistribution {
                            fractivity_entry_id: f.id,
                            start_time,
                            room_id,
                        })
                        .into_iter()
                    })
                    .collect(),
            )),
            None => Ok(None),
        }
    }
}
