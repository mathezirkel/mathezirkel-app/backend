use crate::models::{
    Event, ExportTimestampStatus, Extension, ExtensionSignup, Instructor, InstructorExtension,
    Participant, ReconnectionHash, Zirkel,
};
use crate::{auth::Role, graphql::graphql_access, graphql::graphql_access_advanced};
use crate::{scripts, Context};
use chrono::NaiveDate;
use juniper::{graphql_object, FieldResult, GraphQLObject};
use last_git_commit::LastGitCommit;
use uuid::Uuid;

#[derive(GraphQLObject)]
struct ScriptServerStatus {
    reachable: bool,
    version: String,
}

/// Juniper Query Structure, methods documented with macros
pub struct Query;
#[graphql_object(context = Context)]
impl Query {
    #[graphql(
        description = "Version of the API. Commit hash can be checked for the implementation!"
    )]
    fn api_version(_context: &Context) -> FieldResult<String> {
        let long = match LastGitCommit::new().build() {
            Ok(lgc) => lgc.id().long(), // should work when compiling locally
            Err(_) => String::from(env!("CI_COMMIT_SHA")), // fallback, assumes CI_COMMIT_SHA env variable was set on compile time instead: should be in the building-pipeline
        };

        Ok(String::from(long))
    }

    #[graphql(
        description = "Status of the Script server. Version Commit hash can be checked for the implementation!"
    )]
    async fn script_server(_context: &Context) -> FieldResult<ScriptServerStatus> {
        let (status, version) = scripts::script_server_status().await;
        Ok(ScriptServerStatus {
            reachable: status,
            version,
        })
    }

    #[graphql(description = "List of all Participants")]
    fn all_participants(context: &Context) -> Option<Vec<Participant>> {
        if context.check_role(Role::ReadParticipant) {
            Participant::find_all().ok()
        } else {
            let access_to_participant_uuids = context.has_access_to_participant_uuids();
            Participant::find_all_filter_uuid(&access_to_participant_uuids).ok()
        }
    }

    #[graphql(description = "Get a single Participant by id")]
    async fn participant(
        context: &Context,
        #[graphql(description = "id of the Participant")] id: Uuid,
    ) -> FieldResult<Option<Participant>> {
        if id == Uuid::parse_str("00000000-0000-0000-0000-000000000000")? {
            return Ok(None);
        }

        let participant = Participant::find(id).ok();
        graphql_access_advanced!(
            context,
            Role::ReadParticipant,
            match &participant {
                Some(par) => {
                    let uuids = context.has_access_to_participant_uuids();
                    uuids.contains(&par.id)
                }
                None => false,
            },
            participant
        )
    }

    #[graphql(description = "Get a single Participant-Signup-Extension by id")]
    fn signup_extension(
        context: &Context,
        #[graphql(description = "id of the Participant-Signup-Extension")] id: Uuid,
    ) -> FieldResult<Option<ExtensionSignup>> {
        graphql_access!(context, Role::ManageSignup, Extension::find_signup(id).ok())
    }

    #[graphql(description = "Get a single Participant Extension by id")]
    fn participant_extension(
        context: &Context,
        #[graphql(description = "id of the Participant Extension")] id: Uuid,
    ) -> FieldResult<Option<Extension>> {
        if id == Uuid::parse_str("00000000-0000-0000-0000-000000000000")? {
            return Ok(None);
        }

        let participant_extension = Extension::find(id).ok();
        graphql_access_advanced!(
            context,
            Role::ReadParticipant,
            match &participant_extension {
                Some(ext) => {
                    let uuids = context.has_access_to_participant_extension_uuids();
                    uuids.contains(&ext.id)
                }
                None => false,
            },
            participant_extension
        )
    }

    #[graphql(description = "Get all Participant-Signup-Extensions for a specified Event")]
    fn signup_extensions(
        context: &Context,
        #[graphql(description = "id of the Event")] event_id: Uuid,
    ) -> FieldResult<Option<Vec<ExtensionSignup>>> {
        graphql_access!(
            context,
            Role::ManageSignup,
            Extension::find_all_signup(event_id).ok()
        )
    }

    #[graphql(description = "List of all reconnection-hashes")]
    fn all_hashes(context: &Context) -> FieldResult<Option<Vec<ReconnectionHash>>> {
        graphql_access!(
            context,
            Role::ReadParticipant,
            ReconnectionHash::find_all().ok()
        )
    }

    #[graphql(description = "Test inputs for what reconnection-hashes they give")]
    fn try_hashes(
        context: &Context,
        #[graphql(description = "Family Name to test hashes from")] family_name: String,
        #[graphql(description = "Given Name to test hashes from")] given_name: String,
        #[graphql(description = "Call Name to test hashes from")] call_name: String,
        #[graphql(description = "Birth Date to test hashes from")] birth_date: NaiveDate,
    ) -> FieldResult<Vec<String>> {
        graphql_access!(
            context,
            Role::ReadParticipant,
            ReconnectionHash::generate_hashes(&family_name, &given_name, &call_name, &birth_date)
        )
    }

    #[graphql(description = "List of all Zirkel")]
    fn all_zirkel() -> FieldResult<Option<Vec<Zirkel>>> {
        Ok(Zirkel::find_all().ok())
    }

    #[graphql(description = "Get a single Zirkel by id")]
    fn zirkel(
        #[graphql(description = "id of the Zirkel")] id: Uuid,
    ) -> FieldResult<Option<Zirkel>> {
        Ok(Zirkel::find(id).ok())
    }

    #[graphql(description = "List of all Events")]
    fn all_events() -> FieldResult<Option<Vec<Event>>> {
        Ok(Event::find_all().ok())
    }

    #[graphql(description = "Get a single Event by id")]
    fn event(#[graphql(description = "id of the Event")] id: Uuid) -> FieldResult<Option<Event>> {
        Ok(Event::find(id).ok())
    }

    #[graphql(description = "List of all Instructors")]
    async fn all_instructors(context: &Context) -> Option<Vec<Instructor>> {
        if context.check_role(Role::ReadInstructor) {
            Instructor::find_all().await.ok()
        } else {
            let access_to_instructor_uuids = context.has_access_to_instructor_uuids().await;
            Instructor::find_all_filter_uuid(&access_to_instructor_uuids).ok()
        }
    }

    #[graphql(description = "Get a single Instructor by id")]
    async fn instructor(
        context: &Context,
        #[graphql(description = "id of the Instructor")] id: Uuid,
    ) -> FieldResult<Option<Instructor>> {
        if id == Uuid::parse_str("00000000-0000-0000-0000-000000000000")? {
            return Ok(None);
        }

        let instructor = Instructor::find(id).await.ok();
        graphql_access_advanced!(
            context,
            Role::ReadInstructor,
            match &instructor {
                Some(ext) => {
                    let uuids = context.has_access_to_instructor_uuids().await;
                    uuids.contains(&ext.id)
                }
                None => false,
            },
            instructor
        )
    }

    #[graphql(description = "Get a single Instructor Extension by id")]
    async fn instructor_extension(
        context: &Context,
        #[graphql(description = "id of the Instructor Extension")] id: Uuid,
    ) -> FieldResult<Option<InstructorExtension>> {
        if id == Uuid::parse_str("00000000-0000-0000-0000-000000000000")? {
            return Ok(None);
        }

        let instructor_extension = InstructorExtension::find(id).ok();
        graphql_access_advanced!(
            context,
            Role::ReadInstructor,
            match &instructor_extension {
                Some(ext) => {
                    let uuids = context.has_access_to_instructor_extension_uuids().await;
                    uuids.contains(&ext.id)
                }
                None => false,
            },
            instructor_extension
        )
    }

    #[graphql(description = "Get status of the exports")]
    fn export_status(
        context: &Context,
        #[graphql(description = "Key of export must contain")] key_contains: String,
    ) -> FieldResult<Option<Vec<ExportTimestampStatus>>> {
        graphql_access!(
            context,
            Role::Export,
            ExportTimestampStatus::find_all_contains(key_contains).ok()
        )
    }
}
