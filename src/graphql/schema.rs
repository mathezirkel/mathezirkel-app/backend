use crate::graphql::{Context, Mutation, Query};
use juniper::{EmptySubscription, RootNode};

/// Juniper Schema Structure, Type Boilerplate only
pub type Schema = RootNode<'static, Query, Mutation, EmptySubscription<Context>>;

/// Gets a new Juniper Schema.
///  
/// Caution this generates a new Database, just use it once and then pass it into Juniper.
/// No singleton implemented
pub fn schema() -> Schema {
    Schema::new(Query, Mutation, EmptySubscription::<Context>::new())
}
