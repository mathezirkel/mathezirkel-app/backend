use crate::db::get_database_current_time;
use crate::models::{Instructor, SexEnum, UpsertInstructor};
use chrono::{NaiveDate, NaiveDateTime};
use lazy_static::lazy_static;
use mathezirkel_app_scriptserver_base64::base_64_encode_string_to_string;
use reqwest::{self, Response};
use serde::{self, Deserialize, Serialize};
use std::sync::{Arc, Mutex};
use std::{collections::HashMap, env};
use url::Url;

lazy_static! {
    static ref MUTEX: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
}

fn acquire_lock_to_mutex() -> bool {
    let binding = MUTEX.clone();
    let mut guard_locked = binding.lock().unwrap();

    if !(*guard_locked) {
        *guard_locked = true;
        return true; // successfully acquired lock
    }
    return false; // couldn't acquire lock
}

fn give_back_lock_to_mutex() {
    let binding = MUTEX.clone();
    let mut guard_locked = binding.lock().unwrap();
    *guard_locked = false;
}

pub async fn sync_instructors_from_keycloak_to_database() -> bool {
    while !acquire_lock_to_mutex() {
        actix_web::rt::task::yield_now().await;
    }

    let res = sync_instructors_from_keycloak_to_database_internal().await;

    give_back_lock_to_mutex();

    return res;
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
struct UsersAnswerUser {
    id: String,
    username: String,
    firstName: Option<String>, // this must be lowerCamelCase, because the API response is that way
    lastName: Option<String>,  // this must be lowerCamelCase, because the API response is that way
    email: Option<String>,
    attributes: Option<HashMap<String, Vec<String>>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(transparent)]
struct UsersAnswer {
    answers: Vec<UsersAnswerUser>,
}

///
/// Will check the database time and compare to when the latest pull from keycloak has happened.
/// If this is in the past long enough, then get the keycloak users from the REST API and upsert the local database.
///
/// Just call before attempting to read instructor data
///
async fn sync_instructors_from_keycloak_to_database_internal() -> bool {
    // ! if this function panics, the mutex will never be unlocked. So make sure this doesn't happen
    let db_time = match get_database_current_time() {
        Err(_) => return false,
        Ok(time) => time,
    };
    let oldest_instructor_time = match match Instructor::get_oldest_updated_timestamp() {
        Err(_) => return false,
        Ok(time_option) => time_option,
    } {
        Some(time) => time,
        None => NaiveDateTime::MIN,
    };

    let time_difference_that_keycloak_users_are_cached: u64 = 10; // TODO extract to env parameter

    if db_time
        .signed_duration_since(oldest_instructor_time)
        .num_seconds()
        < time_difference_that_keycloak_users_are_cached
            .try_into()
            .unwrap()
    {
        debug!("DB time is {}, while the oldest_updated_timestamp for instructors is only {}. This is so recent, that keycloak sync is skipped.", db_time, oldest_instructor_time);

        return true;
    }

    // https://www.keycloak.org/docs-api/21.0.1/rest-api/index.html#_users_resource
    let (success, res_option) = keycloak_admin_rest_api(
        "users",
        [(String::from("max"), String::from("400"))].to_vec(), // TODO currently capped to 400. Pagination would be better, but I can't be bothered right now
    )
    .await;

    if success {
        // request Succeeded, try parsing json
        let response = match res_option {
            Some(res) => res,
            None => return false,
        };
        let json_response = match response.json::<UsersAnswer>().await {
            Ok(response) => response,
            Err(error) => {
                // Could not parse
                error!("JSON of users response could not be parsed {}", error);
                return false;
            }
        };
        let mut all_worked = true;
        for instructor in json_response.answers {
            // TODO ? if instructor missing here delete/redact his information from the database
            let attributes = instructor.attributes.unwrap_or_else(|| HashMap::new());
            let get_attr = |name: &str| -> Option<String> {
                let empty: Vec<String> = Vec::new();
                let tmp = attributes.get(name).unwrap_or_else(|| &empty);

                if tmp.len() > 0 {
                    return Some(String::from(&tmp[0]));
                } else {
                    return None;
                }
            };
            let new_or_old_instr = Instructor::upsert(UpsertInstructor {
                keycloak_id: instructor.id,
                keycloak_username: Some(instructor.username),
                given_name: instructor.firstName,
                family_name: instructor.lastName,
                email: instructor.email,
                telephone: get_attr("telephone"),
                call_name: get_attr("callName"),
                birth_date: match NaiveDate::parse_from_str(
                    &get_attr("birthDate").unwrap_or_default(),
                    "%Y-%m-%d",
                ) {
                    Ok(date) => Some(date),
                    Err(_) => None,
                },
                sex: SexEnum::from_string(&get_attr("sex").unwrap_or_default()),
                gender: get_attr("gender"),
                pronoun: get_attr("pronoun"),
                street: get_attr("street"),
                street_number: get_attr("streetNumber"),
                postal_code: get_attr("postalCode"),
                city: get_attr("city"),
                country: get_attr("country"),
                abbreviation: get_attr("abbreviation"),
                iban: get_attr("iban"),
            });
            let worked = match &new_or_old_instr {
                Ok(_) => true,
                Err(err) => {
                    error!(
                        "Database error {}:{}",
                        err.error_status_code, err.error_message
                    );
                    return false;
                }
            };
            if !worked {
                all_worked = false;
            }
        }

        // the process succeeded
        debug!("Keycloak Admin Rest API used successfully for updating local user database");
        return all_worked;
    }

    return false;
}

#[derive(Debug, Serialize, Deserialize)]
struct TokenAnswer {
    access_token: String,
}

async fn keycloak_admin_rest_api(
    path: &str,
    query_pairs: Vec<(String, String)>,
) -> (bool, Option<Response>) {
    let client = reqwest::Client::new();
    // ! Get a one-time use API token
    let base64_auth_buf = base_64_encode_string_to_string(&format!(
        "{}:{}",
        env::var("KEYCLOAK_ADMIN_REST_API_CLIENT_ID")
            .expect("Keycloak data ('KEYCLOAK_ADMIN_REST_API_CLIENT_ID') not set in .env"),
        env::var("KEYCLOAK_ADMIN_REST_API_CLIENT_SECRET")
            .expect("Keycloak data ('KEYCLOAK_ADMIN_REST_API_CLIENT_SECRET') not set in .env")
    ));

    let token_response = match client
        .post(env::var("OAUTH_TOKEN_URL").expect("OAUTHVALUE ('OAUTH_TOKEN_URL') not set in .env"))
        .header("authorization", format!("Basic {}", base64_auth_buf))
        .form(&[
            (
                "username",
                &env::var("KEYCLOAK_ADMIN_REST_API_USERNAME")
                    .expect("Keycloak data ('KEYCLOAK_ADMIN_REST_API_USERNAME') not set in .env"),
            ),
            (
                "password",
                &env::var("KEYCLOAK_ADMIN_REST_API_PASSWORD")
                    .expect("Keycloak data ('KEYCLOAK_ADMIN_REST_API_PASSWORD') not set in .env"),
            ),
            ("grant_type", &String::from("password")),
        ])
        .send()
        .await
    {
        Ok(response) => response,
        Err(error) => {
            // If we are here, Keycloak didn't respond at all
            error!(
                "Keycloak Token request endpoint did not respond, maybe check endpoint: {}",
                error
            );
            return (false, None);
        }
    };

    let token_response_json = token_response
        .json::<TokenAnswer>()
        .await
        .unwrap_or_else(|_| TokenAnswer {
            access_token: String::from(""),
        });

    if token_response_json.access_token.is_empty() || token_response_json.access_token.len() < 5 {
        // 5 is random assumption so that it cant be "none" or stuff like this

        // this is logged, both if the oauth server produced an error but sent an empty/incomplete message
        error!(
            "Keycloak Token request endpoint responded ok but no valid json or no full response."
        );
        return (false, None);
    }

    let access_token = token_response_json.access_token;

    // ! Use the API token and execute REST-API call
    let mut url = Url::parse(
        &env::var("KEYCLOAK_ADMIN_REST_API_URL")
            .expect("Keycloak url ('KEYCLOAK_ADMIN_REST_API_URL') not set in .env"),
    )
    .unwrap();
    url.path_segments_mut().unwrap().push(path);
    query_pairs.iter().for_each(|(a, b)| {
        url.query_pairs_mut().append_pair(a, b);
    });

    debug!("Admin Rest API request URL: {}", &url.to_string());

    let response = match client
        .get(url.to_string())
        .header("authorization", format!("Bearer {}", access_token))
        .send()
        .await
    {
        Ok(response) => response,
        Err(error) => {
            // If we are here, Keycloak didn't respond at all
            error!(
                "Admin Rest API did not respond, maybe check endpoint: {}",
                error
            );
            return (false, None);
        }
    };

    if response.status().is_success() {
        return (true, Some(response));
    }

    // If we fell through here, the Keycloak server responded with an error
    // This may be because the request is 401-Unauthorized
    error!(
        "Admin Rest API Request did not succeed: {}",
        response.text().await.unwrap()
    );
    return (false, None);
}
