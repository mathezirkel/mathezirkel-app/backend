#[macro_use]
extern crate log;

mod auth;
mod color;
mod database;
mod db;
mod error_handler;
mod export;
mod fractivity;
mod graphql;
mod instructors;
mod models;
mod scripts;
mod webdav;

use actix_cors::Cors;
use actix_web::{
    http::header,
    middleware,
    web::{self, Data},
    App, Error, HttpResponse, HttpServer,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use actix_web_static_files::ResourceFiles;
use auth::{
    index, logout, post_logout_redirect, redeem_hash, redirect_target, refresh_token,
    request_token, user_info, verify_bearer_token,
};
use dotenv::dotenv;
use export::export_route;
use graphql::{graphiql, schema, Context, Schema};
use juniper_actix::graphql_handler;
use static_files::Resource;
use std::collections::HashMap;
use std::env;

// map juniper routes
async fn graphql_route(
    req: actix_web::HttpRequest,
    payload: actix_web::web::Payload,
    schema: web::Data<Schema>,
    auth: BearerAuth,
) -> Result<HttpResponse, Error> {
    let bearer_token = String::from(auth.token());

    let (access, claims) = verify_bearer_token(bearer_token, None).await;

    if access {
        let claims = claims.unwrap(); // because access, this succeeds

        let context = Context::new(claims.roles, claims.instructor_uuid);

        return graphql_handler(&schema, &context, req, payload).await;
    } else {
        // error, if the bearer-auth challenge was failed
        return Ok(HttpResponse::Unauthorized()
            .append_header(("Content-Type", "text/json"))
            .body(
                "\"Unauthorized: Bearer Token did not authenticate you to access this server\"",
            ));
    }
}

// include statically stored files into executable
fn generate() -> HashMap<&'static str, Resource> {
    include!("./../static_generated/generated.rs")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // init logging, env variables (requires .env file next to the executable) and database
    env_logger::init();
    dotenv().ok();
    db::init();

    // main Actix server function
    let server = HttpServer::new(move || {
        let generated = generate(); // include statically generated resources
        App::new()
            .app_data(Data::new(schema()))
            .app_data(web::PayloadConfig::new(7000000)) // 7mb payload limit
            .app_data(web::JsonConfig::default().limit(7000000)) // 7mb json limit
            .wrap(
                Cors::default()
                    .allow_any_origin() // TODO: could be tightened but can cause problems, if different OS set the effective URL of the tauri client differently
                    .allowed_methods(vec!["POST", "GET"])
                    .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
                    .allowed_header(header::CONTENT_TYPE)
                    .supports_credentials()
                    .max_age(3600),
            )
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            // all web-service-endpoints are registered below here
            .service(
                web::resource("/graphql")
                    .route(web::post().to(graphql_route))
                    .route(web::get().to(graphql_route)),
            )
            .service(web::resource("/export").route(web::post().to(export_route)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
            .service(web::resource("/request_token").route(web::get().to(request_token)))
            .service(web::resource("/redirect_target/{hash}").route(web::get().to(redirect_target)))
            .service(web::resource("/redeem_hash").route(web::get().to(redeem_hash)))
            .service(web::resource("/refresh_token").route(web::get().to(refresh_token)))
            .service(web::resource("/user_info").route(web::get().to(user_info)))
            .service(web::resource("/logout").route(web::get().to(logout)))
            .service(web::redirect(
                "/account",
                env::var("KEYCLOAK_ACCOUNT_MANAGEMENT_CONSOLE").expect(
                    "Account Console URL ('KEYCLOAK_ACCOUNT_MANAGEMENT_CONSOLE') not set in .env",
                ),
            ))
            .service(
                web::resource("/post_logout_redirect").route(web::get().to(post_logout_redirect)),
            )
            .service(web::resource("/").route(web::get().to(index)))
            .service(ResourceFiles::new("/resource", generated))
    });

    // Start the server on the specified port/host
    let server_host =
        env::var("WEBSERVER_HOST").expect("Webserver host ('WEBSERVER_HOST') not set in .env");
    let server_port =
        env::var("WEBSERVER_PORT").expect("Webserver port ('WEBSERVER_PORT') not set in .env");
    info!("Server started on http://{}:{}", server_host, server_port);
    info!(
        "See Graphiql on http://{}:{}/graphiql",
        server_host, server_port
    );
    server
        .bind(format!("{}:{}", server_host, server_port))
        .unwrap()
        .run()
        .await
}
