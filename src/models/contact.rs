use crate::auth::Role;
use crate::database::contacts;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::{graphql_access_advanced, Context};
use crate::models::Extension;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = contacts)]
pub struct CreateContact {
    pub extension_id: Uuid,
    pub name: String,
    pub telephone: String,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = contacts)]
pub struct UpdateContact {
    pub extension_id: Option<Uuid>,
    pub name: Option<String>,
    pub telephone: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Extension))]
#[diesel(table_name = contacts)]
pub struct Contact {
    pub id: Uuid,
    pub extension_id: Uuid,
    pub name: String,
    pub name_created_at: NaiveDateTime,
    pub name_updated_at: NaiveDateTime,
    pub telephone: String,
    pub telephone_created_at: NaiveDateTime,
    pub telephone_updated_at: NaiveDateTime,
    pub created_at: NaiveDateTime,
}

impl Contact {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(contacts::table.load::<Contact>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(contacts::table
            .filter(contacts::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(contact: CreateContact) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(contacts::table)
            .values(contact)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, contact: UpdateContact) -> Result<Self, CustomError> {
        Ok(diesel::update(contacts::table)
            .filter(contacts::id.eq(id))
            .set(contact)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(contacts::table.filter(contacts::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }
}

#[graphql_object(description = "Information about a specific Contact", context = Context)]
impl Contact {
    #[graphql(description = "The id of the Contact")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Participant-Extension, the Contact belongs to")]
    pub fn extension_id(&self) -> Uuid {
        self.extension_id
    }

    #[graphql(description = "The name of the Person associated with the Contact")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "The phone-number of the Person associated with the Contact")]
    pub fn telephone(&self) -> &String {
        &self.telephone
    }

    #[graphql(description = "The Participant-Extension, the Contact belongs to")]
    pub fn extension(&self, context: &Context) -> FieldResult<Option<Extension>> {
        let participant_extension = Extension::find(self.extension_id).ok();
        graphql_access_advanced!(
            context,
            Role::ReadParticipant,
            match &participant_extension {
                Some(ext) => {
                    let uuids = context.has_access_to_participant_extension_uuids();
                    uuids.contains(&ext.id)
                }
                None => false,
            },
            participant_extension
        )
    }

    #[graphql(
        description = "A timestamp that marks the creation time of this object. Used for sorting. String because we send ALL THE PRECISION"
    )]
    pub fn created_at(&self) -> String {
        self.created_at.format("%Y-%m-%d %H:%M:%S%.6f").to_string()
    }
}
