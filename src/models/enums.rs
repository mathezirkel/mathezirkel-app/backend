use crate::graphql::Context;
use diesel::{
    deserialize::{self, FromSql},
    pg::{Pg, PgValue},
    prelude::QueryableByName,
    serialize::{self, Output, ToSql, WriteTuple},
    sql_types::Record,
};
use juniper::{graphql_object, GraphQLInputObject};
use serde::{Deserialize, Serialize};

#[derive(
    Clone,
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::ClassyearEnum"]
#[graphql(
    description = "An enum-value that describes the year a pupil or teacher is in (Jahrgangsstufe)"
)]
pub enum ClassyearEnum {
    #[graphql(description = "Pupil in Grade 5")]
    #[db_rename = "class_5"]
    Class5,
    #[graphql(description = "Pupil in Grade 6")]
    #[db_rename = "class_6"]
    Class6,
    #[graphql(description = "Pupil in Grade 7")]
    #[db_rename = "class_7"]
    Class7,
    #[graphql(description = "Pupil in Grade 8")]
    #[db_rename = "class_8"]
    Class8,
    #[graphql(description = "Pupil in Grade 9")]
    #[db_rename = "class_9"]
    Class9,
    #[graphql(description = "Pupil in Grade 10")]
    #[db_rename = "class_10"]
    Class10,
    #[graphql(description = "Pupil in Grade 11")]
    #[db_rename = "class_11"]
    Class11,
    #[graphql(description = "Pupil in Grade 12")]
    #[db_rename = "class_12"]
    Class12,
    #[graphql(description = "Pupil in Grade 13")]
    #[db_rename = "class_13"]
    Class13,
    #[graphql(
        description = "Other person that is listed in the DB, because they want to receive Mails but from all Grades"
    )]
    ClassInfo,
}

#[derive(
    Clone,
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::EventtypeEnum"]
#[graphql(description = "An enum-value that describes the type of a Mathezirkel event")]
pub enum EventtypeEnum {
    // !! Was removed from the api, still exists in the database, because modifying postgres-enums is very un-pretty.
    // !! Graphql will hide events that have that type set without further notice. But as the type doesn't exist in the GQL API this should not happen if nobody fucks with the database
    // #[graphql(description = "The final event at the end of a zirkel-year")]
    // ClosingEvent,
    #[graphql(
        description = "A regular during the math-year running zirkel, that may be of the Sub-Types 'Präsenz', 'Korrespondenz-Mail' or 'Korrespondenz-Brief'"
    )]
    Zirkel,
    #[graphql(description = "A special day with specialize math-events")]
    MathDay,
    #[graphql(description = "The Oberstufen-Wintercamp event")]
    WinterCamp,
    #[graphql(description = "The original Mathecamp established 2014")]
    MathCamp,
    #[graphql(description = "Productive time for instructors to plan the camp")]
    PreparationWeekend,
    #[graphql(
        description = "People added here only want info mails and are not (forced) to also be participating at any event for real"
    )]
    Info,
}

#[derive(
    Clone,
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::NutritionEnum"]
#[graphql(description = "An enum-value that describes different nutritional orientations")]
pub enum NutritionEnum {
    #[graphql(description = "Someone whose diet allows them to eat everything")]
    Omnivore,
    #[graphql(description = "Someone whose diet allows them not to eat meat")]
    Vegetarian,
    #[graphql(description = "Someone whose diet allows them to eat no products made from animals")]
    Vegan,
    #[graphql(
        description = "The nutrition once was set. But as it is a sensitive information, in this case the data was purged from the System"
    )]
    Redacted,
}
impl NutritionEnum {
    pub fn value(&self) -> String {
        match self {
            NutritionEnum::Omnivore => String::from("fleischhaltig"),
            NutritionEnum::Vegetarian => String::from("vegetarisch"),
            NutritionEnum::Vegan => String::from("vegan"),
            NutritionEnum::Redacted => String::from("redacted"),
        }
    }
}

#[derive(
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    Clone,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::SexEnum"]
#[graphql(description = "An enum-value that describes the sexuality by-law")]
pub enum SexEnum {
    #[graphql(description = "The sexuality is considered male")]
    Male,
    #[graphql(description = "The sexuality is considered female")]
    Female,
    #[graphql(description = "The sexuality is considered diverse")]
    Diverse,
    #[graphql(
        description = "The sexuality is forced to be set and once was. But as it is a sensitive information, in this case the data was purged from the System"
    )]
    Redacted,
}
impl SexEnum {
    pub fn value(&self) -> String {
        match self {
            SexEnum::Male => String::from("männlich"),
            SexEnum::Female => String::from("weiblich"),
            SexEnum::Diverse => String::from("divers"),
            SexEnum::Redacted => String::from("redacted"),
        }
    }

    pub fn from_string(arg: &str) -> Option<SexEnum> {
        match arg {
            "male" => Some(SexEnum::Male),
            "female" => Some(SexEnum::Female),
            "diverse" => Some(SexEnum::Diverse),
            "redacted" => Some(SexEnum::Redacted),
            _ => None,
        }
    }
}

#[derive(
    Clone,
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::TraveltypeEnum"]
#[graphql(
    description = "An enum-value that describes different ways a person can arrive at the event"
)]
pub enum TraveltypeEnum {
    #[graphql(
        description = "The person arrives with private means like walking of by personal car"
    )]
    Private,
    #[graphql(description = "The person arrives by a bus that we manage")]
    Bus,
    #[graphql(description = "The person rides a bike")]
    Bicycle,
}
impl TraveltypeEnum {
    pub fn value(&self) -> String {
        match self {
            TraveltypeEnum::Private => String::from("Privat (Auto, Kamel,...)"),
            TraveltypeEnum::Bus => String::from("Mathezirkel Bus"),
            TraveltypeEnum::Bicycle => String::from("Fahrrad"),
        }
    }
}

#[derive(
    diesel_derive_enum::DbEnum,
    Debug,
    Deserialize,
    Serialize,
    juniper::GraphQLEnum,
    Clone,
    PartialEq,
)]
#[ExistingTypePath = "crate::database::sql_types::RoomEnum"]
#[graphql(description = "An enum-value that describes the purpose of a room")]
pub enum RoomEnum {
    #[graphql(description = "Needs a talk room (possibly furnished and enough space)")]
    Talk,
    #[graphql(description = "Requires the gymnasium or possibly outside")]
    Sport,
    #[graphql(description = "Needs the Craft Rooms")]
    Crafts,
    #[graphql(
        description = "Programming related fractivity (needs the room with laptop support possibly)"
    )]
    Programming,
    #[graphql(description = "Takes place outside")]
    Outside,
    #[graphql(description = "Takes place inside")]
    Inside,
    #[graphql(description = "A Place for Music")]
    Music,
    #[graphql(description = "Location describes only the take-off üoint of the fractivity")]
    Meetingpoint,
}
impl RoomEnum {
    pub fn value(&self) -> String {
        match self {
            RoomEnum::Talk => String::from("Vortrag"),
            RoomEnum::Sport => String::from("Sport"),
            RoomEnum::Crafts => String::from("Werken"),
            RoomEnum::Programming => String::from("Programmieren"),
            RoomEnum::Outside => String::from("Draußen"),
            RoomEnum::Meetingpoint => String::from("Treffpunkt"),
            RoomEnum::Inside => String::from("Drinnen"),
            RoomEnum::Music => String::from("Musik"),
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, GraphQLInputObject)]
pub struct BlockedSlotInput {
    start_block: i32,
    end_block: i32,
    name_block: String,
}

impl ToSql<crate::database::sql_types::BlockedSlot, Pg> for BlockedSlotInput {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        WriteTuple::<(
            diesel::sql_types::Integer,
            diesel::sql_types::Integer,
            diesel::sql_types::Text,
        )>::write_tuple(&(self.start_block, self.end_block, &self.name_block), out)
    }
}

#[derive(Debug, QueryableByName, Deserialize, Serialize, Clone, PartialEq)]
pub struct BlockedSlot {
    #[diesel(sql_type = diesel::sql_types::Integer)]
    start_block: i32,
    #[diesel(sql_type = diesel::sql_types::Integer)]
    end_block: i32,
    #[diesel(sql_type = diesel::sql_types::Text)]
    name_block: String,
}

#[graphql_object(description = "A Slot that is blocked for the fractivity Plan", context = Context)]
impl BlockedSlot {
    #[graphql(description = "The start of the block")]
    pub fn start_block(&self) -> i32 {
        self.start_block
    }

    #[graphql(description = "The end of the block")]
    pub fn end_block(&self) -> i32 {
        self.end_block
    }

    #[graphql(description = "The name of the block")]
    pub fn name_block(&self) -> &String {
        &self.name_block
    }
}

impl FromSql<crate::database::sql_types::BlockedSlot, Pg> for BlockedSlot {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        let (start_block, end_block, name_block) = FromSql::<
            Record<(
                diesel::sql_types::Integer,
                diesel::sql_types::Integer,
                diesel::sql_types::Text,
            )>,
            Pg,
        >::from_sql(bytes)?;
        Ok(BlockedSlot {
            start_block,
            end_block,
            name_block,
        })
    }
}
