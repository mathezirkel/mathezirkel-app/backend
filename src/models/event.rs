use super::{
    BlockedSlot, BlockedSlotInput, FractivityDistribution, FractivityEntry, FractivityRoom,
    InstructorExtension, InstructorExtensionPublic, ZirkelPlanSlot,
};
use crate::auth::Role;
use crate::database::events;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::Context;
use crate::models::{EventtypeEnum, Extension, Zirkel};
use chrono::NaiveDate;
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = events)]
pub struct CreateEvent {
    pub type_: EventtypeEnum,
    pub year: i32,
    pub name: String,
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
    pub default_fee: i32,
    pub give_instructors_full_read_access: Option<bool>,
    pub purpose_of_business_trip: Option<String>,
    pub overnight_costs: Option<f64>,
    pub additional_costs: Option<f64>,
    pub reason_of_additional_costs: Option<String>,
    pub budget_section: Option<String>,
    pub budget_item: Option<String>,
    pub cost_center_budget: Option<String>,
    pub cost_type: Option<String>,
    pub cost_center_cost_and_activity_accounting: Option<String>,
    pub cost_object: Option<String>,
    pub certificate_signature_date: Option<NaiveDate>,
    pub certificate_default_signature: Option<String>,
    pub number_of_available_beamers: Option<i32>,
    pub first_fractivity_time: Option<i32>,
    pub last_fractivity_time: Option<i32>,
    pub min_intervals_fractivity: Option<i32>,
    pub blocked_fractivity_slots: Option<Vec<BlockedSlotInput>>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = events)]
pub struct UpdateEvent {
    pub type_: Option<EventtypeEnum>,
    pub year: Option<i32>,
    pub name: Option<String>,
    pub start_date: Option<NaiveDate>,
    pub end_date: Option<NaiveDate>,
    pub default_fee: Option<i32>,
    pub give_instructors_full_read_access: Option<bool>,
    pub purpose_of_business_trip: Option<String>,
    pub overnight_costs: Option<f64>,
    pub additional_costs: Option<f64>,
    pub reason_of_additional_costs: Option<String>,
    pub budget_section: Option<String>,
    pub budget_item: Option<String>,
    pub cost_center_budget: Option<String>,
    pub cost_type: Option<String>,
    pub cost_center_cost_and_activity_accounting: Option<String>,
    pub cost_object: Option<String>,
    pub certificate_signature_date: Option<NaiveDate>,
    pub certificate_default_signature: Option<String>,
    pub number_of_available_beamers: Option<i32>,
    pub first_fractivity_time: Option<i32>,
    pub last_fractivity_time: Option<i32>,
    pub min_intervals_fractivity: Option<i32>,
    pub blocked_fractivity_slots: Option<Vec<BlockedSlotInput>>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable)]
#[diesel(table_name = events)]
pub struct Event {
    pub id: Uuid,
    pub type_: EventtypeEnum,
    pub year: i32,
    pub name: String,
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
    pub default_fee: i32,
    pub give_instructors_full_read_access: bool,
    pub statistics: JsonValue,
    pub purpose_of_business_trip: Option<String>,
    pub overnight_costs: Option<f64>,
    pub additional_costs: Option<f64>,
    pub reason_of_additional_costs: Option<String>,
    pub budget_section: Option<String>,
    pub budget_item: Option<String>,
    pub cost_center_budget: Option<String>,
    pub cost_type: Option<String>,
    pub cost_center_cost_and_activity_accounting: Option<String>,
    pub cost_object: Option<String>,
    pub certificate_signature_date: Option<NaiveDate>,
    pub certificate_default_signature: Option<String>,
    pub number_of_available_beamers: Option<i32>,
    pub first_fractivity_time: i32,
    pub last_fractivity_time: i32,
    pub min_intervals_fractivity: i32,
    pub blocked_fractivity_slots: Vec<BlockedSlot>,
}

impl Event {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(events::table.load::<Event>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(events::table
            .filter(events::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(event: CreateEvent) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(events::table)
            .values(event)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, event: UpdateEvent) -> Result<Self, CustomError> {
        Ok(diesel::update(events::table)
            .filter(events::id.eq(id))
            .set(event)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(events::table.filter(events::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_extensions(&self) -> Result<Vec<Extension>, CustomError> {
        Ok(Extension::belonging_to(&self).load::<Extension>(&mut db::connection()?)?)
    }

    pub fn relation_instructor_extensions(&self) -> Result<Vec<InstructorExtension>, CustomError> {
        Ok(InstructorExtension::belonging_to(&self)
            .load::<InstructorExtension>(&mut db::connection()?)?)
    }

    pub fn relation_zirkel(&self) -> Result<Vec<Zirkel>, CustomError> {
        Ok(Zirkel::belonging_to(&self).load::<Zirkel>(&mut db::connection()?)?)
    }

    pub fn relation_zirkel_plan_slots(&self) -> Result<Vec<ZirkelPlanSlot>, CustomError> {
        Ok(ZirkelPlanSlot::belonging_to(&self).load::<ZirkelPlanSlot>(&mut db::connection()?)?)
    }

    pub fn relation_fractivity_rooms(&self) -> Result<Vec<FractivityRoom>, CustomError> {
        Ok(FractivityRoom::belonging_to(&self).load::<FractivityRoom>(&mut db::connection()?)?)
    }

    pub fn relation_fractivity_entry(&self) -> Result<Vec<FractivityEntry>, CustomError> {
        Ok(FractivityEntry::belonging_to(&self).load::<FractivityEntry>(&mut db::connection()?)?)
    }

    pub fn relation_fractivity_distribution(
        &self,
    ) -> Result<Vec<FractivityDistribution>, CustomError> {
        let entries = match self.relation_fractivity_entry() {
            Err(err) => return Err(err),
            Ok(entries) => entries,
        };

        let distributions: Vec<FractivityDistribution> = entries
            .iter()
            .filter_map(|entry| {
                return entry.fractivity_distribution();
            })
            .collect();

        return Ok(distributions);
    }
}

#[graphql_object(description = "Information about a specific Event", context = Context)]
impl Event {
    #[graphql(description = "The id of the Event")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(name = "type", description = "The type of the Event")]
    pub fn type_(&self) -> &EventtypeEnum {
        &self.type_
    }

    #[graphql(description = "The year the Event takes place in")]
    pub fn year(&self) -> i32 {
        self.year
    }

    #[graphql(description = "The name, that describes the Event")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "The date, the Event starts")]
    pub fn start_date(&self) -> NaiveDate {
        self.start_date
    }

    #[graphql(description = "The date, the Event ends")]
    pub fn end_date(&self) -> NaiveDate {
        self.end_date
    }

    #[graphql(
        description = "The default amount of Euros that a Participant must pay to participate at the Event"
    )]
    pub fn default_fee(&self) -> i32 {
        self.default_fee
    }

    #[graphql(description = "The count of Participants with various properties at the event")]
    pub fn statistics(&self) -> String {
        self.statistics.to_string()
    }

    #[graphql(
        description = "When this is true, all instructors that have an Instructor-Extension for this event can read all the Participants data of the event. Not just their Zirkel."
    )]
    pub fn give_instructors_full_read_access(&self) -> bool {
        self.give_instructors_full_read_access
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Zweck der Reise"
    )]
    pub fn purpose_of_business_trip(&self) -> Option<&String> {
        self.purpose_of_business_trip.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Übernachtungspreis"
    )]
    pub fn overnight_costs(&self) -> Option<f64> {
        match self.overnight_costs {
            Some(num) => Some(((num * 100.0).round() / 100.0).into()),
            None => None,
        }
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Sonstige Kosten"
    )]
    pub fn additional_costs(&self) -> Option<f64> {
        match self.additional_costs {
            Some(num) => Some(((num * 100.0).round() / 100.0).into()),
            None => None,
        }
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Begründung Sonstige Kosten"
    )]
    pub fn reason_of_additional_costs(&self) -> Option<&String> {
        self.reason_of_additional_costs.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Kapitel"
    )]
    pub fn budget_section(&self) -> Option<&String> {
        self.budget_section.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Titel"
    )]
    pub fn budget_item(&self) -> Option<&String> {
        self.budget_item.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Kostenstelle HH"
    )]
    pub fn cost_center_budget(&self) -> Option<&String> {
        self.cost_center_budget.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Kostenart"
    )]
    pub fn cost_type(&self) -> Option<&String> {
        self.cost_type.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Kostenstelle KLR"
    )]
    pub fn cost_center_cost_and_activity_accounting(&self) -> Option<&String> {
        self.cost_center_cost_and_activity_accounting.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Kostenträger"
    )]
    pub fn cost_object(&self) -> Option<&String> {
        self.cost_object.as_ref()
    }

    #[graphql(description = "The date, the certificates are signed")]
    pub fn certificate_signature_date(&self) -> Option<NaiveDate> {
        self.certificate_signature_date
    }

    #[graphql(description = "The person, who signs the certificates by default")]
    pub fn certificate_default_signature(&self) -> Option<&String> {
        self.certificate_default_signature.as_ref()
    }

    #[graphql(description = "The number of available beamers")]
    pub fn number_of_available_beamers(&self) -> Option<i32> {
        self.number_of_available_beamers
    }

    #[graphql(description = "The Participant-Extensions that belong to the Event")]
    pub fn extensions(&self, context: &Context) -> Vec<Extension> {
        let extensions = self.relation_extensions().unwrap_or_default();
        if context.check_role(Role::ReadParticipant) {
            extensions
        } else {
            let access_to_extension_uuids = context.has_access_to_participant_extension_uuids();
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }

    #[graphql(description = "The Instructor-Extensions that belong to the Event")]
    pub async fn instructor_extensions(&self, context: &Context) -> Vec<InstructorExtension> {
        let extensions = self.relation_instructor_extensions().unwrap_or_default();
        if context.check_role(Role::ReadInstructor) {
            extensions
        } else {
            let access_to_extension_uuids =
                context.has_access_to_instructor_extension_uuids().await;
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }

    #[graphql(
        description = "The Instructor-Extensions that belong to the Event (Public view only)"
    )]
    pub async fn instructor_extensions_public(
        &self,
    ) -> FieldResult<Option<Vec<InstructorExtensionPublic>>> {
        let uuids = self
            .relation_instructor_extensions()
            .unwrap_or_default()
            .iter()
            .map(|x| x.id)
            .collect();

        return Ok(InstructorExtension::public_vec_by_uuids(uuids).await.ok());
    }

    #[graphql(description = "The Zirkel that belong to the Event")]
    pub fn zirkel(&self) -> FieldResult<Option<Vec<Zirkel>>> {
        Ok(self.relation_zirkel().ok())
    }

    #[graphql(description = "The ZirkelPlanSlots that belong to the Event")]
    pub fn zirkel_plan_slots(&self) -> FieldResult<Option<Vec<ZirkelPlanSlot>>> {
        Ok(self.relation_zirkel_plan_slots().ok())
    }

    #[graphql(description = "The FractivityRooms that belong to the Event")]
    pub fn fractivity_rooms(&self) -> FieldResult<Option<Vec<FractivityRoom>>> {
        Ok(self.relation_fractivity_rooms().ok())
    }

    #[graphql(description = "The FractivityEntries that belong to the Event")]
    pub fn fractivity_entries(&self) -> FieldResult<Option<Vec<FractivityEntry>>> {
        Ok(self.relation_fractivity_entry().ok())
    }

    #[graphql(description = "The FractivityDistributions that belong to the Event")]
    pub fn fractivity_distributions(&self) -> FieldResult<Option<Vec<FractivityDistribution>>> {
        Ok(self.relation_fractivity_distribution().ok())
    }

    #[graphql(
        description = "The parameter to control the first time of fractivities plan for the event"
    )]
    pub fn first_fractivity_time(&self) -> i32 {
        self.first_fractivity_time
    }
    #[graphql(
        description = "The parameter to control the last time of fractivities plan for the event"
    )]
    pub fn last_fractivity_time(&self) -> i32 {
        self.last_fractivity_time
    }
    #[graphql(
        description = "The parameter to control the time interval of fractivities plan for the event"
    )]
    pub fn min_intervals_fractivity(&self) -> i32 {
        self.min_intervals_fractivity
    }
    #[graphql(
        description = "The parameter to control blocked slots of fractivities plan for the event"
    )]
    pub fn blocked_fractivity_slots(&self) -> Vec<BlockedSlot> {
        self.blocked_fractivity_slots.clone()
    }
}
