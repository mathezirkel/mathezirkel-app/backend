use crate::database::export_timestamps;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::Context;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, GraphQLInputObject, Insertable, Clone)]
#[diesel(table_name = export_timestamps)]
pub struct UpsertExportTimestampStatus {
    pub key: String,
    pub successful_jobs: i32,
    pub total_pending_jobs: i32,
    pub failure: bool,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable)]
#[diesel(table_name = export_timestamps)]
pub struct ExportTimestampStatus {
    pub id: Uuid,
    pub key: String,
    pub export_time: NaiveDateTime,
    pub successful_jobs: i32,
    pub total_pending_jobs: i32,
    pub failure: bool,
}

impl ExportTimestampStatus {
    pub fn find_all_contains(key_contains: String) -> Result<Vec<Self>, CustomError> {
        Ok(export_timestamps::table
            .filter(export_timestamps::key.ilike(format!("%{}%", key_contains)))
            .load::<ExportTimestampStatus>(&mut db::connection()?)?)
    }

    pub fn upsert(status: &UpsertExportTimestampStatus) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(export_timestamps::table)
            .values(status)
            .on_conflict(export_timestamps::key)
            .do_update()
            .set((status, export_timestamps::export_time.eq(diesel::dsl::now)))
            .get_result(&mut db::connection()?)?)
    }
}

#[graphql_object(description = "The status of a keyed export operation", context = Context)]
impl ExportTimestampStatus {
    #[graphql(description = "The key of the export")]
    pub fn key(&self) -> &String {
        &self.key
    }

    #[graphql(
        description = "A timestamp that indicates when the export operation status was last updated"
    )]
    pub fn status_timestamp(&self) -> NaiveDateTime {
        self.export_time
    }

    #[graphql(description = "Specific status: number of successful jobs")]
    pub fn successful_jobs(&self) -> i32 {
        self.successful_jobs
    }

    #[graphql(description = "Specific status: number of total pending jobs")]
    pub fn total_pending_jobs(&self) -> i32 {
        self.total_pending_jobs
    }

    #[graphql(description = "Has the operation failed somewhere")]
    pub fn failure(&self) -> bool {
        self.failure
    }
}
