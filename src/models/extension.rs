use crate::auth::Role;
use crate::database::extensions;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::{graphql_access_advanced, Context};
use crate::models::{
    ClassyearEnum, Contact, Event, EventtypeEnum, NutritionEnum, Participant, TraveltypeEnum,
    Zirkel,
};
use chrono::{Datelike, NaiveDate, NaiveDateTime, NaiveTime};
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = extensions)]
pub struct CreateExtension {
    pub participant_id: Uuid,
    pub event_id: Uuid,
    pub certificate: Option<bool>,
    pub participates: Option<bool>,
    pub email_self: Option<String>,
    pub additional_emails: Option<Vec<String>>,
    pub notes: Option<String>,
    pub reason_of_signoff: Option<String>,
    pub time_of_signoff: Option<NaiveDateTime>,
    pub class_year: Option<ClassyearEnum>,
    pub nutrition: Option<NutritionEnum>,
    pub food_restriction: Option<String>,
    pub fee: Option<i32>,
    pub confirmed: Option<bool>,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub room_partner_wishes: Option<Vec<String>>,
    pub zirkel_partner_wishes: Option<Vec<String>>,
    pub medical_notes: Option<Vec<String>>,
    pub topic_wishes: Option<Vec<String>>,
    pub fractivity_wishes: Option<Vec<String>>,
    pub leaving_premise: Option<bool>,
    pub carpool_data_sharing: Option<bool>,
    pub remove_ticks: Option<bool>,
    pub room_number: Option<String>,
    pub instruments: Option<Vec<String>>,
    pub pool: Option<bool>,
    pub telephone: Option<String>,
    pub has_signed_up: Option<bool>, // TODO this can not update Photo. Do NOT forget when redacting
    pub zirkel_ids: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = extensions)]
pub struct UpdateExtension {
    pub participant_id: Option<Uuid>,
    pub event_id: Option<Uuid>,
    pub certificate: Option<bool>,
    pub participates: Option<bool>,
    pub email_self: Option<String>,
    pub additional_emails: Option<Vec<String>>,
    pub notes: Option<String>,
    pub reason_of_signoff: Option<String>,
    pub time_of_signoff: Option<NaiveDateTime>,
    pub class_year: Option<ClassyearEnum>,
    pub nutrition: Option<NutritionEnum>,
    pub food_restriction: Option<String>,
    pub fee: Option<i32>,
    pub confirmed: Option<bool>,
    pub registration_timestamp: Option<NaiveDateTime>,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub room_partner_wishes: Option<Vec<String>>,
    pub zirkel_partner_wishes: Option<Vec<String>>,
    pub medical_notes: Option<Vec<String>>,
    pub topic_wishes: Option<Vec<String>>,
    pub fractivity_wishes: Option<Vec<String>>,
    pub leaving_premise: Option<bool>,
    pub carpool_data_sharing: Option<bool>,
    pub remove_ticks: Option<bool>,
    pub room_number: Option<String>,
    pub instruments: Option<Vec<String>>,
    pub pool: Option<bool>,
    pub telephone: Option<String>,
    pub has_signed_up: Option<bool>, // TODO this can not update Photo. Do NOT forget when redacting
    pub zirkel_ids: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Participant))]
#[diesel(belongs_to(Event))]
#[diesel(table_name = extensions)]
pub struct Extension {
    pub id: Uuid,
    pub participant_id: Uuid,
    pub event_id: Uuid,
    pub certificate: bool,
    pub participates: bool,
    pub email_self: Option<String>,
    pub email_self_created_at: NaiveDateTime,
    pub email_self_updated_at: NaiveDateTime,
    pub additional_emails: Vec<String>,
    pub additional_emails_created_at: NaiveDateTime,
    pub additional_emails_updated_at: NaiveDateTime,
    pub notes: Option<String>,
    pub notes_created_at: NaiveDateTime,
    pub notes_updated_at: NaiveDateTime,
    pub reason_of_signoff: Option<String>,
    pub time_of_signoff: Option<NaiveDateTime>,
    pub class_year: Option<ClassyearEnum>,
    pub nutrition: Option<NutritionEnum>,
    pub nutrition_created_at: NaiveDateTime,
    pub nutrition_updated_at: NaiveDateTime,
    pub food_restriction: Option<String>,
    pub food_restriction_created_at: NaiveDateTime,
    pub food_restriction_updated_at: NaiveDateTime,
    pub fee: Option<i32>,
    pub fee_created_at: NaiveDateTime,
    pub fee_updated_at: NaiveDateTime,
    pub confirmed: bool,
    pub registration_timestamp: Option<NaiveDateTime>,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub arrival_notes_created_at: NaiveDateTime,
    pub arrival_notes_updated_at: NaiveDateTime,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub departure_notes_created_at: NaiveDateTime,
    pub departure_notes_updated_at: NaiveDateTime,
    pub room_partner_wishes: Vec<String>,
    pub room_partner_wishes_created_at: NaiveDateTime,
    pub room_partner_wishes_updated_at: NaiveDateTime,
    pub zirkel_partner_wishes: Vec<String>,
    pub zirkel_partner_wishes_created_at: NaiveDateTime,
    pub zirkel_partner_wishes_updated_at: NaiveDateTime,
    pub medical_notes: Vec<String>,
    pub medical_notes_created_at: NaiveDateTime,
    pub medical_notes_updated_at: NaiveDateTime,
    pub topic_wishes: Vec<String>,
    pub fractivity_wishes: Vec<String>,
    pub leaving_premise: bool,
    pub carpool_data_sharing: bool,
    pub remove_ticks: bool,
    pub room_number: Option<String>,
    pub instruments: Vec<String>,
    pub pool: bool,
    pub telephone: Option<String>,
    pub telephone_created_at: NaiveDateTime,
    pub telephone_updated_at: NaiveDateTime,
    pub has_signed_up: bool,
    pub signup_photo: Option<String>,
    pub signup_photo_created_at: NaiveDateTime,
    pub signup_photo_updated_at: NaiveDateTime,
    pub zirkel_ids: Vec<Uuid>,
}

#[derive(Serialize, Deserialize)]
pub struct ExtensionSignup {
    pub id: Uuid,
    pub participant_id: Uuid,
    pub event_id: Uuid,
    pub room_number: Option<String>,
    pub has_signed_up: bool,
    pub signup_photo: Option<String>,
    pub call_name: String,
    pub family_name: String,
}
#[graphql_object(description = "Information about a specific Participant-Extension (Signup only)", context = Context)]
impl ExtensionSignup {
    #[graphql(description = "The id of the Signup-Extension")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Participant, the Signup-Extension belongs to")]
    pub fn participant_id(&self) -> Uuid {
        self.participant_id
    }

    #[graphql(description = "The id of the Event, the Signup-Extension belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "The room to direct the Signup-Extension's Participant to")]
    pub fn room_number(&self) -> Option<&String> {
        self.room_number.as_ref()
    }

    #[graphql(
        description = "Whether or not a Signup-Extension's Participant has finished the signup process at the beginning of the event."
    )]
    pub fn has_signed_up(&self) -> bool {
        self.has_signed_up
    }

    #[graphql(description = "The identification photo taken at the beginning of the event")]
    pub fn signup_photo(&self) -> Option<&String> {
        self.signup_photo.as_ref()
    }

    #[graphql(description = "The surname of the Signup-Extension's Participant")]
    pub fn family_name(&self) -> &String {
        &self.family_name
    }

    #[graphql(description = "The name of the Signup-Extension's Participant")]
    pub fn call_name(&self) -> &String {
        &self.call_name
    }

    #[graphql(description = "Whether or not the Signup-Extension has a photo set")]
    pub fn has_signup_photo(&self) -> bool {
        match &(self.signup_photo) {
            Some(photo_string) => photo_string.len() > 20,
            None => false,
        }
    }
}

#[derive(Serialize, Deserialize, AsChangeset, GraphQLInputObject)]
#[diesel(table_name = extensions)]
pub struct UpdateSignupExtension {
    pub has_signed_up: Option<bool>,
    pub signup_photo: Option<String>,
}

impl Extension {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(extensions::table.load::<Extension>(&mut db::connection()?)?)
    }

    pub fn find_all_signup(event_id: Uuid) -> Result<Vec<ExtensionSignup>, CustomError> {
        match Event::find(event_id) {
            Ok(event) => {
                let extensions = event.relation_extensions();

                match extensions {
                    Ok(exts) => Ok(exts
                        .iter()
                        .filter(|extension| {
                            extension.confirmed() && extension.time_of_signoff.is_none()
                        })
                        .map(|extension| extension.convert_to_signup())
                        .collect::<Vec<ExtensionSignup>>()),
                    Err(err) => Err(err),
                }
            }
            Err(err) => Err(err),
        }
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(extensions::table
            .filter(extensions::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn find_signup(id: Uuid) -> Result<ExtensionSignup, CustomError> {
        let res = Extension::find(id);

        match res {
            Err(err) => Err(err),
            Ok(ext) => Ok(ext.convert_to_signup()),
        }
    }

    pub fn create(extension: CreateExtension) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(extensions::table)
            .values(extension)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, extension: UpdateExtension) -> Result<Self, CustomError> {
        Ok(diesel::update(extensions::table)
            .filter(extensions::id.eq(id))
            .set(extension)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update_signup(
        id: Uuid,
        extension: UpdateSignupExtension,
    ) -> Result<ExtensionSignup, CustomError> {
        let conn = &mut db::connection()?;
        let res = diesel::update(extensions::table)
            .filter(extensions::id.eq(id))
            .set(extension)
            .get_result::<Self>(conn);

        match res {
            Ok(ext) => Ok(ext.convert_to_signup()),
            Err(_) => Err(CustomError::new(
                500,
                String::from("result back-conversion failed"),
            )),
        }
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(extensions::table.filter(extensions::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_contacts(&self) -> Result<Vec<Contact>, CustomError> {
        Ok(Contact::belonging_to(&self).load::<Contact>(&mut db::connection()?)?)
    }

    fn convert_to_signup(&self) -> ExtensionSignup {
        let participant = Participant::find(self.participant_id);
        let (call_name, family_name) = match participant {
            Ok(partic) => (
                String::from(partic.call_name()),
                String::from(partic.family_name()),
            ),
            Err(_) => (String::from(""), String::from("")),
        };

        ExtensionSignup {
            id: self.id,
            participant_id: self.participant_id,
            event_id: self.event_id,
            room_number: self.room_number.clone(),
            has_signed_up: self.has_signed_up,
            signup_photo: self.signup_photo.clone(),
            call_name: call_name,
            family_name: family_name,
        }
    }

    pub fn relation_event(&self) -> Result<Event, CustomError> {
        Event::find(self.event_id)
    }

    pub fn relation_zirkel(&self) -> Vec<Zirkel> {
        match self.relation_event() {
            Err(_) => return Vec::new(),
            Ok(event) => match event.relation_zirkel() {
                Err(_) => return Vec::new(),
                Ok(zirkel_relation) => {
                    return zirkel_relation
                        .into_iter()
                        .filter(|a_zirkel| self.zirkel_ids.contains(&a_zirkel.id))
                        .collect()
                }
            },
        }
    }
}

#[graphql_object(description = "Information about a specific Participant-Extension", context = Context)]
impl Extension {
    #[graphql(description = "The id of the Participant-Extension")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Participant, the Participant-Extension belongs to")]
    pub fn participant_id(&self) -> Uuid {
        self.participant_id
    }

    #[graphql(description = "The id of the Event, the Participant-Extension belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(
        description = "The ids of the Zirkel, the Participant-Extension is part of. As this is a Orga-Field this relation must not be set from the beginning"
    )]
    pub fn zirkel_ids(&self) -> Vec<Uuid> {
        self.zirkel_ids.clone()
    }

    #[graphql(description = "Whether or not the Participant gets a certificate for the Event")]
    pub fn certificate(&self, context: &Context) -> bool {
        if !context.check_role(Role::ReadParticipant) {
            return false;
        }
        self.certificate
    }

    #[graphql(
        description = "Whether or not the Participant will participate at the Event (in person)"
    )]
    pub fn participates(&self, context: &Context) -> bool {
        if !context.check_role(Role::ReadParticipant) {
            return false;
        }
        self.participates
    }

    #[graphql(description = "The email of the Participant them-self")]
    pub fn email_self(&self, context: &Context) -> Option<String> {
        if context.check_role(Role::ReadParticipant) {
            return self.email_self.clone();
        }
        match self.relation_event() {
            Ok(event) => {
                if event.type_ == EventtypeEnum::Zirkel {
                    return self.email_self.clone();
                }
            }
            Err(_) => {}
        };
        return Some(String::from("REDACTED"));
    }

    #[graphql(
        description = "Additional emails, that may be relevant for the management (like parents emails)"
    )]
    pub fn additional_emails(&self, context: &Context) -> Vec<String> {
        if context.check_role(Role::ReadParticipant) {
            return self.additional_emails.clone();
        }
        match self.relation_event() {
            Ok(event) => {
                if event.type_ == EventtypeEnum::Zirkel {
                    return self.additional_emails.clone();
                }
            }
            Err(_) => {}
        };
        return Vec::new();
    }

    #[graphql(
        description = "Info contact telephone number attached to a Participant, as well as a specific Event"
    )]
    pub fn telephone(&self, context: &Context) -> Option<String> {
        if context.check_role(Role::ReadParticipant) {
            return self.telephone.clone();
        }
        match self.relation_event() {
            Ok(event) => {
                if event.type_ == EventtypeEnum::Zirkel {
                    return self.telephone.clone();
                }
            }
            Err(_) => {}
        };
        return Some(String::from("REDACTED"));
    }

    #[graphql(
        description = "Additional notes that are attached to a Participant, as well as a specific Event"
    )]
    pub fn notes(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.notes.clone()
    }

    #[graphql(
        description = "A reason, indicating that and why a Participant either left or never attended the Event"
    )]
    pub fn reason_of_signoff(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.reason_of_signoff.clone()
    }

    #[graphql(
        description = "A timestamp that is automatically set, when the reason for signoff is set, indicating when a Participant either left or never attended the Event"
    )]
    pub fn time_of_signoff(&self) -> Option<NaiveDateTime> {
        // this is filtered in access check, so will always be None for Non-read-Access
        self.time_of_signoff
    }

    #[graphql(
        description = "The class-level the participant was considered to be in, during the current or just-ended year"
    )]
    pub fn class_year(&self) -> Option<&ClassyearEnum> {
        self.class_year.as_ref()
    }

    #[graphql(description = "Main Choice of diet for the Participant")]
    pub fn nutrition(&self, context: &Context) -> Option<NutritionEnum> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(NutritionEnum::Redacted);
        }
        self.nutrition.clone()
    }

    #[graphql(
        description = "Other Food-related restrictions that further specify the Participants diet"
    )]
    pub fn food_restriction(&self, context: &Context) -> Option<String> {
        if context.check_role(Role::ReadParticipant) {
            return self.food_restriction.clone();
        }
        match self.relation_event() {
            Ok(event) => {
                if event.type_ == EventtypeEnum::WinterCamp
                    || event.type_ == EventtypeEnum::MathCamp
                {
                    return self.food_restriction.clone();
                }
            }
            Err(_) => {}
        };
        return Some(String::from("REDACTED"));
    }

    #[graphql(
        description = "The amount of Euros that a Participant must pay to participate at the Event."
    )]
    pub fn fee(&self, context: &Context) -> Option<i32> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(-10);
        }
        self.fee
    }

    #[graphql(
        description = "Whether or not the participant is guaranteed a place (confirmation-mail sent)"
    )]
    pub fn confirmed(&self) -> bool {
        // this is filtered in access check, so will always be true for Non-read-Access
        self.confirmed
    }

    #[graphql(
        description = "A timestamp that describes when a Participant is initially signed up, indicating their place on the waiting-list"
    )]
    pub fn registration_timestamp(&self, context: &Context) -> Option<NaiveDateTime> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(NaiveDateTime::new(
                NaiveDate::from_ymd_opt(1, 1, 1).unwrap(),
                NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
            ));
        }
        self.registration_timestamp.clone()
    }

    #[graphql(description = "The way the Participant will arrive at the Event")]
    pub fn arrival(&self, context: &Context) -> Option<TraveltypeEnum> {
        if !context.check_role(Role::ReadParticipant) {
            return None;
        }
        self.arrival.clone()
    }

    #[graphql(description = "Extra notes concerning the arrival of the Participant at the Event")]
    pub fn arrival_notes(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.arrival_notes.clone()
    }

    #[graphql(description = "The way the Participant will depart from the Event")]
    pub fn departure(&self, context: &Context) -> Option<TraveltypeEnum> {
        if !context.check_role(Role::ReadParticipant) {
            return None;
        }
        self.departure.clone()
    }

    #[graphql(
        description = "Extra notes concerning the departure of the Participant from the Event"
    )]
    pub fn departure_notes(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.departure_notes.clone()
    }

    #[graphql(
        description = "A list of names of Participants, that the Participant wishes to be in a room with"
    )]
    pub fn room_partner_wishes(&self, context: &Context) -> Vec<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Vec::new();
        }
        self.room_partner_wishes.clone()
    }

    #[graphql(
        description = "A list of names of Participants, that the Participant wishes to be in a zirkel with"
    )]
    pub fn zirkel_partner_wishes(&self, context: &Context) -> Vec<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Vec::new();
        }
        self.zirkel_partner_wishes.clone()
    }

    #[graphql(description = "A list of medical notes, that concern the Participant")]
    pub fn medical_notes(&self, context: &Context) -> Vec<String> {
        if context.check_role(Role::ReadParticipant) {
            return self.medical_notes.clone();
        }
        match self.relation_event() {
            Ok(event) => {
                if event.type_ == EventtypeEnum::WinterCamp
                    || event.type_ == EventtypeEnum::MathCamp
                {
                    return self.medical_notes.clone();
                }
            }
            Err(_) => {}
        };
        return Vec::new();
    }

    #[graphql(description = "A list of topics, the Participant wishes for")]
    pub fn topic_wishes(&self) -> &Vec<String> {
        self.topic_wishes.as_ref()
    }

    #[graphql(description = "A list of fractivities, the Participant wishes for")]
    pub fn fractivity_wishes(&self) -> &Vec<String> {
        self.fractivity_wishes.as_ref()
    }

    #[graphql(
        description = "Whether or not the Participant is allowed to leave the premise in a large enough group of old enough peers"
    )]
    pub fn leaving_premise(&self) -> bool {
        self.leaving_premise
    }

    #[graphql(
        description = "Whether or not it is allowed to share personal data of the Participant for the usage of building Carpools"
    )]
    pub fn carpool_data_sharing(&self, context: &Context) -> bool {
        if !context.check_role(Role::ReadParticipant) {
            return false;
        }
        self.carpool_data_sharing
    }

    #[graphql(
        description = "Whether or not it is allowed to remove ticks from the Participant or a doctor needs to be visited"
    )]
    pub fn remove_ticks(&self, context: &Context) -> bool {
        if !context.check_role(Role::ReadParticipant) {
            return false;
        }
        self.remove_ticks
    }

    #[graphql(
        description = "The number of the room, the Participant is living in. As this is a Orga-Field this relation must not be set from the beginning"
    )]
    pub fn room_number(&self) -> Option<&String> {
        self.room_number.as_ref()
    }

    #[graphql(description = "All the instruments a Participant is able to play")]
    pub fn instruments(&self) -> &Vec<String> {
        self.instruments.as_ref()
    }

    #[graphql(description = "Whether or not a Participant is allowed to go into the pool")]
    pub fn pool(&self) -> bool {
        self.pool
    }

    #[graphql(
        description = "Whether or not the Participant has their birthday during the duration of the event. Computed, will be false if birthday gets redacted"
    )]
    pub fn has_birthday(&self, context: &Context) -> bool {
        if !context.check_role(Role::ReadParticipant) {
            return false;
        }

        let event = Event::find(self.event_id);
        let participant = Participant::find(self.participant_id);

        let (start, end) = match event {
            Ok(event) => (event.start_date, event.end_date),
            Err(_) => (NaiveDate::MIN, NaiveDate::MIN),
        };
        let birthday = match participant {
            Ok(participant) => participant.birth_date,
            Err(_) => NaiveDate::MAX,
        };

        let mut day = birthday.day();
        let month = birthday.month();
        if month == 2 && day == 29 {
            day = 28; // birthday on leap year, always check for the 28th
        }
        let constr_birthday = NaiveDate::from_ymd_opt(start.year(), month, day).unwrap();

        constr_birthday >= start && constr_birthday <= end
    }

    #[graphql(
        description = "Whether or not a Participant has finished the signup process at the beginning of the event."
    )]
    pub fn has_signed_up(&self) -> bool {
        self.has_signed_up
    }

    #[graphql(description = "The identification photo taken at the beginning of the event")]
    pub fn signup_photo(&self) -> Option<&String> {
        self.signup_photo.as_ref()
    }

    #[graphql(description = "The Contacts that belong to the Participant-Extension")]
    pub fn contacts(&self, context: &Context) -> FieldResult<Option<Vec<Contact>>> {
        let mut acc = context.check_role(Role::ReadParticipant);
        if !acc {
            acc = match self.relation_event() {
                Ok(event) => {
                    if event.type_ == EventtypeEnum::Zirkel {
                        let uuids = context.has_access_to_participant_extension_uuids();
                        uuids.contains(&self.id)
                    } else {
                        false
                    }
                }
                Err(_) => false,
            }
        }

        if acc {
            Ok(self.relation_contacts().ok())
        } else {
            Ok(Some(Vec::new()))
        }
    }

    #[graphql(description = "The Participant, the Participant-Extension belongs to")]
    pub fn participant(&self, context: &Context) -> FieldResult<Option<Participant>> {
        let participant = Participant::find(self.participant_id).ok();
        graphql_access_advanced!(
            context,
            Role::ReadParticipant,
            match &participant {
                Some(par) => {
                    let uuids = context.has_access_to_participant_uuids();
                    uuids.contains(&par.id)
                }
                None => false,
            },
            participant
        )
    }

    #[graphql(description = "The Event, the Participant-Extension belongs to")]
    pub fn event(&self) -> Option<Event> {
        self.relation_event().ok()
    }

    #[graphql(description = "The Zirkel, the Participant-Extension belongs to")]
    pub fn zirkel(&self) -> Vec<Zirkel> {
        self.relation_zirkel()
    }
}
