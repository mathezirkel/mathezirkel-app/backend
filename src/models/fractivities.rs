use super::{Event, InstructorExtension, InstructorExtensionPublic, RoomEnum};
use crate::database::{fractivity_distribution, fractivity_entries, fractivity_rooms};
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::Context;
use chrono::NaiveDate;
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_rooms)]
pub struct CreateFractivityRoom {
    pub event_id: Uuid,
    pub name: String,
    pub room_types: Vec<RoomEnum>,
    pub priority: Option<i32>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_rooms)]
pub struct UpdateFractivityRoom {
    pub event_id: Option<Uuid>,
    pub name: Option<String>,
    pub room_types: Option<Vec<RoomEnum>>,
    pub priority: Option<i32>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Event))]
#[diesel(table_name = fractivity_rooms)]
pub struct FractivityRoom {
    pub id: Uuid,
    pub event_id: Uuid,
    pub name: String,
    pub room_types: Vec<RoomEnum>,
    pub priority: i32,
}

impl FractivityRoom {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(fractivity_rooms::table.load::<FractivityRoom>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(fractivity_rooms::table
            .filter(fractivity_rooms::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(fractivity_room: CreateFractivityRoom) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(fractivity_rooms::table)
            .values(fractivity_room)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, fractivity_room: UpdateFractivityRoom) -> Result<Self, CustomError> {
        Ok(diesel::update(fractivity_rooms::table)
            .filter(fractivity_rooms::id.eq(id))
            .set(fractivity_room)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(fractivity_rooms::table.filter(fractivity_rooms::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_event(&self) -> Result<Event, CustomError> {
        Event::find(self.event_id)
    }
}

#[graphql_object(description = "A room available for fractivity distribution", context = Context)]
impl FractivityRoom {
    #[graphql(description = "The id of the FractivityRoom")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Event, the FractivityRoom belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "The name of the Room")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "The Types of the Room")]
    pub fn room_types(&self) -> Vec<RoomEnum> {
        self.room_types.clone()
    }

    #[graphql(description = "The Event, the Room belongs to")]
    pub fn event(&self) -> Option<Event> {
        self.relation_event().ok()
    }

    #[graphql(description = "The priority of the Room")]
    pub fn priority(&self) -> i32 {
        self.priority
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_entries)]
pub struct CreateFractivityEntry {
    pub event_id: Uuid,
    pub instructor_extension_uuids: Vec<Uuid>,
    pub topic: String,
    pub duration: i32,
    pub allowed_rooms: Vec<Uuid>,
    pub allowed_starts: Vec<i32>,
    pub preparation_time: Option<i32>,
    pub follow_up_time: Option<i32>,
    pub start_day: NaiveDate,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_entries)]
pub struct UpdateFractivityEntry {
    pub event_id: Option<Uuid>,
    pub instructor_extension_uuids: Option<Vec<Uuid>>,
    pub topic: Option<String>,
    pub duration: Option<i32>,
    pub allowed_rooms: Option<Vec<Uuid>>,
    pub allowed_starts: Option<Vec<i32>>,
    pub preparation_time: Option<i32>,
    pub follow_up_time: Option<i32>,
    pub start_day: Option<NaiveDate>,
}

#[derive(
    Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations, Clone, Debug,
)]
#[diesel(belongs_to(Event))]
#[diesel(table_name = fractivity_entries)]
pub struct FractivityEntry {
    pub id: Uuid,
    pub event_id: Uuid,
    pub instructor_extension_uuids: Vec<Uuid>,
    pub topic: String,
    pub duration: i32,
    pub allowed_rooms: Vec<Uuid>,
    pub allowed_starts: Vec<i32>,
    pub preparation_time: i32,
    pub follow_up_time: i32,
    pub start_day: NaiveDate,
}

impl FractivityEntry {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(fractivity_entries::table.load::<FractivityEntry>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(fractivity_entries::table
            .filter(fractivity_entries::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(fractivity_entry: CreateFractivityEntry) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(fractivity_entries::table)
            .values(fractivity_entry)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, fractivity_entry: UpdateFractivityEntry) -> Result<Self, CustomError> {
        Ok(diesel::update(fractivity_entries::table)
            .filter(fractivity_entries::id.eq(id))
            .set(fractivity_entry)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(fractivity_entries::table.filter(fractivity_entries::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_event(&self) -> Result<Event, CustomError> {
        Event::find(self.event_id)
    }

    pub fn relation_distribution(&self) -> Result<FractivityDistribution, CustomError> {
        Ok(fractivity_distribution::table
            .filter(fractivity_distribution::fractivity_entry_id.eq(self.id))
            .first::<FractivityDistribution>(&mut db::connection()?)?)
    }
}

#[graphql_object(description = "A wish that defines a fractivity distribution", context = Context)]
impl FractivityEntry {
    #[graphql(description = "The id of the FractivityEntry")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Event, the FractivityEntry belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "The topic of the Fractivity")]
    pub fn topic(&self) -> &String {
        &self.topic
    }

    #[graphql(description = "The length of the fractivity in min")]
    pub fn duration(&self) -> i32 {
        self.duration
    }

    #[graphql(description = "The length of the fractivity in min")]
    pub fn allowed_rooms(&self) -> Vec<FractivityRoom> {
        match FractivityRoom::find_all() {
            Err(_) => return Vec::new(),
            Ok(rooms) => {
                return rooms
                    .into_iter()
                    .filter(|room| self.allowed_rooms.contains(&room.id))
                    .collect()
            }
        };
    }

    #[graphql(description = "The possible starting time offsets")]
    pub fn allowed_starts(&self) -> Vec<i32> {
        self.allowed_starts.clone()
    }

    #[graphql(description = "The preparation time before the fractivity")]
    pub fn preparation_time(&self) -> i32 {
        self.preparation_time
    }

    #[graphql(description = "The room-blocking time after the fractivity")]
    pub fn follow_up_time(&self) -> i32 {
        self.follow_up_time
    }

    #[graphql(description = "The day the fractivity starts")]
    pub fn start_day(&self) -> &NaiveDate {
        &self.start_day
    }

    #[graphql(description = "The InstructorExtensions, that belong to the FractivityEntry")]
    pub async fn instructor_extensions_public(
        &self,
    ) -> FieldResult<Option<Vec<InstructorExtensionPublic>>> {
        return Ok(InstructorExtension::public_vec_by_uuids(
            self.instructor_extension_uuids.clone(),
        )
        .await
        .ok());
    }

    #[graphql(description = "The Event, the FractivityEntry belongs to")]
    pub fn event(&self) -> Option<Event> {
        self.relation_event().ok()
    }

    #[graphql(description = "The FractivityDistribution, that belongs to the FractivityEntry")]
    pub fn fractivity_distribution(&self) -> Option<FractivityDistribution> {
        match self.relation_distribution() {
            Err(_) => return None,
            Ok(dist) => Some(dist),
        }
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_distribution)]
pub struct CreateFractivityDistribution {
    pub fractivity_entry_id: Uuid,
    pub start_time: i32,
    pub room_id: Uuid,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = fractivity_distribution)]
pub struct UpdateFractivityDistribution {
    pub fractivity_entry_id: Option<Uuid>,
    pub start_time: Option<i32>,
    pub room_id: Option<Uuid>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations, Clone)]
#[diesel(belongs_to(FractivityEntry))]
#[diesel(table_name = fractivity_distribution)]
pub struct FractivityDistribution {
    pub id: Uuid,
    pub fractivity_entry_id: Uuid,
    pub start_time: i32,
    pub room_id: Uuid,
}

impl FractivityDistribution {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(fractivity_distribution::table.load::<FractivityDistribution>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(fractivity_distribution::table
            .filter(fractivity_distribution::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(
        fractivity_distribution: CreateFractivityDistribution,
    ) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(fractivity_distribution::table)
            .values(fractivity_distribution)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(
        id: Uuid,
        fractivity_distribution: UpdateFractivityDistribution,
    ) -> Result<Self, CustomError> {
        Ok(diesel::update(fractivity_distribution::table)
            .filter(fractivity_distribution::id.eq(id))
            .set(fractivity_distribution)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(
                fractivity_distribution::table.filter(fractivity_distribution::id.eq(id)),
            )
            .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_entry(&self) -> Result<FractivityEntry, CustomError> {
        FractivityEntry::find(self.fractivity_entry_id)
    }

    pub fn relation_room(&self) -> Result<FractivityRoom, CustomError> {
        FractivityRoom::find(self.room_id)
    }
}

#[graphql_object(description = "A room available for fractivity distribution", context = Context)]
impl FractivityDistribution {
    #[graphql(description = "The id of the FractivityDistribution")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the FractivityEntry, the FractivityDistribution belongs to")]
    pub fn fractivity_entry_id(&self) -> Uuid {
        self.fractivity_entry_id
    }

    #[graphql(description = "The time offset, the fractivity starts")]
    pub fn start_time(&self) -> i32 {
        self.start_time
    }

    #[graphql(description = "The Uuid of the FractivityRoom the fractivity takes place in")]
    pub fn room_id(&self) -> &Uuid {
        &self.room_id
    }

    #[graphql(description = "The FractivityRoom, the FractivityDistribution takes place in")]
    pub fn room(&self) -> FractivityRoom {
        self.relation_room().ok().unwrap() // must be there, otherwise something else is faulty
    }

    #[graphql(description = "The FractivityEntry, the FractivityDistribution belongs to")]
    pub fn fractivity_entry(&self) -> FractivityEntry {
        self.relation_entry().ok().unwrap() // must be there, otherwise something else is faulty
    }

    #[graphql(description = "The color of the FractivityDistribution")]
    pub fn color(&self, context: &Context) -> String {
        return context.get_color_for_fractivity_distribution(self);
    }
}
