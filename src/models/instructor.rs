use super::InstructorExtension;
use crate::auth::Role;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::Context;
use crate::models::SexEnum;
use crate::{database::instructors, instructors::sync_instructors_from_keycloak_to_database};
use chrono::{NaiveDate, NaiveDateTime};
use diesel::prelude::*;
use juniper::graphql_object;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable)]
#[diesel(table_name = instructors)]
#[diesel(treat_none_as_null = true)]
pub struct UpsertInstructor {
    pub keycloak_id: String,
    pub keycloak_username: Option<String>,
    pub given_name: Option<String>,
    pub family_name: Option<String>,
    pub call_name: Option<String>,
    pub birth_date: Option<NaiveDate>,
    pub sex: Option<SexEnum>,
    pub gender: Option<String>,
    pub pronoun: Option<String>,
    pub street: Option<String>,
    pub street_number: Option<String>,
    pub postal_code: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,
    pub email: Option<String>,
    pub telephone: Option<String>,
    pub abbreviation: Option<String>,
    pub iban: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable)]
pub struct Instructor {
    pub id: Uuid,
    pub last_update: NaiveDateTime,
    pub keycloak_id: String,
    pub keycloak_username: Option<String>,
    pub given_name: Option<String>,
    pub family_name: Option<String>,
    pub call_name: Option<String>,
    pub birth_date: Option<NaiveDate>,
    pub sex: Option<SexEnum>,
    pub gender: Option<String>,
    pub pronoun: Option<String>,
    pub street: Option<String>,
    pub street_number: Option<String>,
    pub postal_code: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,
    pub email: Option<String>,
    pub telephone: Option<String>,
    pub abbreviation: Option<String>,
    pub iban: Option<String>,
}

impl Instructor {
    pub async fn find_all() -> Result<Vec<Self>, CustomError> {
        sync_instructors_from_keycloak_to_database().await;

        Ok(instructors::table.load::<Instructor>(&mut db::connection()?)?)
    }

    pub fn find_all_filter_uuid(filter_uuids: &Vec<Uuid>) -> Result<Vec<Self>, CustomError> {
        Ok(instructors::table
            .filter(instructors::dsl::id.eq_any(filter_uuids))
            .load::<Instructor>(&mut db::connection()?)?)
    }

    pub async fn find(id: Uuid) -> Result<Self, CustomError> {
        sync_instructors_from_keycloak_to_database().await;

        Ok(instructors::table
            .filter(instructors::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn upsert(instructor: UpsertInstructor) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(instructors::table)
            .values(&instructor)
            .on_conflict(instructors::keycloak_id)
            .do_update()
            .set((&instructor, instructors::last_update.eq(diesel::dsl::now)))
            .get_result(&mut db::connection()?)?)
    }

    pub fn get_oldest_updated_timestamp() -> Result<Option<NaiveDateTime>, CustomError> {
        let oldest_instructor: Option<Self> = match instructors::table
            .order(instructors::last_update.asc())
            .first(&mut db::connection()?)
        {
            Ok(instr) => Some(instr),
            Err(_) => None,
        };

        Ok(match oldest_instructor {
            Some(instr) => Some(instr.last_update),
            None => None,
        })
    }

    pub fn relation_extensions(&self) -> Result<Vec<InstructorExtension>, CustomError> {
        Ok(InstructorExtension::belonging_to(&self)
            .load::<InstructorExtension>(&mut db::connection()?)?)
    }
}

#[graphql_object(description = "Information about a Mathezirkel-instructor (Synced from Keycloak)", context = Context)]
impl Instructor {
    #[graphql(description = "The id of the Instructor")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The surname of the Instructor")]
    pub fn family_name(&self) -> Option<&String> {
        self.family_name.as_ref()
    }

    #[graphql(description = "The by-law name of the Instructor")]
    pub fn given_name(&self) -> Option<&String> {
        self.given_name.as_ref()
    }

    #[graphql(
        description = "The name the Instructor wants to be called, if set. If unset, the first substring of the GivenName of the Instructor"
    )]
    pub fn call_name(&self) -> String {
        let empty = String::from("");
        let call_name = self.call_name.as_ref().unwrap_or(&empty);

        if call_name == "" {
            String::from(
                &self
                    .given_name
                    .as_ref()
                    .unwrap_or(&empty)
                    .split_whitespace()
                    .next()
                    .map(|s| s.to_string())
                    .unwrap_or_else(|| String::new()),
            )
        } else {
            String::from(call_name)
        }
    }

    #[graphql(
        description = "A timestamp that indicates when the user-data has been last synced from Keycloak (in database time)"
    )]
    pub fn last_update(&self) -> NaiveDateTime {
        self.last_update
    }

    #[graphql(description = "The per-law sex of the Instructor")]
    pub fn sex(&self) -> Option<&SexEnum> {
        self.sex.as_ref()
    }

    #[graphql(description = "The keycloak_id of the Instructor")]
    pub fn keycloak_id(&self) -> &String {
        &self.keycloak_id
    }

    #[graphql(description = "The keycloak_username of the Instructor")]
    pub fn keycloak_username(&self) -> Option<&String> {
        self.keycloak_username.as_ref()
    }

    #[graphql(description = "The date the Instructor was born")]
    pub fn birth_date(&self) -> Option<&NaiveDate> {
        self.birth_date.as_ref()
    }

    #[graphql(
        description = "If set, the preferred gender of the Instructor. If unset will return the sex of the Instructor"
    )]
    pub fn gender(&self) -> String {
        let empty = String::from("");
        let sex = self.sex.as_ref().unwrap_or(&SexEnum::Redacted);
        let gender = self.gender.as_ref().unwrap_or(&empty);

        if gender == "" {
            String::from(sex.value())
        } else {
            String::from(gender)
        }
    }

    #[graphql(
        description = "If set, the preferred pronoun of the Instructor. If unset will return the pronouns computed, based on the gender of the Instructor"
    )]
    pub fn pronoun(&self) -> String {
        let empty = String::from("");
        let pronoun = self.pronoun.as_ref().unwrap_or(&empty);
        let gender = self.gender();

        if pronoun == "" {
            if gender == SexEnum::Male.value() {
                String::from("Er")
            } else if gender == SexEnum::Female.value() {
                String::from("Sie")
            } else {
                String::from("Du")
            }
        } else {
            String::from(pronoun)
        }
    }

    #[graphql(description = "The street of the Instructor")]
    pub fn street(&self) -> Option<&String> {
        self.street.as_ref()
    }

    #[graphql(description = "The street_number of the Instructor")]
    pub fn street_number(&self) -> Option<&String> {
        self.street_number.as_ref()
    }

    #[graphql(description = "The postal_code of the Instructor")]
    pub fn postal_code(&self) -> Option<&String> {
        self.postal_code.as_ref()
    }

    #[graphql(description = "The city of the Instructor")]
    pub fn city(&self) -> Option<&String> {
        self.city.as_ref()
    }

    #[graphql(description = "The country of the Instructor")]
    pub fn country(&self) -> Option<&String> {
        self.country.as_ref()
    }

    #[graphql(description = "The email of the Instructor")]
    pub fn email(&self) -> Option<&String> {
        self.email.as_ref()
    }

    #[graphql(description = "The telephone number of the Instructor")]
    pub fn telephone(&self) -> Option<&String> {
        self.telephone.as_ref()
    }

    #[graphql(description = "The abbreviation of the Instructors name")]
    pub fn abbreviation(&self) -> Option<&String> {
        self.abbreviation.as_ref()
    }

    #[graphql(description = "The iban, linked to payments of/from the Instructor")]
    pub fn iban(&self) -> Option<&String> {
        self.iban.as_ref()
    }

    #[graphql(description = "The Instructor-Extensions that belong to the Instructor")]
    pub async fn extensions(&self, context: &Context) -> Vec<InstructorExtension> {
        let extensions = self.relation_extensions().unwrap_or_default();
        if context.check_role(Role::ReadInstructor) {
            extensions
        } else {
            let access_to_extension_uuids =
                context.has_access_to_instructor_extension_uuids().await;
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }
}
