use crate::auth::Role;
use crate::database::{instructor_extensions, zirkel};
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::{graphql_access_advanced, Context};
use crate::models::{
    Event, Instructor, InstructorExtensionToZirkelMapping, NutritionEnum, TraveltypeEnum, Zirkel,
};
use chrono::{Datelike, Duration, NaiveDate, NaiveDateTime, Timelike};
use diesel::prelude::*;
use diesel::Insertable;
use futures::future::join_all;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = instructor_extensions)]
pub struct CreateInstructorExtension {
    pub instructor_id: Uuid,
    pub event_id: Uuid,
    pub nutrition: Option<NutritionEnum>,
    pub food_restriction: Option<String>,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub has_contract: Option<bool>,
    pub contract_with: Option<String>,
    pub room_number: Option<String>,
    pub faculty: Option<String>,
    pub chair: Option<String>,
    pub personnel_number: Option<String>,
    pub job_title: Option<String>,
    pub email_work: Option<String>,
    pub telephone_work: Option<String>,
    pub start_of_travel: Option<NaiveDateTime>,
    pub arrival_at_business_location: Option<NaiveDateTime>,
    pub start_of_business_activities: Option<NaiveDateTime>,
    pub end_of_business_activities: Option<NaiveDateTime>,
    pub start_of_return_journey: Option<NaiveDateTime>,
    pub end_of_travel: Option<NaiveDateTime>,
    pub accommodation_costs: Option<f64>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = instructor_extensions)]
pub struct UpdateInstructorExtension {
    pub instructor_id: Option<Uuid>,
    pub event_id: Option<Uuid>,
    pub nutrition: Option<NutritionEnum>,
    pub food_restriction: Option<String>,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub has_contract: Option<bool>,
    pub contract_with: Option<String>,
    pub room_number: Option<String>,
    pub faculty: Option<String>,
    pub chair: Option<String>,
    pub personnel_number: Option<String>,
    pub job_title: Option<String>,
    pub email_work: Option<String>,
    pub telephone_work: Option<String>,
    pub start_of_travel: Option<NaiveDateTime>,
    pub arrival_at_business_location: Option<NaiveDateTime>,
    pub start_of_business_activities: Option<NaiveDateTime>,
    pub end_of_business_activities: Option<NaiveDateTime>,
    pub start_of_return_journey: Option<NaiveDateTime>,
    pub end_of_travel: Option<NaiveDateTime>,
    pub accommodation_costs: Option<f64>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Instructor))]
#[diesel(belongs_to(Event))]
#[diesel(table_name = instructor_extensions)]
pub struct InstructorExtension {
    pub id: Uuid,
    pub instructor_id: Uuid,
    pub event_id: Uuid,
    pub nutrition: Option<NutritionEnum>,
    pub nutrition_created_at: NaiveDateTime,
    pub nutrition_updated_at: NaiveDateTime,
    pub food_restriction: Option<String>,
    pub food_restriction_created_at: NaiveDateTime,
    pub food_restriction_updated_at: NaiveDateTime,
    pub arrival: Option<TraveltypeEnum>,
    pub arrival_notes: Option<String>,
    pub arrival_notes_created_at: NaiveDateTime,
    pub arrival_notes_updated_at: NaiveDateTime,
    pub departure: Option<TraveltypeEnum>,
    pub departure_notes: Option<String>,
    pub departure_notes_created_at: NaiveDateTime,
    pub departure_notes_updated_at: NaiveDateTime,
    pub has_contract: bool,
    pub contract_with: Option<String>,
    pub contract_with_created_at: NaiveDateTime,
    pub contract_with_updated_at: NaiveDateTime,
    pub room_number: Option<String>,
    pub faculty: Option<String>,
    pub chair: Option<String>,
    pub personnel_number: Option<String>,
    pub personnel_number_created_at: NaiveDateTime,
    pub personnel_number_updated_at: NaiveDateTime,
    pub job_title: Option<String>,
    pub email_work: Option<String>,
    pub telephone_work: Option<String>,
    pub start_of_travel: Option<NaiveDateTime>,
    pub arrival_at_business_location: Option<NaiveDateTime>,
    pub start_of_business_activities: Option<NaiveDateTime>,
    pub end_of_business_activities: Option<NaiveDateTime>,
    pub start_of_return_journey: Option<NaiveDateTime>,
    pub end_of_travel: Option<NaiveDateTime>,
    pub accommodation_costs: Option<f64>,
}

impl InstructorExtension {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(instructor_extensions::table.load::<InstructorExtension>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(instructor_extensions::table
            .filter(instructor_extensions::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(instructor_extension: CreateInstructorExtension) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(instructor_extensions::table)
            .values(instructor_extension)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(
        id: Uuid,
        instructor_extension: UpdateInstructorExtension,
    ) -> Result<Self, CustomError> {
        Ok(diesel::update(instructor_extensions::table)
            .filter(instructor_extensions::id.eq(id))
            .set(instructor_extension)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(instructor_extensions::table.filter(instructor_extensions::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_zirkel(&self) -> Result<Vec<Zirkel>, CustomError> {
        Ok(InstructorExtensionToZirkelMapping::belonging_to(&self)
            .inner_join(zirkel::table)
            .select(zirkel::all_columns)
            .load::<Zirkel>(&mut db::connection()?)?)
    }

    pub async fn convert_to_public(&self) -> InstructorExtensionPublic {
        let instructor = Instructor::find(self.instructor_id).await;
        let (call_name, family_name_start, gender) = match instructor {
            Ok(inst) => (
                String::from(inst.call_name()),
                inst.family_name()
                    .unwrap_or(&String::from(""))
                    .chars()
                    .next()
                    .unwrap_or(' '),
                String::from(inst.gender()),
            ),
            Err(_) => (String::from(""), ' ', String::from("")),
        };

        InstructorExtensionPublic {
            id: self.id,
            name: String::from(format!("{} {}.", call_name, family_name_start)),
            gender: gender,
            instructor_id: self.instructor_id,
        }
    }

    pub async fn public_vec_by_uuids(
        uuids: Vec<Uuid>,
    ) -> Result<Vec<InstructorExtensionPublic>, CustomError> {
        let extensions: Vec<InstructorExtension> = instructor_extensions::table
            .filter(instructor_extensions::id.eq_any(uuids))
            .load::<InstructorExtension>(&mut db::connection()?)?;

        let futures: Vec<_> = extensions
            .into_iter()
            .map(|x| async move { x.convert_to_public().await })
            .collect();

        return Ok(join_all(futures).await);
    }
}

#[graphql_object(description = "Information about a specific Instructor-Extension", context = Context)]
impl InstructorExtension {
    #[graphql(description = "The id of the Instructor-Extension")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Instructor, the Instructor-Extension belongs to")]
    pub fn instructor_id(&self) -> Uuid {
        self.instructor_id
    }

    #[graphql(description = "The id of the Event, the Instructor-Extension belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "Main Choice of diet for the Instructor")]
    pub fn nutrition(&self) -> Option<&NutritionEnum> {
        self.nutrition.as_ref()
    }

    #[graphql(
        description = "Other Food-related restrictions that further specify the Instructors diet"
    )]
    pub fn food_restriction(&self) -> Option<&String> {
        self.food_restriction.as_ref()
    }

    #[graphql(description = "The way the Instructor will arrive at the Event")]
    pub fn arrival(&self) -> Option<&TraveltypeEnum> {
        self.arrival.as_ref()
    }

    #[graphql(description = "Extra notes concerning the arrival of the Instructor at the Event")]
    pub fn arrival_notes(&self) -> Option<&String> {
        self.arrival_notes.as_ref()
    }

    #[graphql(description = "The way the Instructor will depart from the Event")]
    pub fn departure(&self) -> Option<&TraveltypeEnum> {
        self.departure.as_ref()
    }

    #[graphql(
        description = "Extra notes concerning the departure of the Instructor from the Event"
    )]
    pub fn departure_notes(&self) -> Option<&String> {
        self.departure_notes.as_ref()
    }

    #[graphql(
        description = "Whether or not the Instructor has a contract during the duration of the Event"
    )]
    pub fn has_contract(&self) -> bool {
        self.has_contract
    }

    #[graphql(
        description = "With whom the Instructor has a contract during the duration of the Event"
    )]
    pub fn contract_with(&self) -> Option<&String> {
        self.contract_with.as_ref()
    }

    #[graphql(
        description = "The number of the room, the Instructor is living in. As this is a Orga-Field this relation must not be set from the beginning"
    )]
    pub fn room_number(&self) -> Option<&String> {
        self.room_number.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Fakultät"
    )]
    pub fn faculty(&self) -> Option<&String> {
        self.faculty.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Lehrstuhl"
    )]
    pub fn chair(&self) -> Option<&String> {
        self.chair.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Lehrstuhl"
    )]
    pub fn personnel_number(&self) -> Option<&String> {
        self.personnel_number.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Personalnummer"
    )]
    pub fn job_title(&self) -> Option<&String> {
        self.job_title.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: E-Mail (dienstlich)"
    )]
    pub fn email_work(&self) -> Option<&String> {
        self.email_work.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Telefon (dienstlich)"
    )]
    pub fn telephone_work(&self) -> Option<&String> {
        self.telephone_work.as_ref()
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Beginn der Reise"
    )]
    pub fn start_of_travel(&self) -> Option<NaiveDateTime> {
        self.start_of_travel
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Ende des Dienstgeschäfts"
    )]
    pub fn end_of_business_activities(&self) -> Option<NaiveDateTime> {
        self.end_of_business_activities
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Ankunft am Geschäftsort"
    )]
    pub fn arrival_at_business_location(&self) -> Option<NaiveDateTime> {
        match (self.arrival_at_business_location, self.start_of_travel()) {
            (Some(arrival), _) => Some(arrival),
            (None, Some(start)) => Some(start + Duration::hours(1)),
            (None, None) => None,
        }
    }

    #[graphql(
        description = "Direct readout of the arrival_at_business_location property. Used only for programmatic state feedback."
    )]
    pub fn arrival_at_business_location_direct_readout(&self) -> Option<NaiveDateTime> {
        self.arrival_at_business_location
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Beginn des Dienstgeschäfts"
    )]
    pub fn start_of_business_activities(&self) -> Option<NaiveDateTime> {
        match (
            self.start_of_business_activities,
            self.arrival_at_business_location(),
        ) {
            (Some(activities), _) => Some(activities),
            (None, Some(arrival)) => Some(arrival),
            (None, None) => None,
        }
    }

    #[graphql(
        description = "Direct readout of the start_of_business_activities property. Used only for programmatic state feedback."
    )]
    pub fn start_of_business_activities_direct_readout(&self) -> Option<NaiveDateTime> {
        self.start_of_business_activities
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Rückreise"
    )]
    pub fn start_of_return_journey(&self) -> Option<NaiveDateTime> {
        match (
            self.start_of_return_journey,
            self.end_of_business_activities(),
        ) {
            (Some(start), _) => Some(start),
            (None, Some(end)) => Some(end),
            (None, None) => None,
        }
    }

    #[graphql(
        description = "Direct readout of the start_of_return_journey property. Used only for programmatic state feedback."
    )]
    pub fn start_of_return_journey_direct_readout(&self) -> Option<NaiveDateTime> {
        self.start_of_return_journey
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Ende der Reise"
    )]
    pub fn end_of_travel(&self) -> Option<NaiveDateTime> {
        match (self.end_of_travel, self.end_of_business_activities()) {
            (Some(travel), _) => Some(travel),
            (None, Some(business)) => Some(business + Duration::hours(1)),
            (None, None) => None,
        }
    }

    #[graphql(
        description = "Direct readout of the end_of_travel property. Used only for programmatic state feedback."
    )]
    pub fn end_of_travel_direct_readout(&self) -> Option<NaiveDateTime> {
        self.end_of_travel
    }

    #[graphql(description = "The Instructor, the Instructor-Extension belongs to")]
    pub async fn instructor(&self, context: &Context) -> FieldResult<Option<Instructor>> {
        let instructor = Instructor::find(self.instructor_id).await.ok();
        graphql_access_advanced!(
            context,
            Role::ReadInstructor,
            match &instructor {
                Some(ext) => {
                    let uuids = context.has_access_to_instructor_uuids().await;
                    uuids.contains(&ext.id)
                }
                None => false,
            },
            instructor
        )
    }

    #[graphql(description = "The Event, the Instructor-Extension belongs to")]
    pub fn event(&self) -> FieldResult<Option<Event>> {
        Ok(Event::find(self.event_id).ok())
    }

    #[graphql(
        description = "The Zirkel, the Instructor that owns the Instructor-Extension teaches"
    )]
    pub fn zirkel(&self) -> FieldResult<Option<Vec<Zirkel>>> {
        Ok(self.relation_zirkel().ok())
    }

    #[graphql(
        description = "Whether or not the Instructor has their birthday during the duration of the event. Computed, will be false if birthday gets redacted"
    )]
    pub async fn has_birthday(&self) -> bool {
        let event = Event::find(self.event_id);
        let instructor = Instructor::find(self.instructor_id).await;

        let (start, end) = match event {
            Ok(event) => (event.start_date, event.end_date),
            Err(_) => (NaiveDate::MIN, NaiveDate::MIN),
        };
        let birthday = match instructor {
            Ok(instructor) => match instructor.birth_date {
                Some(date) => date,
                None => NaiveDate::MAX,
            },
            Err(_) => NaiveDate::MAX,
        };

        let mut day = birthday.day();
        let month = birthday.month();
        if month == 2 && day == 29 {
            day = 28; // birthday on leap year, always check for the 28th
        }
        let constr_birthday = NaiveDate::from_ymd_opt(start.year(), month, day).unwrap();

        constr_birthday >= start && constr_birthday <= end
    }

    #[graphql(
        description = "Field for creating 'travel-authorization-requests'. German Form Name: Übernachtungskosten"
    )]
    pub fn accommodation_costs(&self) -> f64 {
        let event = Event::find(self.event_id);

        let overnight_costs = match &event {
            Ok(event) => event.overnight_costs().unwrap_or(0.0),
            Err(_) => 1.0,
        };

        let mut start_of_travel = self.start_of_travel().unwrap_or(NaiveDateTime::MIN);
        let mut end_of_travel = self.end_of_travel().unwrap_or(
            start_of_travel
                .checked_add_signed(Duration::days(1))
                .unwrap(),
        ); // this should hopefully never possibly overflow
        start_of_travel = start_of_travel
            .with_hour(2)
            .unwrap()
            .with_minute(0)
            .unwrap()
            .with_second(0)
            .unwrap();
        end_of_travel = end_of_travel
            .with_hour(22)
            .unwrap()
            .with_minute(0)
            .unwrap()
            .with_second(0)
            .unwrap();
        // manually overwrite times to start and end of day, to ensure full days are recognized, IF a night is spent, no matter the start and end TIME

        let day_difference = end_of_travel
            .signed_duration_since(start_of_travel)
            .num_days();
        let calculated_costs = (day_difference as f64) * overnight_costs;

        match self.accommodation_costs {
            Some(stored_costs) => stored_costs,
            None => ((calculated_costs * 100.0).round() / 100.0).into(),
        }
    }

    #[graphql(
        description = "Direct readout of the accommodation_costs property. Used only for programmatic state feedback."
    )]
    pub fn accommodation_costs_direct_readout(&self) -> Option<f64> {
        self.accommodation_costs
    }
}

#[derive(Serialize, Deserialize)]
pub struct InstructorExtensionPublic {
    pub id: Uuid,
    pub name: String,
    pub gender: String,
    pub instructor_id: Uuid,
}

#[graphql_object(description = "Information about a specific Instructor-Extension (Public-Facing subset only)", context = Context)]
impl InstructorExtensionPublic {
    #[graphql(description = "The id of the Instructor-Extension")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The Instructor-Extensions name with abbreviated surname")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "If set in instructorExtension, the preferred gender of the Instructor. If unset will return the sex of the Instructor")]
    pub fn gender(&self) -> &String {
        &self.gender
    }

    #[graphql(
        description = "The base-id of the Instructor that the Instructor-Extension belongs to"
    )]
    pub fn instructor_id(&self) -> Uuid {
        self.instructor_id
    }

    #[graphql(
        description = "The number of the room, the Instructor is living in. Access helper that directly targets the main Extension"
    )]
    pub fn room_number(&self) -> FieldResult<Option<String>> {
        Ok(InstructorExtension::find(self.id)?.room_number().cloned())
    }

    #[graphql(
        description = "The Zirkel, the Instructor that owns the Instructor-Extension teaches. Access helper that directly targets the main Extension"
    )]
    pub fn zirkel(&self) -> FieldResult<Option<Vec<Zirkel>>> {
        InstructorExtension::find(self.id)?.zirkel()
    }
}
