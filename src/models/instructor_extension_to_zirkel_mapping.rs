use crate::database::instructor_extension_to_zirkel_mapping;
use crate::db;
use crate::error_handler::CustomError;
use crate::models::{InstructorExtension, Zirkel};
use diesel::prelude::*;
use diesel::Insertable;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable)]
#[diesel(table_name = instructor_extension_to_zirkel_mapping)]
pub struct CreateInstructorExtensionToZirkelMapping {
    pub instructor_extension_id: Uuid,
    pub zirkel_id: Uuid,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(InstructorExtension))]
#[diesel(belongs_to(Zirkel))]
#[diesel(table_name = instructor_extension_to_zirkel_mapping)]
pub struct InstructorExtensionToZirkelMapping {
    pub id: Uuid,
    pub instructor_extension_id: Uuid,
    pub zirkel_id: Uuid,
}

impl InstructorExtensionToZirkelMapping {
    pub fn create(instructor_extension_id: Uuid, zirkel_id: Uuid) -> Result<i32, CustomError> {
        let mapping = CreateInstructorExtensionToZirkelMapping {
            instructor_extension_id,
            zirkel_id,
        };

        Ok(i32::try_from(
            diesel::insert_into(instructor_extension_to_zirkel_mapping::table)
                .values(&mapping)
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn delete(instructor_extension_id: Uuid, zirkel_id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(
                instructor_extension_to_zirkel_mapping::table
                    .filter(
                        instructor_extension_to_zirkel_mapping::instructor_extension_id
                            .eq(instructor_extension_id),
                    )
                    .filter(instructor_extension_to_zirkel_mapping::zirkel_id.eq(zirkel_id)),
            )
            .execute(&mut db::connection()?)?,
        )?)
    }
}
