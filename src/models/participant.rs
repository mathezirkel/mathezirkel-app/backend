use crate::auth::Role;
use crate::database::participants;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::{graphql_access, Context};
use crate::models::{Extension, ReconnectionHash, SexEnum};
use chrono::{NaiveDate, NaiveDateTime};
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = participants)]
pub struct CreateParticipant {
    pub family_name: String,
    pub given_name: String,
    pub call_name: Option<String>,
    pub birth_date: NaiveDate,
    pub sex: SexEnum,
    pub gender: Option<String>,
    pub street: String,
    pub street_number: String,
    pub postal_code: String,
    pub city: String,
    pub country: String,
    pub notes: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = participants)]
pub struct UpdateParticipant {
    pub family_name: Option<String>,
    pub given_name: Option<String>,
    pub call_name: Option<String>,
    pub birth_date: Option<NaiveDate>,
    pub sex: Option<SexEnum>,
    pub gender: Option<String>,
    pub street: Option<String>,
    pub street_number: Option<String>,
    pub postal_code: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,
    pub notes: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable)]
#[diesel(belongs_to(Extension))]
pub struct Participant {
    pub id: Uuid,
    pub family_name: String,
    pub family_name_created_at: NaiveDateTime,
    pub family_name_updated_at: NaiveDateTime,
    pub given_name: String,
    pub given_name_created_at: NaiveDateTime,
    pub given_name_updated_at: NaiveDateTime,
    pub call_name: Option<String>,
    pub call_name_created_at: NaiveDateTime,
    pub call_name_updated_at: NaiveDateTime,
    pub birth_date: NaiveDate,
    pub birth_date_created_at: NaiveDateTime,
    pub birth_date_updated_at: NaiveDateTime,
    pub sex: SexEnum,
    pub sex_created_at: NaiveDateTime,
    pub sex_updated_at: NaiveDateTime,
    pub gender: Option<String>,
    pub gender_created_at: NaiveDateTime,
    pub gender_updated_at: NaiveDateTime,
    pub street: String,
    pub street_created_at: NaiveDateTime,
    pub street_updated_at: NaiveDateTime,
    pub street_number: String,
    pub street_number_created_at: NaiveDateTime,
    pub street_number_updated_at: NaiveDateTime,
    pub postal_code: String,
    pub postal_code_created_at: NaiveDateTime,
    pub postal_code_updated_at: NaiveDateTime,
    pub city: String,
    pub city_created_at: NaiveDateTime,
    pub city_updated_at: NaiveDateTime,
    pub country: String,
    pub country_created_at: NaiveDateTime,
    pub country_updated_at: NaiveDateTime,
    pub notes: Option<String>,
    pub notes_created_at: NaiveDateTime,
    pub notes_updated_at: NaiveDateTime,
}

impl Participant {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(participants::table.load::<Participant>(&mut db::connection()?)?)
    }

    pub fn find_all_filter_uuid(filter_uuids: &Vec<Uuid>) -> Result<Vec<Self>, CustomError> {
        Ok(participants::table
            .filter(participants::dsl::id.eq_any(filter_uuids))
            .load::<Participant>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(participants::table
            .filter(participants::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(participant: CreateParticipant) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(participants::table)
            .values(participant)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, participant: UpdateParticipant) -> Result<Self, CustomError> {
        Ok(diesel::update(participants::table)
            .filter(participants::id.eq(id))
            .set(participant)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(participants::table.filter(participants::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_extensions(&self) -> Result<Vec<Extension>, CustomError> {
        Ok(Extension::belonging_to(&self).load::<Extension>(&mut db::connection()?)?)
    }

    pub fn relation_hashes(&self) -> Result<Vec<ReconnectionHash>, CustomError> {
        Ok(
            ReconnectionHash::belonging_to(&self)
                .load::<ReconnectionHash>(&mut db::connection()?)?,
        )
    }

    pub fn create_new_set_of_hashes(&self) -> () {
        let hashes = ReconnectionHash::generate_hashes(
            self.family_name(),
            &self.given_name, // given name function may be redacted role-based. For the purpose of hash generation this usage can be considered acceptable
            &self.call_name(),
            &self.birth_date, // birth date function may be redacted role-based. For the purpose of hash generation this usage can be considered acceptable
        );

        for hash in hashes {
            let _ = ReconnectionHash::create({
                super::CreateReconnectionHash {
                    hash,
                    participant_id: self.id,
                }
            });
        }
    }
}

#[graphql_object(description = "Information about a participating person", context = Context)]
impl Participant {
    #[graphql(description = "The id of the Participant")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The surname of the Participant")]
    pub fn family_name(&self) -> &String {
        &self.family_name
    }

    #[graphql(description = "The by-law name of the Participant")]
    pub fn given_name(&self, context: &Context) -> String {
        if !context.check_role(Role::ReadParticipant) {
            return String::from("REDACTED");
        }
        self.given_name.clone()
    }

    #[graphql(
        description = "The name the Participant wants to be called, if set. If unset, the first substring of the GivenName of the Participant"
    )]
    pub fn call_name(&self) -> String {
        let empty = String::from("");
        let call_name = self.call_name.as_ref().unwrap_or(&empty);

        if call_name == "" {
            String::from(
                &self
                    .given_name
                    .split_whitespace()
                    .next()
                    .map(|s| s.to_string())
                    .unwrap_or_else(|| String::new()),
            )
        } else {
            String::from(call_name)
        }
    }

    #[graphql(
        description = "Direct readout of the call_name property. Used only for programmatic state feedback."
    )]
    pub fn call_name_direct_readout(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.call_name.clone()
    }

    #[graphql(description = "The date the Participant was born")]
    pub fn birth_date(&self, context: &Context) -> NaiveDate {
        if !context.check_role(Role::ReadParticipant) {
            return NaiveDate::from_ymd_opt(1, 1, 1).unwrap();
        }
        self.birth_date.clone()
    }

    #[graphql(description = "The per-law sex a person will need to provide")]
    pub fn sex(&self, context: &Context) -> SexEnum {
        if !context.check_role(Role::ReadParticipant) {
            return SexEnum::Redacted;
        }
        self.sex.clone()
    }

    #[graphql(
        description = "If set, the preferred gender of the Participant. If unset will return the sex of the Participant"
    )]
    pub fn gender(&self) -> String {
        let empty = String::from("");
        let gender = self.gender.as_ref().unwrap_or(&empty);

        if gender == "" {
            String::from(&self.sex.value())
        } else {
            String::from(gender)
        }
    }

    #[graphql(
        description = "Direct readout of the gender property. Used only for programmatic state feedback."
    )]
    pub fn gender_direct_readout(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(SexEnum::Redacted.value());
        }
        self.gender.clone()
    }

    #[graphql(description = "The street the Participant is living in")]
    pub fn street(&self) -> &String {
        &self.street
    }

    #[graphql(description = "The number of the house the Participant is living in")]
    pub fn street_number(&self) -> &String {
        &self.street_number
    }

    #[graphql(description = "The postal code of the city the Participant is living in")]
    pub fn postal_code(&self) -> &String {
        &self.postal_code
    }

    #[graphql(description = "The city the Participant is living in")]
    pub fn city(&self) -> &String {
        &self.city
    }

    #[graphql(description = "The country the Participant is living in")]
    pub fn country(&self) -> &String {
        &self.country
    }

    #[graphql(description = "Additional notes that are attached to a Participant directly")]
    pub fn notes(&self, context: &Context) -> Option<String> {
        if !context.check_role(Role::ReadParticipant) {
            return Some(String::from("REDACTED"));
        }
        self.notes.clone()
    }

    #[graphql(description = "The Participant-Extensions that belong to the Participant")]
    pub fn extensions(&self, context: &Context) -> Vec<Extension> {
        let extensions = self.relation_extensions().unwrap_or_default();
        if context.check_role(Role::ReadParticipant) {
            extensions
        } else {
            let access_to_extension_uuids = context.has_access_to_participant_extension_uuids();
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }

    #[graphql(description = "The hashes that belong to the Participant")]
    pub fn hashes(&self, context: &Context) -> FieldResult<Option<Vec<ReconnectionHash>>> {
        graphql_access!(context, Role::ReadParticipant, self.relation_hashes().ok())
    }
}
