use crate::auth::{hash_sha256, Role};
use crate::database::reconnection_hashes;
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::{graphql_access, Context};
use crate::models::Participant;
use chrono::{NaiveDate, NaiveDateTime};
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable)]
#[diesel(table_name = reconnection_hashes)]
pub struct CreateReconnectionHash {
    pub hash: String,
    pub participant_id: Uuid,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Participant))]
#[diesel(primary_key(hash, participant_id))]
#[diesel(table_name = reconnection_hashes)]
pub struct ReconnectionHash {
    pub hash: String,
    pub participant_id: Uuid,
    pub timestamp: NaiveDateTime,
}

impl ReconnectionHash {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(reconnection_hashes::table.load::<ReconnectionHash>(&mut db::connection()?)?)
    }

    pub fn create(reconnection_hash: CreateReconnectionHash) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(reconnection_hashes::table)
            .values(reconnection_hash)
            .get_result(&mut db::connection()?)?)
    }

    pub fn generate_hashes(
        family_name: &String,
        given_name: &String,
        call_name: &String,
        birth_date: &NaiveDate,
    ) -> Vec<String> {
        let inputs = [
            String::from(format!("%{}%{}%", family_name, call_name)).to_lowercase(),
            String::from(format!("%{}%{}%", family_name, given_name)).to_lowercase(),
            String::from(format!("%{}%{}%", family_name, birth_date)).to_lowercase(),
            String::from(format!("%{}%{}%", call_name, birth_date)).to_lowercase(),
            String::from(format!("%{}%{}%{}%", family_name, call_name, birth_date)).to_lowercase(),
        ];

        // THIS FUNCTION MUST STAY EXACTLY THIS WAY IF RECONNECTION SHOULD BE POSSIBLE IN THE FUTURE
        inputs
            .to_vec()
            .iter()
            .filter(|&s| {
                !s.contains("0001-01-01")
                    && !s.contains("2010-01-01")
                    && !s.contains("redacted")
                    && !s.contains("%%")
            })
            .map(|input| hash_sha256(input))
            .collect()
    }
}

#[graphql_object(description = "A Hash of the sensitive personal information used for the possibility of reconnecting", context = Context)]
impl ReconnectionHash {
    #[graphql(description = "The hash of the sensitive information")]
    pub fn hash(&self) -> &String {
        &self.hash
    }

    #[graphql(description = "The id of the Participant, the hash belongs to")]
    pub fn participant_id(&self) -> Uuid {
        self.participant_id
    }

    #[graphql(description = "The creation timestamp of the hash")]
    pub fn timestamp(&self) -> NaiveDateTime {
        self.timestamp
    }

    #[graphql(description = "The Participant, the hash belongs to")]
    pub fn participant(&self, context: &Context) -> FieldResult<Option<Participant>> {
        // Here ReadParticipant check is good (no access with uuid-extra-check), because stops from de-anonymizing if one has no read access (can not read hashes without read anyway)
        graphql_access!(
            context,
            Role::ReadParticipant,
            Participant::find(self.participant_id).ok()
        )
    }
}
