use crate::auth::Role;
use crate::database::{
    extensions, instructor_extensions, zirkel, zirkel_plan_entries, zirkel_plan_slots,
};
use crate::db;
use crate::error_handler::CustomError;
use crate::graphql::Context;
use crate::models::{
    Event, Extension, InstructorExtension, InstructorExtensionPublic,
    InstructorExtensionToZirkelMapping, ZirkelPlanEntry,
};
use diesel::dsl::sql;
use diesel::prelude::*;
use diesel::sql_types::{Bool, Uuid as DieselUuid};
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel)]
pub struct CreateZirkel {
    pub event_id: Uuid,
    pub name: String,
    pub topics: Option<Vec<String>>,
    pub room: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel)]
pub struct UpdateZirkel {
    pub event_id: Option<Uuid>,
    pub name: Option<String>,
    pub topics: Option<Vec<String>>,
    pub room: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Event))]
#[diesel(table_name = zirkel)]
pub struct Zirkel {
    pub id: Uuid,
    pub event_id: Uuid,
    pub name: String,
    pub topics: Vec<String>,
    pub room: Option<String>,
}

impl Zirkel {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(zirkel::table.load::<Zirkel>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(zirkel::table
            .filter(zirkel::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(zirkel: CreateZirkel) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(zirkel::table)
            .values(zirkel)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, zirkel: UpdateZirkel) -> Result<Self, CustomError> {
        Ok(diesel::update(zirkel::table)
            .filter(zirkel::id.eq(id))
            .set(zirkel)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(zirkel::table.filter(zirkel::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_extensions(&self) -> Result<Vec<Extension>, CustomError> {
        Ok(extensions::table
            .select(extensions::all_columns)
            .filter(
                sql::<Bool>("")
                    .bind::<DieselUuid, _>(self.id)
                    .sql("= ANY(extensions.zirkel_ids)"),
            )
            .load::<Extension>(&mut db::connection()?)?)
    }

    pub fn relation_instructor_extensions(&self) -> Result<Vec<InstructorExtension>, CustomError> {
        Ok(InstructorExtensionToZirkelMapping::belonging_to(&self)
            .inner_join(instructor_extensions::table)
            .select(instructor_extensions::all_columns)
            .load::<InstructorExtension>(&mut db::connection()?)?)
    }

    pub fn relation_zirkel_plan_entries(&self) -> Result<Vec<ZirkelPlanEntry>, CustomError> {
        Ok(ZirkelPlanEntry::belonging_to(&self).load::<ZirkelPlanEntry>(&mut db::connection()?)?)
    }

    pub fn relation_zirkel_plan_entries_with_certificate(
        &self,
    ) -> Result<Vec<ZirkelPlanEntry>, CustomError> {
        Ok(ZirkelPlanEntry::belonging_to(&self)
            .inner_join(
                zirkel_plan_slots::table
                    .on(zirkel_plan_slots::id.eq(zirkel_plan_entries::zirkel_plan_slot_id)),
            )
            .filter(zirkel_plan_slots::certificate.eq(true))
            .select(zirkel_plan_entries::all_columns)
            .load::<ZirkelPlanEntry>(&mut db::connection()?)?)
    }
}

#[graphql_object(description = "Information about a specific Zirkel", context = Context)]
impl Zirkel {
    #[graphql(description = "The id of the Zirkel")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Event the Zirkel belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "The name of the Zirkel")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "The topics that will be taught in the Zirkel")]
    pub fn topics(&self) -> Vec<String> {
        let mut direct_topics = self.topics.clone();

        let entries = match self.relation_zirkel_plan_entries_with_certificate() {
            Ok(entry) => entry,
            Err(_) => Vec::new(),
        };

        let topics_plan: Vec<_> = entries
            .into_iter()
            .filter_map(|entry| {
                if let Some(ref topic) = entry.topic {
                    if !topic.is_empty() {
                        return Some(topic.clone());
                    }
                }
                None
            })
            .collect();

        direct_topics.extend(topics_plan);

        direct_topics
    }

    #[graphql(description = "The topics that are requested to be taught in the Zirkel")]
    pub fn topic_wishes(&self) -> Vec<String> {
        let extensions = match self.relation_extensions() {
            Ok(exts) => exts,
            Err(_) => return Vec::new(),
        };

        extensions
            .into_iter()
            .filter(|ext| ext.confirmed) // signed off wishes still displayed
            .flat_map(|ext| ext.topic_wishes)
            .collect()
    }

    #[graphql(description = "The number of participants that are taught in the Zirkel")]
    pub fn number_of_participants(&self) -> i32 {
        let extensions = match self.relation_extensions() {
            Ok(exts) => exts,
            Err(_) => Vec::new(),
        };

        extensions
            .into_iter()
            .filter(|ext| ext.confirmed && ext.time_of_signoff == None)
            .count()
            .try_into()
            .unwrap_or_default()
    }

    #[graphql(description = "The room, the Zirkel will take place in")]
    pub fn room(&self) -> Option<&String> {
        self.room.as_ref()
    }

    #[graphql(description = "The Participant-Extensions that belong to the Zirkel")]
    pub fn extensions(&self, context: &Context) -> Vec<Extension> {
        let extensions = self.relation_extensions().unwrap_or_default();
        if context.check_role(Role::ReadParticipant) {
            extensions
        } else {
            let access_to_extension_uuids = context.has_access_to_participant_extension_uuids();
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }

    #[graphql(description = "The Event, the Zirkel belongs to")]
    pub fn event(&self) -> FieldResult<Option<Event>> {
        Ok(Event::find(self.event_id).ok())
    }

    #[graphql(description = "The Instructor-Extensions of the Instructors that teach this Zirkel")]
    pub async fn instructor_extensions(&self, context: &Context) -> Vec<InstructorExtension> {
        let extensions = self.relation_instructor_extensions().unwrap_or_default();
        if context.check_role(Role::ReadInstructor) {
            extensions
        } else {
            let access_to_extension_uuids =
                context.has_access_to_instructor_extension_uuids().await;
            extensions
                .into_iter()
                .filter(|ext| access_to_extension_uuids.contains(&ext.id))
                .collect()
        }
    }

    #[graphql(
        description = "The Instructor-Extensions that belong to the Zirkel (Public view only)"
    )]
    pub async fn instructor_extensions_public(
        &self,
    ) -> FieldResult<Option<Vec<InstructorExtensionPublic>>> {
        let uuids = self
            .relation_instructor_extensions()
            .unwrap_or_default()
            .iter()
            .map(|x| x.id)
            .collect();

        return Ok(InstructorExtension::public_vec_by_uuids(uuids).await.ok());
    }

    #[graphql(description = "The ZirkelPlanEntries that belong to the Zirkel")]
    pub fn zirkel_plan_entries(&self) -> FieldResult<Option<Vec<ZirkelPlanEntry>>> {
        Ok(self.relation_zirkel_plan_entries().ok())
    }
}
