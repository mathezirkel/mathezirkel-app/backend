use super::{InstructorExtension, InstructorExtensionPublic, Zirkel};
use crate::database::{zirkel_plan_entries, zirkel_plan_slots};
use crate::db;
use crate::error_handler::CustomError;
use crate::models::Event;
use crate::Context;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::Insertable;
use juniper::{graphql_object, FieldResult, GraphQLInputObject};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};
use uuid::Uuid;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel_plan_slots)]
pub struct CreateZirkelPlanSlot {
    pub event_id: Uuid,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub name: String,
    pub certificate: Option<bool>,
    pub beamer: Option<bool>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel_plan_slots)]
pub struct UpdateZirkelPlanSlot {
    pub event_id: Option<Uuid>,
    pub start_time: Option<NaiveDateTime>,
    pub end_time: Option<NaiveDateTime>,
    pub name: Option<String>,
    pub certificate: Option<bool>,
    pub beamer: Option<bool>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(Event))]
#[diesel(table_name = zirkel_plan_slots)]
pub struct ZirkelPlanSlot {
    pub id: Uuid,
    pub event_id: Uuid,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub name: String,
    pub certificate: bool,
    pub beamer: bool,
}

impl ZirkelPlanSlot {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(zirkel_plan_slots::table.load::<ZirkelPlanSlot>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(zirkel_plan_slots::table
            .filter(zirkel_plan_slots::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(zirkel_plan_slot: CreateZirkelPlanSlot) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(zirkel_plan_slots::table)
            .values(zirkel_plan_slot)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, zirkel_plan_slot: UpdateZirkelPlanSlot) -> Result<Self, CustomError> {
        Ok(diesel::update(zirkel_plan_slots::table)
            .filter(zirkel_plan_slots::id.eq(id))
            .set(zirkel_plan_slot)
            .get_result(&mut db::connection()?)?)
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(zirkel_plan_slots::table.filter(zirkel_plan_slots::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn relation_zirkel_plan_entries(&self) -> Result<Vec<ZirkelPlanEntry>, CustomError> {
        Ok(ZirkelPlanEntry::belonging_to(&self).load::<ZirkelPlanEntry>(&mut db::connection()?)?)
    }
}

#[graphql_object(description = "A slot in the ZirkelPlan", context = Context)]
impl ZirkelPlanSlot {
    #[graphql(description = "The id of the ZirkelPlanSlot")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the Event, the ZirkelPlanSlot belongs to")]
    pub fn event_id(&self) -> Uuid {
        self.event_id
    }

    #[graphql(description = "The description of the ZirkelPlanSlot")]
    pub fn name(&self) -> &String {
        &self.name
    }

    #[graphql(description = "A timestamp that describes the start of the ZirkelPlanSlot")]
    pub fn start_time(&self) -> NaiveDateTime {
        self.start_time
    }

    #[graphql(description = "A timestamp that describes the start of the ZirkelPlanSlot")]
    pub fn end_time(&self) -> NaiveDateTime {
        self.end_time
    }

    #[graphql(description = "Whether the slot is relevant for the certificate-generation")]
    pub fn certificate(&self) -> bool {
        self.certificate
    }

    #[graphql(description = "Whether the slot is relevant for the Beamer-plan")]
    pub fn beamer(&self) -> bool {
        self.beamer
    }

    #[graphql(description = "The ZirkelPlanEntries that belong to the ZirkelPlanSlot")]
    pub fn zirkel_plan_entries(&self) -> FieldResult<Option<Vec<ZirkelPlanEntry>>> {
        Ok(self.relation_zirkel_plan_entries().ok())
    }

    #[graphql(description = "The Event, the ZirkelPlanSlot belongs to")]
    pub fn event(&self) -> FieldResult<Option<Event>> {
        Ok(Event::find(self.event_id).ok())
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel_plan_entries)]
pub struct CreateZirkelPlanEntry {
    pub zirkel_plan_slot_id: Uuid,
    pub zirkel_id: Uuid,
    pub topic: Option<String>,
    pub beamer: Option<bool>,
    pub notes: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, GraphQLInputObject)]
#[diesel(table_name = zirkel_plan_entries)]
pub struct UpdateZirkelPlanEntry {
    pub zirkel_plan_slot_id: Option<Uuid>,
    pub zirkel_id: Option<Uuid>,
    pub topic: Option<String>,
    pub beamer: Option<bool>,
    pub notes: Option<String>,
}

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Identifiable, Associations)]
#[diesel(belongs_to(ZirkelPlanSlot))]
#[diesel(belongs_to(Zirkel))]
#[diesel(table_name = zirkel_plan_entries)]
pub struct ZirkelPlanEntry {
    pub id: Uuid,
    pub zirkel_plan_slot_id: Uuid,
    pub zirkel_id: Uuid,
    pub instructor_extension_uuids: Vec<Uuid>,
    pub topic: Option<String>,
    pub beamer: bool,
    pub notes: Option<String>,
    pub reference_entry_id: Option<Uuid>,
    pub auto_update: bool,
}

impl ZirkelPlanEntry {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        Ok(zirkel_plan_entries::table.load::<ZirkelPlanEntry>(&mut db::connection()?)?)
    }

    pub fn find(id: Uuid) -> Result<Self, CustomError> {
        Ok(zirkel_plan_entries::table
            .filter(zirkel_plan_entries::id.eq(id))
            .first(&mut db::connection()?)?)
    }

    pub fn create(zirkel_plan_entry: CreateZirkelPlanEntry) -> Result<Self, CustomError> {
        Ok(diesel::insert_into(zirkel_plan_entries::table)
            .values(zirkel_plan_entry)
            .get_result(&mut db::connection()?)?)
    }

    pub fn update(id: Uuid, zirkel_plan_entry: UpdateZirkelPlanEntry) -> Result<Self, CustomError> {
        Ok(diesel::update(zirkel_plan_entries::table)
            .filter(zirkel_plan_entries::id.eq(id))
            .set(zirkel_plan_entry)
            .get_result(&mut db::connection()?)?)
    }

    pub fn link_entry_to_entry(source_uuid: Uuid, target_uuid: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::update(zirkel_plan_entries::table)
                .filter(zirkel_plan_entries::id.eq_any([source_uuid, target_uuid]))
                .set(zirkel_plan_entries::reference_entry_id.eq(target_uuid))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn unset_link_relations(&self) -> Result<i32, CustomError> {
        if self.id == self.reference_entry_id.unwrap_or_default() {
            // is a target, remove all references

            return Ok(i32::try_from(
                diesel::update(zirkel_plan_entries::table)
                    .filter(zirkel_plan_entries::reference_entry_id.eq(self.reference_entry_id))
                    .set(zirkel_plan_entries::reference_entry_id.eq(None::<Uuid>))
                    .execute(&mut db::connection()?)?,
            )?);
        }

        // is a source, remove only its link
        return Ok(i32::try_from(
            diesel::update(zirkel_plan_entries::table)
                .filter(zirkel_plan_entries::id.eq(self.id))
                .set(zirkel_plan_entries::reference_entry_id.eq(None::<Uuid>))
                .execute(&mut db::connection()?)?,
        )?);
    }

    pub fn delete(id: Uuid) -> Result<i32, CustomError> {
        Ok(i32::try_from(
            diesel::delete(zirkel_plan_entries::table.filter(zirkel_plan_entries::id.eq(id)))
                .execute(&mut db::connection()?)?,
        )?)
    }

    pub fn add_instructor_extension_to_entry(
        &self,
        instructor_extension: InstructorExtension,
    ) -> Result<i32, CustomError> {
        let mut current_ids = self.instructor_extension_uuids.clone();
        let new_uuid = instructor_extension.id;

        if !current_ids.contains(&new_uuid) {
            let slot = match ZirkelPlanSlot::find(self.zirkel_plan_slot_id) {
                Err(_) => return Ok(0),
                Ok(slt) => slt,
            };

            if slot.event_id == instructor_extension.event_id {
                current_ids.push(new_uuid);

                return Ok(i32::try_from(
                    diesel::update(zirkel_plan_entries::table)
                        .filter(zirkel_plan_entries::id.eq(self.id))
                        .set(zirkel_plan_entries::instructor_extension_uuids.eq(current_ids))
                        .execute(&mut db::connection()?)?,
                )?);
            }
        }

        return Ok(0);
    }

    pub fn remove_instructor_extension_from_entry(
        &self,
        instructor_extension: InstructorExtension,
    ) -> Result<i32, CustomError> {
        let mut current_ids = self.instructor_extension_uuids.clone();
        let remove_uuid = instructor_extension.id;

        if current_ids.contains(&remove_uuid) {
            let index = current_ids.iter().position(|x| *x == remove_uuid).unwrap(); // cannot fail because contains checked
            current_ids.remove(index);

            return Ok(i32::try_from(
                diesel::update(zirkel_plan_entries::table)
                    .filter(zirkel_plan_entries::id.eq(self.id))
                    .set(zirkel_plan_entries::instructor_extension_uuids.eq(current_ids))
                    .execute(&mut db::connection()?)?,
            )?);
        }

        return Ok(0);
    }

    pub fn force_instructor_extension_uuid_array(
        &self,
        uuids: Vec<Uuid>,
    ) -> Result<i32, CustomError> {
        return Ok(i32::try_from(
            diesel::update(zirkel_plan_entries::table)
                .filter(zirkel_plan_entries::id.eq(self.id))
                .set(zirkel_plan_entries::instructor_extension_uuids.eq(uuids))
                .execute(&mut db::connection()?)?,
        )?);
    }
}

#[graphql_object(description = "An entry in a ZirkelPlanSlot", context = Context)]
impl ZirkelPlanEntry {
    #[graphql(description = "The id of the ZirkelPlanEntry")]
    pub fn id(&self) -> Uuid {
        self.id
    }

    #[graphql(description = "The id of the ZirkelPlanEntry that this entry is referencing")]
    pub fn reference_entry_id(&self) -> Option<&Uuid> {
        self.reference_entry_id.as_ref()
    }

    #[graphql(description = "The id of the Zirkel, the ZirkelPlanEntry belongs to")]
    pub fn zirkel_id(&self) -> Uuid {
        self.zirkel_id
    }

    #[graphql(description = "The id of the ZirkelPlanSlot, the ZirkelPlanEntry belongs to")]
    pub fn zirkel_plan_slot_id(&self) -> Uuid {
        self.zirkel_plan_slot_id
    }

    #[graphql(description = "The topic of the ZirkelPlanEntry")]
    pub fn topic(&self) -> Option<&String> {
        self.topic.as_ref()
    }

    #[graphql(description = "The notes of the ZirkelPlanEntry")]
    pub fn notes(&self) -> Option<&String> {
        self.notes.as_ref()
    }

    #[graphql(description = "The color of the ZirkelPlanEntry")]
    pub fn color(&self, context: &Context) -> String {
        return context.get_color_for_zirkel_plan_entry(self);
    }

    #[graphql(description = "Whether the entry needs a beamer")]
    pub fn beamer(&self) -> bool {
        self.beamer
    }

    #[graphql(description = "The Zirkel, the ZirkelPlanEntry belongs to")]
    pub fn zirkel(&self) -> FieldResult<Option<Zirkel>> {
        Ok(Zirkel::find(self.zirkel_id).ok())
    }

    #[graphql(description = "The ZirkelPlanSlot, the ZirkelPlanEntry belongs to")]
    pub fn zirkel_plan_slot(&self) -> FieldResult<Option<ZirkelPlanSlot>> {
        Ok(ZirkelPlanSlot::find(self.zirkel_plan_slot_id).ok())
    }

    #[graphql(description = "The InstructorExtensions, that belong to the ZirkelPlanEntry")]
    pub async fn instructor_extensions_public(
        &self,
    ) -> FieldResult<Option<Vec<InstructorExtensionPublic>>> {
        return Ok(InstructorExtension::public_vec_by_uuids(
            self.instructor_extension_uuids.clone(),
        )
        .await
        .ok());
    }
}

lazy_static! {
    static ref MUTEX: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
}

fn acquire_lock_to_mutex() -> bool {
    let binding = MUTEX.clone();
    let mut guard_locked = binding.lock().unwrap();

    if !(*guard_locked) {
        *guard_locked = true;
        return true; // successfully acquired lock
    }
    return false; // couldn't acquire lock
}

fn give_back_lock_to_mutex() {
    let binding = MUTEX.clone();
    let mut guard_locked = binding.lock().unwrap();
    *guard_locked = false;
}

pub async fn add_instructor_extension_to_zirkel_plan_entry_thread_save(
    instructor_extension_id: Uuid,
    zirkel_plan_entry_id: Uuid,
) -> Result<i32, CustomError> {
    while !acquire_lock_to_mutex() {
        actix_web::rt::task::yield_now().await;
    }

    // add to ZirkelPlanEntry
    let entry = match ZirkelPlanEntry::find(zirkel_plan_entry_id) {
        Err(err) => {
            give_back_lock_to_mutex(); // early return MUST give back mutex
            return Err(err);
        }
        Ok(ext) => ext,
    };
    let instructor_extension = match InstructorExtension::find(instructor_extension_id) {
        Err(err) => {
            give_back_lock_to_mutex(); // early return MUST give back mutex
            return Err(err);
        }
        Ok(inst) => inst,
    };

    let res = entry.add_instructor_extension_to_entry(instructor_extension);

    give_back_lock_to_mutex();

    return res;
}

pub async fn remove_instructor_extension_from_zirkel_plan_entry_thread_save(
    instructor_extension_id: Uuid,
    zirkel_plan_entry_id: Uuid,
) -> Result<i32, CustomError> {
    while !acquire_lock_to_mutex() {
        actix_web::rt::task::yield_now().await;
    }
    // add to ZirkelPlanEntry
    let entry = match ZirkelPlanEntry::find(zirkel_plan_entry_id) {
        Err(err) => {
            give_back_lock_to_mutex(); // early return MUST give back mutex
            return Err(err);
        }
        Ok(ext) => ext,
    };
    let instructor_extension = match InstructorExtension::find(instructor_extension_id) {
        Err(err) => {
            give_back_lock_to_mutex(); // early return MUST give back mutex
            return Err(err);
        }
        Ok(inst) => inst,
    };

    let res = entry.remove_instructor_extension_from_entry(instructor_extension);

    give_back_lock_to_mutex();

    return res;
}
