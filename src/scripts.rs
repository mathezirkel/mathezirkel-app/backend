use mathezirkel_app_scriptserver_interfaces::{
    JQRequestPostVariables, LatexRequestPostVariables, ProbeRequestPostVariables,
    PythonRequestPostVariables, ScriptFile, ScriptResponse, VersionRequestPostVariables,
};
use reqwest;
use tokio::time::sleep;
use std::{env, time::Duration};
use url::Url;

pub async fn jq(json: &String, filter: &String) -> ScriptResponse {
    script_request::<JQRequestPostVariables>(
        String::from("jq"),
        JQRequestPostVariables {
            filter: String::from(filter),
            json: String::from(json),
        },
    )
    .await
}

pub async fn python(
    entrypoint_file: &ScriptFile,
    entrypoint_file_signature: &String,
    supplementary_files: &Vec<ScriptFile>,
    returns_output_file_names: &Vec<String>,
) -> ScriptResponse {
    script_request::<PythonRequestPostVariables>(
        String::from("python"),
        PythonRequestPostVariables {
            arguments: Vec::new(), // TODO if arguments are required -> expose
            entrypoint_file: entrypoint_file.clone(),
            entrypoint_file_signature: String::from(entrypoint_file_signature),
            supplementary_files: supplementary_files.clone(),
            returns_output_file_names: returns_output_file_names.clone(),
        },
    )
    .await
}

pub async fn latex(
    main_doc_file: &ScriptFile,
    main_doc_file_signature: &String,
    supplementary_files: &Vec<ScriptFile>,
) -> ScriptResponse {
    script_request::<LatexRequestPostVariables>(
        String::from("latex"),
        LatexRequestPostVariables {
            main_doc_file: main_doc_file.clone(),
            main_doc_file_signature: String::from(main_doc_file_signature),
            supplementary_files: supplementary_files.clone(),
        },
    )
    .await
}

// get graphql response from self (over network, to not risk generating security holes and avoid figuring out how to do direct calls without the juniper_actix::graphql_handler)
pub async fn gql_loopback(bearer_token: &String, query: &String) -> ScriptResponse {
    let graphql_reqwest_client = reqwest::Client::new();
    let (gql_loopback_success, gql_response_text) = match graphql_reqwest_client
        .post(format!(
            "http://localhost:{}/graphql",
            env::var("WEBSERVER_PORT").expect("Webserver port ('WEBSERVER_PORT') not set in .env")
        ))
        .header("Authorization", format!("Bearer {}", bearer_token))
        .header("Content-Type", "application/json")
        .json(&serde_json::json!({
            "query": query,
        }))
        .send()
        .await
    {
        Ok(response) => {
            if response.status().is_success() {
                info!("Graphql loopback successful");

                let graphql_response = String::from(response.text().await.unwrap());
                (true, graphql_response)
            } else if response.status().is_client_error() {
                error!(
                    "Graphql loopback did not accept the format of request/query {}",
                    response.status()
                );

                let error_msg = String::from(response.text().await.unwrap());
                debug!("Graphql loopback client error msg {}", error_msg);
                (false, error_msg)
            } else {
                error!(
                    "Graphql loopback could be reached but no success {}",
                    response.status()
                );
                (false, String::from("Unknown Graphql Server State"))
            }
        }
        Err(error) => {
            // If we are here, loopback graphql didn't respond
            error!(
                "Graphql loopback could not be reached from export {}",
                error
            );
            (false, String::from("Server error"))
        }
    };

    ScriptResponse {
        files: Vec::new(),
        success: gql_loopback_success,
        std_out: if gql_loopback_success {
            String::from(&gql_response_text)
        } else {
            String::from("")
        },
        std_err: if !gql_loopback_success {
            String::from(&gql_response_text)
        } else {
            String::from("")
        },
        async_id: None,
    }
}

pub async fn script_server_status() -> (bool, String) {
    let response = script_request::<VersionRequestPostVariables>(
        String::from("version"),
        VersionRequestPostVariables {},
    )
    .await;

    match response.success {
        true => (
            true,
            String::from(format!("{}{}", response.std_out, response.std_err)),
        ),
        false => (
            false,
            String::from(format!(
                "Failed Version lookup {}{}",
                response.std_out, response.std_err
            )),
        ),
    }
}

async fn script_request<T: serde::Serialize>(path_segment: String, input: T) -> ScriptResponse {
    debug!("Script job started to job {}", &path_segment);

    // get script response from adjacent running script server (improved security, because no shelling out/script execution on backend machine AND smaller backend image)
    let reqwest_client = reqwest::Client::new();

    let mut url = Url::parse(
        &env::var("SCRIPTSERVER_URL")
            .expect("Scriptserver URL ('SCRIPTSERVER_URL') not set in .env"),
    )
    .unwrap();
    let mut poll_url = url.clone();
    url.path_segments_mut().unwrap().push(&path_segment);
    poll_url.path_segments_mut().unwrap().push("cache");

    match reqwest_client
        .post(url.to_string())
        .header(
            "Authorization",
            format!(
                "Bearer {}",
                env::var("SCRIPTSERVER_AUTH_TOKEN")
                    .expect("Scriptserver auth token ('SCRIPTSERVER_AUTH_TOKEN') not set in .env")
            ),
        )
        .header("Content-Type", "application/json")
        .json(&input)
        .send()
        .await
    {
        Ok(response) => {
            let success_status = response.status().is_success();
            let response_status = response.status();

            let (response_decodable, resp_opt, decode_err) =
                match response.json::<ScriptResponse>().await {
                    Ok(resp) => (true, Some(resp), String::from("")),
                    Err(err) => (false, None, err.to_string()),
                };
            if !response_decodable {
                error!("Decoding Response failed {}", decode_err);

                return ScriptResponse {
                    success: false,
                    std_err: String::from(format!("Decoding Response failed {}", decode_err)),
                    std_out: String::from(""),
                    files: Vec::new(),
                    async_id: None,
                };
            }
            let script_response = resp_opt.unwrap(); // must work here

            if success_status {
                info!("Script dispatched successfully");

                let mut step_script_response = script_response.clone();

                let mut counter = 0;
                let max_poll_iterations = 1500; // ca 20 min
                let poll_delay_in_ms = 800;
                loop {
                    if let Some(poll_id) = step_script_response.async_id {
                        // we need to poll the scriptserver, execution is asynchronously
                        info!("Script requires polling of id {}", poll_id);

                        let poll_input = ProbeRequestPostVariables { id: poll_id };

                        match reqwest_client
                            .post(poll_url.to_string())
                            .header(
                                "Authorization",
                                format!(
                                                "Bearer {}",
                                                env::var("SCRIPTSERVER_AUTH_TOKEN")
                                                .expect("Scriptserver auth token ('SCRIPTSERVER_AUTH_TOKEN') not set in .env")
                                            ),
                            )
                            .header("Content-Type", "application/json")
                            .json(&poll_input)
                            .send()
                            .await
                        {
                            Ok(poll_response) => {
                                let poll_response_success_status = poll_response.status().is_success();
                                let poll_response_status = poll_response.status();

                                let (poll_response_decodable, poll_resp_opt, poll_decode_err) =
                                match poll_response.json::<ScriptResponse>().await {
                                    Ok(resp) => (true, Some(resp), String::from("")),
                                    Err(err) => (false, None, err.to_string()),
                                };

                                if !poll_response_decodable {
                                    error!("Decoding Poll Response failed {}", poll_decode_err);

                                    return ScriptResponse {
                                        success: false,
                                        std_err: String::from(format!("Decoding Poll Response failed {}", poll_decode_err)),
                                        std_out: String::from(""),
                                        files: Vec::new(),
                                        async_id: None,
                                    };
                                }
                                let poll_script_response = poll_resp_opt.unwrap(); // must work here
                                    
                                if poll_response_success_status {
                                    step_script_response = poll_script_response;

                                    if step_script_response.async_id.is_some() {
                                        debug!("Polling still returned an id that should be polled, not finished");
                                    } else {
                                        debug!("Polling did no longer return a polling id. Should be finished");
                                    }
                                } else {
                                    error!(
                                        "Script server did not accept poll request to the cache {}",
                                        poll_response_status
                                    );
                    
                                    debug!(
                                        "Script server poll endpoint error msg {}",
                                        &poll_script_response.std_err
                                    );
                                    return poll_script_response; // instant return, error-status on endpoint requires stop of polling
                                }
                            }
                            Err(poll_err) => {
                                error!("Script execution server polling endpoint could not be reached: {}", poll_err);
                                return ScriptResponse {
                                    success: false,
                                    std_err: poll_err.to_string(),
                                    std_out: String::from(""),
                                    files: Vec::new(),
                                    async_id: None,
                                };
                            }
                        }
                    } else {
                        if counter != 0 {
                            debug!("Polling iteration {} and no async_id returned: polling finished", counter);
                        }
                        break;
                    }

                    sleep(Duration::from_millis(poll_delay_in_ms)).await;
                    counter += 1; // increment poll counter

                    if counter >= max_poll_iterations {
                        error!(
                            "Polling long running job timed out after {} iterations",
                            counter
                        );
                        return ScriptResponse {
                            success: false,
                            std_err: String::from(format!("Polling long running job timed out after {} iterations", counter)),
                            std_out: String::from(""),
                            files: Vec::new(),
                            async_id: None,
                        };
                    }
                }

                // first request AND (potential) polling succeeded
                step_script_response
            } else {
                error!(
                    "Script execution server did not accept the format of request/query {}",
                    response_status
                );

                debug!(
                    "Script execution server error msg {}",
                    &script_response.std_err
                );
                script_response
            }
        }
        Err(error) => {
            error!("Script execution server could not be reached {}", error);
            ScriptResponse {
                success: false,
                std_err: error.to_string(),
                std_out: String::from(""),
                files: Vec::new(),
                async_id: None,
            }
        }
    }
}
