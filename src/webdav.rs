use mathezirkel_app_webdav::client::Client;
use serde::{Deserialize, Serialize};
use std::env;
use url::Url;

#[derive(Serialize, Deserialize)]
pub struct PathOnlyPostVariables {
    path: String,
}
#[derive(Serialize, Deserialize)]
pub struct PathContentPostVariables {
    path: String,
    content: String,
}

fn webdavclient() -> Client {
    Client::init(
        &env::var("WEBDAV_USER").expect("Webdav user ('WEBDAV_USER') not set in .env"),
        &env::var("WEBDAV_PASSWORD").expect("Webdav password ('WEBDAV_PASSWORD') not set in .env"),
    )
}

pub async fn mkdir(relative_path: &String) -> bool {
    let mut url =
        Url::parse(&env::var("WEBDAV_URL").expect("Webdav url ('WEBDAV_URL') not set in .env"))
            .unwrap();
    let segments: Vec<&str> = relative_path.split('/').collect();
    for segment in segments {
        if !segment.is_empty() {
            url.path_segments_mut().unwrap().push(segment);
        }
    }
    debug!("Full-Path: {}", &url.to_string());

    let resp = webdavclient().mkcol(&url.to_string()).await;

    match resp {
        Ok(resp) => {
            if resp.status().is_success() {
                debug!("Webdav mkdir success");
                true
            } else if resp.status().is_server_error() {
                error!("Webdav mkdir server error!");
                false
            } else {
                error!(
                    "Mkdir: Something else happened. Status: {:?}",
                    resp.status()
                );
                false
            }
        }
        Err(_) => {
            error!("Webdav mkdir error!");
            false
        }
    }
}

pub async fn put(relative_path: &String, content: &Vec<u8>) -> bool {
    let mut url =
        Url::parse(&env::var("WEBDAV_URL").expect("Webdav url ('WEBDAV_URL') not set in .env"))
            .unwrap();
    let segments: Vec<&str> = relative_path.split('/').collect();
    for segment in segments {
        if !segment.is_empty() {
            url.path_segments_mut().unwrap().push(segment);
        }
    }
    debug!("Full-Path: {}", &url.to_string());

    let content_local = content.clone();
    let resp = webdavclient().put(content_local, &url.to_string()).await;

    match resp {
        Ok(resp) => {
            if resp.status().is_success() {
                debug!("Webdav put success");
                true
            } else if resp.status().is_server_error() {
                error!("Webdav put server error!");
                false
            } else {
                error!("Put: Something else happened. Status: {:?}", resp.status());
                false
            }
        }
        Err(_) => {
            error!("Webdav put error!");
            false
        }
    }
}

/// Relative path must end with "/" in order to delete a folder.
/// If the given path ends with "/" that will be kept and if the last segment does not contain a "." an extra "/" will be pushed
pub async fn delete(relative_path: &String) -> bool {
    let mut url =
        Url::parse(&env::var("WEBDAV_URL").expect("Webdav url ('WEBDAV_URL') not set in .env"))
            .unwrap();
    let ends_with_slash = relative_path.ends_with('/');
    let segments: Vec<&str> = relative_path.split('/').collect();
    let mut contains_dot = false;
    for segment in segments {
        contains_dot = segment.contains('.');
        if !segment.is_empty() {
            url.path_segments_mut().unwrap().push(segment);
        }
    }
    let mut full_url = url.to_string();
    if ends_with_slash || !contains_dot {
        full_url.push('/');
    }
    debug!("Full-Path: {}", &full_url);

    let resp = webdavclient().delete(&full_url).await;

    match resp {
        Ok(resp) => {
            if resp.status().is_success() {
                debug!("Webdav delete success");
                true
            } else if resp.status().is_server_error() {
                error!("Webdav delete server error!");
                false
            } else {
                error!(
                    "Delete: Something else happened. Status: {:?}",
                    resp.status()
                );
                false
            }
        }
        Err(_) => {
            error!("Webdav delete error!");
            false
        }
    }
}
