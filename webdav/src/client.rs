/*
 * File: client.rs
 * Authors: Jonas Kell, Andrei Nagacevschi <andrein_bio@yahoo.com>
 * License: GNU GPLv3
 *
 * This file is originally taken from the `rustydav` crates implementation.
 * https://github.com/andreinbio/rustydav
 *
 * The library was lacking multiple required asynchronicity and timeout-limit (because of reqwest-blocking) therefor was modified and stripped for use in the mathezirkel app
 */

//! Webdav client
//!
//! Create a client
//! ```ignore
//! let client = Client::init("username", "password");
//! ```
//! Now you can use the client to call any of the methods listed in the **Client** Struct.
//!
//! All the paths used by the methods should be absolute on the webdav server to the required file, folder, zip.
//!
//! Every method will return a Result<Response, Error>
//! ```rust
//! # let result: Result<&str, String> = Ok("test");
//! if result.is_ok() {
//!    // the method completed with success
//! } else {
//!    // something when wrong
//! }
//! ```

use reqwest::{header, Body, Error, Method, RequestBuilder, Response, Url};

#[derive(Debug)]
pub struct Client {
    username: String,
    password: String,
    client: reqwest::Client,
}

impl Client {
    /// Initialization of the client
    ///
    /// Initialized client will be stored for future requests
    pub fn init(username: &str, password: &str) -> Self {
        Client {
            username: username.to_owned(),
            password: password.to_owned(),
            client: reqwest::Client::new(),
        }
    }

    fn custom_header(&self, name: &str, value: &str) -> header::HeaderMap {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            header::HeaderName::from_bytes(name.as_bytes()).unwrap(),
            header::HeaderValue::from_bytes(value.as_bytes()).unwrap(),
        );
        headers
    }

    /// Main function that creates the RequestBuilder, sets the method, url and the basic_auth
    fn start_request(&self, method: Method, path: &str) -> RequestBuilder {
        self.client
            .request(method, Url::parse(path).unwrap())
            .basic_auth(self.username.as_str(), Some(self.password.as_str()))
    }

    /// Get a file from Webdav server
    ///
    /// Use absolute path to the webdav server file location
    pub async fn get(&self, path: &str) -> Result<Response, Error> {
        self.start_request(Method::GET, path).send().await
    }

    /// Upload a file/zip on Webdav server
    ///
    /// It can be any type of file as long as it is transformed to a vector of bytes (Vec<u8>).
    /// This can be achieved with **std::fs::File** or **zip-rs** for sending zip files.
    ///
    /// Use absolute path to the webdav server folder location
    pub async fn put<B: Into<Body>>(&self, body: B, path: &str) -> Result<Response, Error> {
        self.start_request(Method::PUT, path)
            .headers(self.custom_header("content-type", "application/octet-stream"))
            .body(body)
            .send()
            .await
    }

    /// Deletes the collection, file, folder or zip archive at the given path on Webdav server
    ///
    /// Use absolute path to the webdav server file location
    pub async fn delete(&self, path: &str) -> Result<Response, Error> {
        self.start_request(Method::DELETE, path).send().await
    }

    /// Creates a directory on Webdav server
    ///
    /// Use absolute path to the webdav server file location
    pub async fn mkcol(&self, path: &str) -> Result<Response, Error> {
        self.start_request(Method::from_bytes(b"MKCOL").unwrap(), path)
            .send()
            .await
    }
}
